;$PG
$PL=59
;$TL(NEW STAR 160 VADJ_FUNC.ASM SOURCE FILE)
	INCLUDE "INCLUDE.H"
;$Header: C:/Dosland/ns160e/rcs/vadj.asm 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------
; ROUTINE NAMES:

	GLOBAL	VADJ_FUNC
	GLOBAL	TFVADJ

; PROJECT: NEW STAR 160
;--------------------------------------------------------- 


	DEFSEG	VADJSEG,CLASS=CODE
	SEG	VADJSEG

;$SB(VOLTAGE ADJUST SUBROUTINE)


; DATA INPUTS: 

; DATA OUTPUTS
 	EXTERN	EVREQ
	EXTERN	EMVALA
	EXTERN	CAPVAL
	EXTERN	HIWIND
	EXTERN	DACVAL
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE0


; ROUTINES CALLED BY: 
; ROUTINES CALLED
	EXTERN	DIVSHIFT16_FUNC



$EJECT
;$AP

	;***VADJ_FUNC***
	;vadj - checks energy reading and adjusts voltage to correct to 
	;requested energy
	;has 15 volt upside limit as safety 
	;called by - treat
	;calling parameters - none
	;returns - adjusted value in capval and dacval
	;registers used - all
	;calls - 

VADJ_FUNC:	LDA	EVREQ	;get requested energy value
	MOV	C,A	;requested to 'c' reg
GETA:	LDA	EMVALA	;compare with energy reading (read - req.)
	CMP	C	;and adjust voltage accordingly
	JZ	VVOUT	;if emvala = req., no change
	JNC	VDEC	;if emvala > req., dec. voltage
	JC	VINC	;if emvala < req., inc. voltage

VDEC:	LDA	CAPVAL	;if at min voltage, don't adjust
	CPI	VLO
	JC	VVOUT

	MVI	D,0FFH	;prep for subtraction
	LDA	EMVALA	;emvala - evreq = diff
	SUB	C	
	MOV	E,A	;move diff to de for test against hi window
	LDA	HIWIND	;if difference greater than approx. 15%
	CMP	E	;then clip at 15% (no rogue) (we hope)
	JNC	NVCLIPD	;if hiwind > diff, proceed
	MOV	E,A	;else diff = hiwind

NVCLIPD:MOV	A,E
	CMA		;2's complement for subtraction
	INR	A	
	MOV	E,A	;move diff to de for adjust
	
	LHLD	DACVAL
	DAD	D	;add 2's compl. = subtraction
	DAD	D	;5 * diff = dacval adjust 
	DAD	D	;approx. .183 millivolts per mj of difference
	DAD	D
	DAD	D
	SHLD	DACVAL
	
	CALL	DIVSHIFT16_FUNC	;divide by 16 by shifting to save time
	STA	CAPVAL	;and store as capval

	CPI	VLO	;if adjusted value < vlo, clip at vlo
	JNC	VVOUT
	MVI	A,VLO
	STA	CAPVAL
	LXI	H,VLO16
	SHLD	DACVAL
	JMP	VVOUT

VINC:	MVI	A,VMAX	;see if at max for this rep rate
	MOV	D,A
	LDA	CAPVAL	;if at max voltage, don't increase
	CMP	D
	JZ	VVOUT
	
	ORA	A	;clear carry
	MVI	D,0	;prep de for arithmetic
	LDA	EMVALA
	MOV	C,A	
	LDA	EVREQ
	SUB	C	;evreq - emvala = diff
	MOV	E,A	;move diff to de for adjust
	LDA	HIWIND	;if difference greater than approx. 15%
	CMP	E	;then clip at 15% (no rogue) (we hope)
	JNC	NVCLIP	;if hiwind > diff, proceed
	MOV	E,A	;else diff = hiwind
	STA	KEYBOARD_DEBOUNCE_STORAGE0	;just do this store for emulator to see

NVCLIP:	LHLD	DACVAL
	DAD	D	;add diff :  diff = dacval adjust 
	DAD	D	;5 * diff = dacval adjust 
	DAD	D	;approx. .183 millivolts per mj of difference
	DAD	D
	DAD	D
	
			;check if at or over max
	XCHG		;move dacval to de reg.
	LXI	H,VMAX16	;get max allowable volts
		
	MOV	A,D	;if de reg =>hl(vma16) then hl = max
	CMP	H	
	JC	VMOK	;if < vmax16, ok
	JNZ	VMCLIP	;if > vmax16, clip
	
	MOV	A,E	;if hi byte of dacval was = to vmax16 hi, check
			;low byte
	CMP	L
	JC	VMOK

VMCLIP:	XCHG		;put vmax16 (hl reg.) into de reg (dacval)
VMOK:	XCHG		;de (capval) back to hl
	SHLD	DACVAL

	CALL	DIVSHIFT16_FUNC	;divide by 16 by shifting to save time
	CPI	VLO	;if teeny, must have rolled-over
	JNC	NO_VI_ROLLOVER

	MVI	A,0FFH	;clip at max
NO_VI_ROLLOVER:
	STA	CAPVAL	;and store as capval
VVOUT:	RET


	;***TFVADJ***
	;tfvadj - checks energy reading and adjusts voltage to correct to 
	;requested energy
	;doesn't have upside limits that 'treat' vadj has
	;called by - map_fire
	;calling parameters - none
	;returns - adjusted value in capval and dacval
	;registers used - all
	;calls - 
TFVADJ:	LDA	EVREQ	;get requested energy value
	MOV	C,A	;requested to 'c' reg
	LDA	EMVALA	;compare with energy reading (read - req.)
	CMP	C	;and adjust voltage accordingly
	JZ	TFVVOUT	;if emvala = req., no change
	JNC	TFVDEC	;if emvala > req., dec. voltage
	JC	TFVINC	;if emvala < req., inc. voltage

TFVDEC:	LDA	CAPVAL	;if at min voltage, don't adjust
	CPI	VLO
	JC	TFVVOUT

	MVI	D,0FFH	;prep for subtraction
	LDA	EMVALA	;emvala - evreq = diff
	SUB	C	
	CMA		;2's complement for subtraction
	INR	A	
	MOV	E,A	;move diff to de for adjust
	LHLD	DACVAL
	DAD	D	;add 2's compl. = subtraction
	DAD	D	;5 * diff = dacval adjust 
	DAD	D	;approx. .183 millivolts per mj of difference
	DAD	D
	DAD	D
	SHLD	DACVAL
	
	CALL	DIVSHIFT16_FUNC	;divide by 16 by shifting to save time
	STA	CAPVAL	;and store as capval

	CPI	VLO	;if adjusted value < vlo, clip at vlo
	JNC	TFVVOUT
	MVI	A,VLO
	STA	CAPVAL
	LXI	H,VLO16
	SHLD	DACVAL
	JMP	TFVVOUT

TFVINC:	MVI	A,VMAX	;see if at max for this rep rate
	MOV	D,A
	LDA	CAPVAL	;if at max voltage, don't increase
	CMP	D
	JZ	TFVVOUT

	MVI	D,0	;prep de for arithmetic
	LDA	EMVALA
	MOV	C,A	
	LDA	EVREQ
	SUB	C	;evreq - emvala = diff
	MOV	E,A	;move diff to de for adjust
	LHLD	DACVAL
	DAD	D	;add diff :  diff = dacval adjust 
	DAD	D	;5 * diff = dacval adjust 
	DAD	D	;approx. .183 millivolts per mj of difference
	DAD	D
	DAD	D
			;check if at or over max
	XCHG		;move dacval to de reg.
	LXI	H,VMAX16	;get current max allowable volts
		
	MOV	A,D	;if de reg =>hl(VMAX16) then hl = max
	CMP	H	
	JC	TVMOK	;if < VMAX16, ok
	JNZ	TVMCLIP	;if > VMAX16, clip
	
	MOV	A,E	;if hi byte of dacval was = to VMAX16 hi, check
			;low byte
	CMP	L
	JC	TVMOK

TVMCLIP:XCHG		;put VMAX16 (hl reg.) into de reg (dacval)
TVMOK:	XCHG		;de (capval) back to hl
	SHLD	DACVAL

	CALL	DIVSHIFT16_FUNC	;divide by 16 by shifting to save time
	CPI	VLO	;if teeny, must have rolled-over
	JNC	TFNO_VI_ROLLOVER

	MVI	A,0FFH	;clip at max
TFNO_VI_ROLLOVER:
	STA	CAPVAL	;and store as capval

TFVVOUT:RET

	END
