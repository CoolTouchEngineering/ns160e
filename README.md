#Abstract

The following describes the steps that are necessary to get the NS160E firmware up and running on the 8085 CPU Board.


##Summary
The firmware is written in assembly and originally used the Avocet 8085 tool-chain to build the system.
The firmware has been ported to the Softools Toolchain.

### Version
Original V1.50

###Setup
1. Clone the repo
1. Install git bash
1. Install source tree
1. Install Softools
1. Install Avocet (if required)
1. Install Python script "checksum8085"
1. Install Hexplorer to generate ascii hex file, which is processed by "checksum8085"
1. Install Notepad++ (if required)
1. Install Needham's programmer application (Lab system has application running)
1. Beyond Compare (if required, or other diff tool)

###Configuration
Two EEPROMs are required in the NS160E inorder for operation.

###Dependencies
* Softool Toolchain 
* Python script "checksum8085"
* Hexplorer
* Needham's programmer

###How to run tests for Software Development
> Disable (comment out) the checksum verification (INIT.ASM)
See main.c in repo
See checksum8085.py
Limited testing can be performed on a custom development system which does not have a laser, and other components. 
New code is burned into 2 EEPROMs and installed into the test system. System feedback and debug breadcrumbs can be 
provided by writing debug information to the 2x20 display.

###Deployment instructions
See the "LC225-TC EEPROM Programmer Needham" document
See main.c "Release" information
See checksum8085.py


### Contribution guidelines

####Writing tests
TBD

####Code review
Diffs are generated and walthroughs of the diffs, testing and design are addressed.

####Other guidelines
TBD

####Who do I talk to?
* floydg@syneron-candela.com
* dineshk@syneron-candela.com