;$PG
$PL=59
;$TL(NS160E DISPLAY.ASM SOURCE FILE)

	INCLUDE "INCLUDE.H"
;$Header: C:/Dosland/ns160e/rcs/DISPLAY.ASM 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------
; ROUTINE NAMES:

	GLOBAL	OUTPUT_DISPLAY_FUNC
	GLOBAL	WATTS_DISPLAY_FUNC
	GLOBAL	FIBERTEST_OUTPUT_DISPLAY_SETUP_FUNC
	GLOBAL	FIBERTEST_OUTPUT_DISPLAY_FUNC
	GLOBAL	CAL_DISPLAY_FUNC
	GLOBAL	REPRATE_DISPLAY_FUNC
	GLOBAL	SITE_TIME_DISPLAY_FUNC
	GLOBAL	MODE_DISPLAY_FUNC
	GLOBAL	THDISPL
	GLOBAL	THC_FUNC
	GLOBAL	TOTAL_TIME_DISPLAY_FUNC
	GLOBAL	TOTAL_PULSES_DISPLAY_FUNC
	GLOBAL	TOTAL_JOULES_DISPLAY_FUNC
	GLOBAL	TOTAL_HOURS_DISPLAY
	GLOBAL	RESTORE_DISPLAY_FUNC

	GLOBAL	PUTSTR_FUNC
	GLOBAL	WRITE_TO_DISPLAY_FUNC
	GLOBAL	BINASC_FUNC
	GLOBAL	REVISION_DISPLAY_FUNC
	GLOBAL	INITIALIZE_DISPLAY_FUNC
	GLOBAL	RESET_DISPLAY_FUNC
	GLOBAL	SET_CURSOR_POSITION_FUNC

; PROJECT: NS160E
;---------------------------------------------------------


	DEFSEG	DISPLAYSEG,CLASS=CODE
	SEG	DISPLAYSEG


; DATA INPUTS:
	EXTERN	READY_AIM_FLAG
	EXTERN	AVVAL
	EXTERN	AVVALE
	EXTERN	MEASURED_ENERGY_OK
	EXTERN	EVREQ
	EXTERN	DEVREQ
	EXTERN	REQUESTED_POWER
	EXTERN	REPRATE
	EXTERN	ESTEP
	EXTERN	MODEFLG
	EXTERN	STBSTR

	EXTERN	AIMSTR
	EXTERN	RDYSTR
	EXTERN	TOTAL_TIME
	EXTERN	S_HOURS
	EXTERN	TMPSTAT
	EXTERN	OTAFLG
	EXTERN	OTSTAT
	EXTERN	TOTAL_PULSES
	EXTERN	TOTAL_PULSES1
	EXTERN	TEVAL
	EXTERN	TEVAL1
	EXTERN	REV
	EXTERN	LC0
	EXTERN	LC1

	EXTERN	PROC_COUNT_CURR
	EXTERN	PROC_COUNT_DISPLAYED
	EXTERN	TOTAL_PROCS_CURR
	EXTERN	SERSTR_CURR
	EXTERN	FTSTR_CURR
	EXTERN	CARD_STATUS
	EXTERN	INT_CARD_STATUS

	EXTERN	FTSTR_CURR
	EXTERN	FCAL_CURR

	EXTERN	SITE_TIME

; DATA OUTPUTS
	EXTERN	DAVVAL
	EXTERN	TAVVAL
	EXTERN	TEMP0
	EXTERN	TEMP1
	EXTERN	BINTEMP
	EXTERN	LZERO
	EXTERN	D1
	EXTERN	D0

; ROUTINES CALLED BY:
; ROUTINES CALLED
	EXTERN	MULT_FUNC
	EXTERN	DIVIDE_FUNC
	EXTERN	FRAC_FUNC
	EXTERN	BLOOP_FUNC
	EXTERN	BLIP_FUNC

;$AP
$EJECT


	;*************************
	;*      DISPLAY	;*
	;*      SUBROUTINES     ;*
	;*************************

;	**********display routines

OUTPUT_DISPLAY_FUNC:		;display laser output
	CALL	WATTS_DISPLAY_FUNC
	RET

	;***WATTS_DISPLAY_FUNC****
	;displays power reading (or w selected) in upper left corner
	;called by -
	;calling parameters - e value in avval
	;returns - none
	;registers used - a
	;calls - PUTSTR_FUNC, mult, BINASC_FUNC
WATTS_DISPLAY_FUNC:
			;first correct avval for fcal_curr (see eselect)
	LHLD	FCAL_CURR	;get cal value (it's <256 even tho 2 bytes)
	MOV	A,H	;test for insane values
	ORA	A	;should be < 256
	JNZ	DAF_FAIL

	MOV	A,L	;should be <= 100% of nominal
	CPI	101
	JNC	DAF_FAIL
			;should be > 49% of nominal
	CPI	49
	JC	DAF_FAIL

	MOV	C,A	;fcal is fraction so davval = avval * fcal
			;e.g. avval = 100 * 80 (80%) = davval = 80
	LDA	AVVAL
	CALL	FRAC_FUNC
	STA	DAVVAL

	MVI	A,0
	CALL	SET_CURSOR_POSITION_FUNC	;cursor to watts display position
	LDA	DAVVAL	;get davval (displayed energy)
	STA	TAVVAL

	LDA	MEASURED_ENERGY_OK	;if within +-20%, display requested power
	CPI	'Y'
	JNZ	WASIS	;if outside window display measured power

	LDA	REQUESTED_POWER	;if inside window display requested power
	MVI	C,100
	CALL	MULT_FUNC	;power * 1000 in hl
	JMP	WNORM

WASIS:	LDA	TAVVAL	;tavval*2*rr = watts
	MVI	C,2	;scale = 2:1
	CALL	MULT_FUNC
	MOV	C,L	;tavval * 2 from l reg
	LDA	REPRATE
	CALL	MULT_FUNC	;prod. in hl

WNORM:	SHLD	TEMP0	;put in temp for bcd conversion
	LXI	H,0
	SHLD	TEMP1
	CALL	BINASC_FUNC
			;round number to nearest tenth
	LDA	BINTEMP+7	;get hundredth digit
	CPI	'5'	;= 5?
	JZ	WRNDN	;if = 5  as is
	MVI	A,'0'	;if round up or down, digit = 0
	STA	BINTEMP+7
	JNC	WRNDUP	;if > 5, round up
	JMP	WRNDN	;else, round down

WRNDUP:	LDA	BINTEMP+6	;if > 5 , round up next digit to left
	INR	A
	CPI	3AH	;bump to > 9?
	STA	BINTEMP+6
	JC	WRNDN	;if not > 9, then done

	MVI	A,'0'	;if > 9, bump next digit (watts digit)
	STA	BINTEMP+6
	LDA	BINTEMP+5
	INR	A
	CPI	3AH	;bump to > 9?
	STA	BINTEMP+5
	JC	WRNDN	;if not > 9, then done

	MVI	A,'0'	;if > 9, bump next digit (10 watts digit)
	STA	BINTEMP+5
	LDA	BINTEMP+4
	INR	A
	STA	BINTEMP+4

WRNDN:	MVI	A,0
	STA	LZERO	;reset leading zero flag

WD_CONT:LXI	H,BINTEMP+4	;point to tens of watts digit
	MOV	A,M	;get 'tens of watts' digit
	CPI	30H	;if zero, change to space char.
	JZ	LZW

	MVI	A,0FFH	;if not, set lzero flag
	STA	LZERO
	MOV	A,M	;get char.
	JMP	NZW

LZW:	MVI	A,20H
NZW:	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit

	MOV	A,M	;get 'watts' digit
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit

	MVI	A,'.'	;decimal point
	CALL	WRITE_TO_DISPLAY_FUNC
	MOV	A,M	;get tenths digit
	CALL	WRITE_TO_DISPLAY_FUNC

	LXI	H,WSTR	;point to string 'watt'
	CALL	PUTSTR_FUNC	;out to display

DWDONE:	MVI	A,'N'	;force ot update by init overtemp alarm flag
	STA	OTAFLG
	RET

WSTR:	DB	'W$'



DAF_FAIL:
	CALL	RESET_DISPLAY_FUNC	;illegal fcal value!! warn operator!!!
	LXI	H,DAFSTR	;point to string
	CALL	PUTSTR_FUNC	;out to display
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;2nd line
	LXI	H,DAFSTR2	;point to string
	CALL	PUTSTR_FUNC	;out to display
	CALL	BLOOP_FUNC
	CALL	BLIP_FUNC
	RET

DAFSTR:		DB	'ILLEGAL CALIBRATION$'
DAFSTR2:	DB	'VALUE - CALL SERVICE$'


	;***FIBERTEST_OUTPUT_DISPLAY_SETUP_FUNC****
	;displays energy readings from rear and ext monitors
	;called by - cal
	;calling parameters - e value in avval, avvale
	;returns - none
	;registers used - a
	;calls - WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC, mult, BINASC_FUNC
FIBERTEST_OUTPUT_DISPLAY_SETUP_FUNC:
	CALL	RESET_DISPLAY_FUNC	;clear screen

	LXI	H,FTEST_STR1	;display 'target output = 5.0W'
	CALL	PUTSTR_FUNC

	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;2nd line
	LXI	H,FTEST_STR2	;display 'fiber output = 0.0W'
	CALL	PUTSTR_FUNC

	MVI	A,0
	STA	TAVVAL
	CALL	FT_POWER_OUT_FUNC
	RET

	;***FIBERTEST_OUTPUT_DISPLAY_FUNC****
	;displays energy readings from rear and ext monitors
	;called by - cal
	;calling parameters - e value in avval, avvale
	;returns - none
	;registers used - a
	;calls - WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC, mult, BINASC_FUNC
FIBERTEST_OUTPUT_DISPLAY_FUNC:
	MVI	A,35
	CALL	SET_CURSOR_POSITION_FUNC	;2nd line
	LDA	AVVALE	;averaged ext reading
	STA	TAVVAL

	CALL	FT_POWER_OUT_FUNC
	RET

FTEST_STR1:	DB	'TARGET OUTPUT = 5.0W$'
FTEST_STR2:	DB	'FIBER OUTPUT = $'

FT_POWER_OUT_FUNC:
	LDA	TAVVAL	;tavval*2*rr = watts
	MVI	C,2	;scale = 2:1
	CALL	MULT_FUNC
	MOV	C,L	;tavval * 2 from l reg
	LDA	REPRATE
	CALL	MULT_FUNC	;prod. in hl

	SHLD	TEMP0	;put in temp for bcd conversion
	LXI	H,0
	SHLD	TEMP1
	CALL	BINASC_FUNC
			;round number to nearest tenth
	LDA	BINTEMP+7	;get hundredth digit
	CPI	'5'	;= 5?
	JZ	FWRNDN	;if = 5  as is
	MVI	A,'0'	;if round up or down, digit = 0
	STA	BINTEMP+7
	JNC	FWRNDUP	;if > 5, round up
	JMP	FWRNDN	;else, round down

FWRNDUP:LDA	BINTEMP+6	;if > 5 , round up next digit to left
	INR	A
	CPI	3AH	;bump to > 9?
	STA	BINTEMP+6
	JC	FWRNDN	;if not > 9, then done

	MVI	A,'0'	;if > 9, bump next digit (watts digit)
	STA	BINTEMP+6
	LDA	BINTEMP+5
	INR	A
	CPI	3AH	;bump to > 9?
	STA	BINTEMP+5
	JC	FWRNDN	;if not > 9, then done

	MVI	A,'0'	;if > 9, bump next digit (10 watts digit)
	STA	BINTEMP+5
	LDA	BINTEMP+4
	INR	A
	STA	BINTEMP+4

FWRNDN:	MVI	A,0
	STA	LZERO	;reset leading zero flag


			;if power is less than 1 watt, just display 0.0W
	LXI	H,BINTEMP+4	;point to tens of watts digit
	MOV	A,M	;get 'tens of watts' digit
	CPI	30H	;if non-zero, proceed to normal display
	JNZ	FTO_CONT

	INX	H
	MOV	A,M	;get 'watts' digit
	CPI	30H	;if non-zero, proceed to normal display
	JNZ	FTO_CONT

	LXI	H,W0STR	;point to string '0.0W'
	CALL	PUTSTR_FUNC	;out to display
	RET

W0STR:		DB	' 0.0W$'
FTO_CONT:
	LXI	H,BINTEMP+4	;point to tens of watts digit
	MOV	A,M	;get 'tens of watts' digit
	CPI	30H	;if zero, change to space char.
	JZ	FWLZW

	MVI	A,0FFH	;if not, set lzero flag
	STA	LZERO
	MOV	A,M	;get char.
	JMP	FWNZW

FWLZW:	MVI	A,20H
FWNZW:	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit

	MOV	A,M	;get 'watts' digit
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit

	MVI	A,'.'	;decimal point
	CALL	WRITE_TO_DISPLAY_FUNC
	MOV	A,M	;get tenths digit
	CALL	WRITE_TO_DISPLAY_FUNC

	LXI	H,WSTR	;point to string 'watt'
	CALL	PUTSTR_FUNC	;out to display
	RET

	;***CAL_DISPLAY_FUNC***
	;displays pass/fail (> +-20%)
	;called by - calibrate
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
CAL_DISPLAY_FUNC:
	LDA	DEVREQ	;this variable is corrected for fiber xmission
			;that is, no matter what the xmission is,
			;it should be 100mj

	MVI	C,79	;apply 80%, 120% test against this
	CALL	FRAC_FUNC	;frac returns 79% of evreq in acc.
	MOV	C,A	;now compare with measured output
	LDA	AVVALE
	CMP	C	;avvale minus 79%
	JC	CD_FAIL_LOW	;if < 79%, fail

	LDA	DEVREQ	;this variable is corrected for fiber xmission
	MVI	C,121	;apply 80%, 120% test against this
	CALL	FRAC_FUNC	;frac returns 121% of evreq in acc.
	MOV	C,A	;now compare with measured output
	LDA	AVVALE
	CMP	C	;avvale minus 121%
	JNC	CD_FAIL_HIGH	;if >=121%, fail
	JZ	CD_FAIL_HIGH

CD_PASS:
	CALL	RESET_DISPLAY_FUNC
	LXI	H,CD_PASS_STR1
	CALL	PUTSTR_FUNC
	RET

CD_PASS_STR1:	DB	' FIBER TEST PASSED  $'

CD_FAIL_LOW:
	CALL	RESET_DISPLAY_FUNC
	LXI	H,CD_FAILL_STR1
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CD_FAILL_STR2
	CALL	PUTSTR_FUNC
	RET

CD_FAILL_STR1:	DB	' FIBER TEST FAILED  $'
CD_FAILL_STR2:	DB	'  (OUTPUT TOO LOW)  $'

CD_FAIL_HIGH:
	CALL	RESET_DISPLAY_FUNC
	LXI	H,CD_FAILH_STR1
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CD_FAILH_STR2
	CALL	PUTSTR_FUNC
	RET

CD_FAILH_STR1:	DB	' FIBER TEST FAILED  $'
CD_FAILH_STR2:	DB	' (OUTPUT TOO HIGH)  $'



	;***REPRATE_DISPLAY_FUNC***
	;displays rep rate
	;called by - init, RESTORE_DISPLAY_FUNC, repset
	;calling parameters - rep rate in 'reprate'
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
REPRATE_DISPLAY_FUNC:
	MVI	A,5
	CALL	SET_CURSOR_POSITION_FUNC	;point to first line, rep rate pos.

	MVI	A,' '	;xxw @ 20hz e.g.
	CALL	WRITE_TO_DISPLAY_FUNC
	MVI	A,' '	;xxw @ 20hz e.g.
	CALL	WRITE_TO_DISPLAY_FUNC
	MVI	A,' '	;xxw @ 20hz e.g.
	CALL	WRITE_TO_DISPLAY_FUNC

DRR:	LDA	REPRATE
	MVI	H,0	;convert rep rate to bcd
	MOV	L,A
	SHLD	TEMP0	;put in temp for bcd conversion
	LXI	H,0
	SHLD	TEMP1
	CALL	BINASC_FUNC

	LXI	H,BINTEMP+7	;point to 2nd l.s.d.

	MOV	A,M	;get rep rate '10's' digit
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display

	INX	H	;next digit
	MOV	A,M	;get rep rate '1's' digit
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display

	LXI	H,REPSTR	;display 'Hz'
DRDONE:	CALL	PUTSTR_FUNC	;out to display
	RET

REPSTR:	DB	'Hz $'



	;******SITE_TIME_DISPLAY_FUNC***
	;displays total seconds at given treatment site
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - binasc, WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
SITE_TIME_DISPLAY_FUNC:
	MVI	A,16
	CALL	SET_CURSOR_POSITION_FUNC	;first display line

	LHLD	SITE_TIME	;get site treat seconds count
	SHLD	TEMP0	;put in temp for bcd conversion
	CALL	BINASC_FUNC

	MVI	A,0
	STA	LZERO	;reset leading zero flag

			;we display upper 4 digits of 5 digit number
			;least significant digit is tenth secs so
			;we don't display - only whole seconds

	LXI	H,BINTEMP+4	;point to 5th l.s.d.
	MOV	A,M	;get 'tens of thousands' digit
	CPI	30H	;if zero, change to space char.
	JNZ	SCTTHNZ	;if not, set lzero flag

	MVI	M,20H
	JMP	SCTTHO

SCTTHNZ:MVI	A,0FFH
	STA	LZERO	;set flag , non zero char.

SCTTHO:	MOV	A,M	;get char.
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display

	INX	H	;next digit

	LDA	LZERO	;if more significant digit >0, no blank
	ORA	A
	JNZ	SCTHO

	MOV	A,M	;get 'thousands' digit
	CPI	30H	;if zero, output space char.
	JNZ	SCTHONZ	;if not, output digit

	MVI	M,20H	;space char.
	JMP	SCTHO

SCTHONZ:MVI	A,0FFH
	STA	LZERO	;set flag , non zero char.

SCTHO:	MOV	A,M
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit

	LDA	LZERO	;if more significant digit >0, no blank
	ORA	A
	JNZ	SCHO

	MOV	A,M	;get 'hundreds' digit
	CPI	30H	;if zero, change to space char.
	JNZ	SCHNZ	;if not, set lzero flag

	MVI	M,20H
	JMP	SCHO

SCHNZ:	MVI	A,0FFH
	STA	LZERO	;set flag , non zero char.

SCHO:	MOV	A,M	;get char.
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display

	INX	H	;next digit

			;"tens" digit is really "ones" digit as displayed
			;NEVER blanked
	MOV	A,M
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit

	RET

	;***MODE_DISPLAY_FUNC***
	;displays current mode
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - BINASC_FUNC, WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
MODE_DISPLAY_FUNC:
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;cursor to mode position

	LDA	MODEFLG	;display mode
	CPI	'S'
	JZ	DSM
	CPI	'A'
	JZ	DAM
	CPI	'R'
	JZ	DRM

DSM:    
	LXI	H,STBSTR	;write 'standby'
	JMP	MDD

DAM:	LXI	H,AIMSTR	;write 'aiming'
MDD:	CALL	PUTSTR_FUNC
	RET

DRM:	LXI	H,RDYSTR	;write 'ready'
	CALL	PUTSTR_FUNC
	MVI	A,26
	CALL	SET_CURSOR_POSITION_FUNC	;cursor to aimstar position

	LDA	READY_AIM_FLAG	;if aim on, display '*'
	ORA	A
	JNZ	D_STAR
	LXI	H,NOSTAR	;if not, blank star
	CALL	PUTSTR_FUNC
	RET

D_STAR:	LXI	H,STARSTR
	CALL	PUTSTR_FUNC
	RET
STARSTR:	DB	'*$'
NOSTAR:		DB	' $'




	;***TOTAL_TIME_DISPLAY_FUNC***
	;displays total exposure time
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - BINASC_FUNC, WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
TOTAL_TIME_DISPLAY_FUNC:

;note: we drop tenths of secs from display, BUT retain the info in
;total_time, which is ten times actual seconds

	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home

	LXI	H,TOTAL_TIME_STR;display "total exposure time'
	CALL	PUTSTR_FUNC

	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;second line

	LHLD	TOTAL_TIME	;get seconds count
	MOV	A,L	;zero?
	ORA	H
	JNZ	SNZ	;if not display value

	LXI	H,SZSTR	;output '0 sec'
	CALL	PUTSTR_FUNC
	JMP	PDDONE

SNZ:	SHLD	TEMP0	;put in temp for bcd conversion
	LXI	H,0
	SHLD	TEMP1
	CALL	BINASC_FUNC

	MVI	A,0FFH	;set to leading zero blanking
	STA	LZERO


	LXI	H,BINTEMP+4	;point to 5th l.s.d.
	MVI	C,4	;digits 1234 may be blanked, loop

TT_LOOP:MOV	A,M	;get digit
	CPI	30H	;if non-zero, reset leading zero flag
	JZ	NOLFLAG
	MVI	A,0	;if not, reset lzero flag
	STA	LZERO

			;if all more significant digits are
NOLFLAG:		;zero, then blank, else display as is
	LDA	LZERO
	ORA	A
	MOV	A,M	;restore character
	JZ	DISPLAY_AS_IS

	MOV	A,M
	CPI	30H	;if zero, change to space char.
	JNZ	DISPLAY_AS_IS
	MVI	A,20H

DISPLAY_AS_IS:
	PUSH	B	;save count
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	POP	B
	INX	H	;next digit
	DCR	C
	JNZ	TT_LOOP

	LXI	H,TOTAL_TIME_STR2	;display 'seconds'
	CALL	PUTSTR_FUNC		;out to display
PDDONE:	RET

TOTAL_TIME_STR:	DB	'TOTAL EXPOSURE TIME:$'
TOTAL_TIME_STR2:DB	' SECONDS$'
SZSTR:		DB	'   0 SECONDS$'



	;***TOTAL_PULSES_DISPLAY_FUNC***
	;displays total pulses (TOTAL pulses = #####)
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - binasc, WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
TOTAL_PULSES_DISPLAY_FUNC:
	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home

	LXI	H,PULSESTR	;point to string 'TOTAL pulses ='
	CALL	PUTSTR_FUNC	;out to display

	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;second line

	LHLD	TOTAL_PULSES1	;get pulses count (hi word)
	MOV	A,L	;zero?
	ORA	H
	JNZ	TPNZ	;if not display value

	LHLD	TOTAL_PULSES	;get pulses count (lo word)
	MOV	A,L	;zero?
	ORA	H
	JNZ	TPNZ	;if not display value

	LXI	H,TPZSTR	;output '0 '
	CALL	PUTSTR_FUNC
	JMP	TPD_DONE

TPNZ:
	LHLD	TOTAL_PULSES	;get total pulses
	SHLD	TEMP0	;put in temp for bcd conversion
	LHLD	TOTAL_PULSES1	;get upper word
	SHLD	TEMP1
	CALL	BINASC_FUNC

	MVI	A,0FFH	;set to leading zero blanking
	STA	LZERO

	LXI	H,BINTEMP+3	;point to 6th l.s.d.
	MVI	C,6	;digits 12345 may be blanked, loop

TPD_LOOP:
	MOV	A,M	;get digit
	CPI	30H	;if non-zero, reset leading zero flag
	JZ	TPD_NOLFLAG
	MVI	A,0	;if not, reset lzero flag
	STA	LZERO

			;if all more significant digits are
TPD_NOLFLAG:		;zero, then blank, else display as is
	LDA	LZERO
	ORA	A
	MOV	A,M	;restore character
	JZ	TPD_DISPLAY_AS_IS

	MOV	A,M
	CPI	30H	;if zero, change to space char.
	JNZ	TPD_DISPLAY_AS_IS
	MVI	A,20H

TPD_DISPLAY_AS_IS:
	PUSH	B	;save count
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	POP	B
	INX	H	;next digit
	DCR	C
	JNZ	TPD_LOOP

TPD_DONE:
	RET

TPZSTR:		DB	'     0$'

	;***TOTAL_JOULES_DISPLAY_FUNC***
	;displays total JOULES (TOTAL ENERGY = #####.##j)
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - binasc, WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
TOTAL_JOULES_DISPLAY_FUNC:
	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home

	LXI	H,JOULESTR	;point to string 'TOTAL ENERGY ='
	CALL	PUTSTR_FUNC	;out to display

	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;2nd line

	LHLD	TEVAL1	;get joules count (hi word)
	MOV	A,L	;zero?
	ORA	H
	JNZ	TJNZ	;if not display value

	LHLD	TEVAL	;get joules count (lo word)
	MOV	A,L	;zero?
	ORA	H
	JNZ	TJNZ	;if not display value

	LXI	H,TJZSTR	;output '0 '
	CALL	PUTSTR_FUNC
	JMP	TJD_DONE

TJNZ:	LHLD	TEVAL	;get total energy
	SHLD	TEMP0	;put in temp for bcd conversion
	LHLD	TEVAL1
	SHLD	TEMP1
	CALL	BINASC_FUNC


	MVI	A,0FFH	;set to leading zero blanking
	STA	LZERO

	LXI	H,BINTEMP+1	;point to 6th l.s.d.
	MVI	C,5	;digits 12345 may be blanked, loop

TJD_LOOP:
	MOV	A,M	;get digit
	CPI	30H	;if non-zero, reset leading zero flag
	JZ	TJD_NOLFLAG
	MVI	A,0	;if not, reset lzero flag
	STA	LZERO

			;if all more significant digits are
TJD_NOLFLAG:		;zero, then blank, else display as is
	LDA	LZERO
	ORA	A
	MOV	A,M	;restore character
	JZ	TJD_DISPLAY_AS_IS

	MOV	A,M
	CPI	30H	;if zero, change to space char.
	JNZ	TJD_DISPLAY_AS_IS
	MVI	A,20H

TJD_DISPLAY_AS_IS:
	PUSH	B	;save count
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	POP	B
	INX	H	;next digit
	DCR	C
	JNZ	TJD_LOOP

			;truncate number so weenie marketing types
			;don't get headaches trying to understand fractions

	LXI	H,TESTR	;point to string 'j'
	CALL	PUTSTR_FUNC	;out to display

TJD_DONE:	RET


PULSESTR:	DB	'TOTAL PULSES = $'
JOULESTR:	DB	'TOTAL JOULES =$'
TESTR:		DB	' JOULES$'
TJZSTR:		DB	'    0 JOULES$'



	;***TOTAL_HOURS_DISPLAY***
	;displays total system hours (TOTAL HOURS = #####)
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - binasc, WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
TOTAL_HOURS_DISPLAY:
	CALL	RESET_DISPLAY_FUNC

	LXI	H,HOURS_STR1	;point to string 'system hours'
	CALL	PUTSTR_FUNC	;out to display

	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor

	LHLD	S_HOURS      	;zero is special case due to leading 0 blanking
	MOV	A,H
	ORA	L	;if zero, display 0 plus "hours"
	JNZ	NORMAL_HOURS

	MVI	A,'0'
	CALL	WRITE_TO_DISPLAY_FUNC
	LXI	H,HOURS_STR2	;point to string 'hours'
	CALL	PUTSTR_FUNC	;out to display
	RET

NORMAL_HOURS:
	LHLD	S_HOURS      ;load hours into temp
        SHLD	TEMP0
        LXI	H,0
        SHLD	TEMP1
        CALL	BINASC_FUNC	;convert it

	MVI	A,0
	STA	LZERO	;reset leading zero flag

	LXI	H,BINTEMP+4	;point to m.s.d. (of 5)
	MVI	C,5	;count for five digits

S_H_D_LOOP:
	MOV	A,M	;get 'tens of thousands' digit
	CPI	30H	;if zero, change to space char.
	JNZ	S_H_NZP	;if not, set lzero flag

	LDA	LZERO	;if digit already displayed don't suppress 0
	ORA	A
	JNZ	S_H_NZROP
	MVI	A,20H
	MOV	M,A
	JMP	S_H_NZROP

S_H_NZP:
	MVI	A,0FFH
	STA	LZERO	;set flag , non zero char.

S_H_NZROP:
	MOV	A,M	;get char.
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit
	DCR	C	;last digit?
	JNZ	S_H_D_LOOP

	LXI	H,HOURS_STR2	;point to string 'hours'
	CALL	PUTSTR_FUNC	;out to display
	RET

HOURS_STR1:	DB	'SYSTEM HOURS =$'
HOURS_STR2:	DB	' HOURS        $'







	;***THDISPL***
	;displays temperature in farenheit
	;called by -
	;calling parameters - temp. in memory location "therm"
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC, mult, BINASC_FUNC
THDISPL:MVI	A,0
	CALL	SET_CURSOR_POSITION_FUNC	;first display line
	LXI	H,THRMSTR	;output 'temperature='
	CALL	PUTSTR_FUNC

	LDA	TMPSTAT	;get temperature value
	MVI	H,0
	MOV	L,A
	SHLD	TEMP0	;put in temp for bcd conversion
	LXI	H,0
	SHLD	TEMP1
	CALL	BINASC_FUNC

	MVI	A,0
	STA	LZERO	;reset leading zero flag

	LXI	H,BINTEMP+6	;point to 3rd l.s.d.
	MVI	C,3	;count for three digits

THLOOP:	MOV	A,M	;get 'hundreds' digit
	CPI	30H	;if zero, change to space char.
	JNZ	NZET	;if not, set lzero flag

	LDA	LZERO	;if digit already displayed don't suppress 0
	ORA	A
	JNZ	NZEROET
	MVI	A,20H
	MOV	M,A
	JMP	NZEROET

NZET:	MVI	A,0FFH
	STA	LZERO	;set flag , non zero char.

NZEROET:MOV	A,M	;get char.
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit
	DCR	C	;last digit?
	JNZ	THLOOP

	LXI	H,DEGSTR	;point to string ' f'
	CALL	PUTSTR_FUNC	;out to display
	CALL	THC_FUNC	;display centigrade too
	RET

THRMSTR:DB	'TEMP = $'
DEGSTR:	DB	' F, $'


	;***THC_FUNC***
	;displays temperature in centigrade
	;called by -
	;calling parameters - temp. in memory location "therm"
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC, mult, BINASC_FUNC
THC_FUNC:	LDA	TMPSTAT	;get temperature value
	SBI	32	;convert to centigrade
	RC		;return if < 0

	MVI	C,10	;*10 to use integer arithmetic
	CALL	MULT_FUNC	;acc * c reg = hl reg

	XCHG		;prep for divide by 18
	LXI	B,18	;de / bc = de rem bc
	CALL	DIVIDE_FUNC

	MVI	A,9	;round off remainder
	CMP	C
	JNC	RNDNC	;remainder < 1/2 divisor
	INX	D	;round up if > 1/2 divisor

RNDNC:	MVI	H,0
	MOV	L,E
	SHLD	TEMP0	;put in temp for bcd conversion
	LXI	H,0
	SHLD	TEMP1
	CALL	BINASC_FUNC

	MVI	A,0
	STA	LZERO	;reset leading zero flag

	LXI	H,BINTEMP+6	;point to 3rd l.s.d.
	MVI	C,3	;count for three digits

THCLOOP:MOV	A,M	;get 'hundreds' digit
	CPI	30H	;if zero, change to space char.
	JNZ	NZETC	;if not, set lzero flag

	LDA	LZERO	;if digit already displayed don't suppress 0
	ORA	A
	JNZ	NZEROC
	MVI	A,20H
	MOV	M,A
	JMP	NZEROC

NZETC:	MVI	A,0FFH
	STA	LZERO	;set flag , non zero char.

NZEROC:	MOV	A,M	;get char.
	CALL	WRITE_TO_DISPLAY_FUNC	;out to display
	INX	H	;next digit
	DCR	C	;last digit?
	JNZ	THCLOOP

	LXI	H,CDEGSTR	;point to string ' c'
	CALL	PUTSTR_FUNC	;out to display
	RET

CDEGSTR:DB	' C$'




	;***REVISION_DISPLAY_FUNC***
	;displays firmware revision level
	;called by - main
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls -WRITE_TO_DISPLAY_FUNC, PUTSTR_FUNC
REVISION_DISPLAY_FUNC:
	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home

	LXI	H,REV+1	;point to revision header (past '$')
	CALL	PUTSTR_FUNC
	RET

	;***RESTORE_DISPLAY_FUNC***
	;restores display to "normal"
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC, edispl, tedispl, pdispl
RESTORE_DISPLAY_FUNC:
	CALL	RESET_DISPLAY_FUNC
	CALL	OUTPUT_DISPLAY_FUNC	;restore display
	CALL	REPRATE_DISPLAY_FUNC

	LDA	MODEFLG	;if in "READY", display site treat count
	CPI	'R'
	JZ	RD_SITE

	MVI	A,13	;fiber type position
	CALL	SET_CURSOR_POSITION_FUNC

	LDA	INT_CARD_STATUS
	CPI	'P'
	RZ		;if card 'pending', leave fiber fields blank

	CPI	'W'
	JZ	CARD_OK_DISPLAY	;if card ok, display fiber type,


	LDA	INT_CARD_STATUS
	CPI	'Y'
	JZ	CARD_OK_DISPLAY	;if card ok, display fiber type,
			;mode, proc count

	CPI	'D'
	JZ	CARD_OK_DISPLAY	;if card decrementing, ok, display fiber type,
			;mode, proc count

	CPI	'Z'	;is it empty?
	JZ	CARD_EMPTY_DISPLAY

	CPI	'V'	;is it invalid?
	JZ	CARD_INV_DISPLAY

	CPI	'R'	;is it not present?
	JZ	CARD_NPRES_DISPLAY	;display 'insert card'

	JMP	CARD_ERROR_DISPLAY	;illegal code

CARD_OK_DISPLAY:
	MVI	A,14	;fiber type position
	CALL	SET_CURSOR_POSITION_FUNC

	LXI	H,FTSTR_CURR
	CALL	PUTSTR_FUNC
	JMP	RD_LINE2

RD_SITE:CALL	SITE_TIME_DISPLAY_FUNC

RD_LINE2:
	CALL	MODE_DISPLAY_FUNC	;2nd line

	MVI	A,29	;fiber type position
	CALL	SET_CURSOR_POSITION_FUNC

	LXI	H,RDISP_PCSTR	;proc. count
	CALL	PUTSTR_FUNC


;Display PROC = x of x

;Special Case!!!:
;if procedures = 0, but current (last) procedure still "alive"
;can't use usual code, because that will say "6 of 5" or "2 of 1"

	LDA	PROC_COUNT_CURR	;special case if = 0
	CPI	0
	JNZ	PCD_NORM

	LDA	TOTAL_PROCS_CURR;display "last" i.e. 5 of 5
	ADI	30H	;display  (ASCII)
	CALL	WRITE_TO_DISPLAY_FUNC
	JMP	PCD2

PCD_NORM:
	LDA	PROC_COUNT_DISPLAYED	;subtract procs remaining from total allowed
	MOV	C,A
	LDA	TOTAL_PROCS_CURR
	SUB	C
	ADI	1	;so if e.g. 8 left 9 - 8= 1 +1 = 2 of 9
	ADI	30H	;display current proc (ASCII)
	CALL	WRITE_TO_DISPLAY_FUNC

PCD2:	LXI	H,OFSTR
	CALL	PUTSTR_FUNC

	LDA	TOTAL_PROCS_CURR	;display total procs allowed
	ADI	30H
	CALL	WRITE_TO_DISPLAY_FUNC
	RET

OFSTR:	DB	' of $'


CARD_EMPTY_DISPLAY:
	MVI	A,15	;card empty position
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDEMPSTR1
	CALL	PUTSTR_FUNC

	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	CALL	MODE_DISPLAY_FUNC

	MVI	A,35	;card empty position 2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDEMPSTR2
	CALL	PUTSTR_FUNC
	RET

CARD_INV_DISPLAY:
	MVI	A,13	;card invalid position
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDINVSTR1
	CALL	PUTSTR_FUNC

	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	CALL	MODE_DISPLAY_FUNC

	MVI	A,33	;card invalid position 2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDINVSTR2
	CALL	PUTSTR_FUNC
	RET


CARD_NPRES_DISPLAY:
	MVI	A,14	;insert card position
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDINSSTR1
	CALL	PUTSTR_FUNC

	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	CALL	MODE_DISPLAY_FUNC

	MVI	A,34	;insert card position 2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDINSSTR2
	CALL	PUTSTR_FUNC
	RET

CARD_ERROR_DISPLAY:
	MVI	A,14	;insert card position
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDERRSTR1
	CALL	PUTSTR_FUNC

	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	CALL	MODE_DISPLAY_FUNC

	MVI	A,34	;insert card position 2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,CARDERRSTR2
	CALL	PUTSTR_FUNC


;patch
;	MVI	A,' '
;	CALL	WRITE_TO_DISPLAY_FUNC

;	LDA	INT_CARD_STATUS
;	CALL	WRITE_TO_DISPLAY_FUNC
;patch
	RET


RDISP_PCSTR:	DB	'USE= $'
CARDEMPSTR1:	DB	'CARD$'
CARDEMPSTR2:	DB	'EMPTY$'
CARDINVSTR1:	DB	'INVALID$'
CARDINVSTR2:	DB	'CARD$'
CARDINSSTR1:	DB	'INSERT$'
CARDINSSTR2:	DB	'CARD$'
CARDERRSTR1:	DB	'CARD$'
CARDERRSTR2:	DB	'ERROR$'

RDPS_STR:	DB	' S $'
RDPL_STR:	DB	' L $'



	;***INITIALIZE_DISPLAY_FUNC***
	;REALLY resets display
	;causes dimming in new (2004) Noritake displays, so only used on power-up
	;called by - init
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC
INITIALIZE_DISPLAY_FUNC:
	MVI	A,1BH	;escape sequence
	CALL	WRITE_TO_DISPLAY_FUNC
	MVI	A,'I'	;initialize
	CALL	WRITE_TO_DISPLAY_FUNC
			;wait 1200 uSec for display to reset
	PUSH	D
	LXI	D,800
RSD_LOOP:
	DCX	D
	MOV	A,E
	ORA	D
	JNZ	RSD_LOOP
	POP	D
	MVI	A,16H	;set cursor to invisible
	CALL	WRITE_TO_DISPLAY_FUNC
	RET

	;***RESET_DISPLAY_FUNC***
	;resets display
	;REAL reset (see above) causes dimming in new (2004) Noritake displays,
	;so this routine clears, and sets cursor to position 0 (top left)
	;called by -
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC
RESET_DISPLAY_FUNC:
	MVI	A,0EH	;clear display
	CALL	WRITE_TO_DISPLAY_FUNC
	MVI	A,0	;cursor top left
	CALL	SET_CURSOR_POSITION_FUNC
	RET


	;***SET_CURSOR_POSITION_FUNC***
	;places cursor at address specified in acc.
	;called by -
	;calling parameters - cursor position in accumulator
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC
SET_CURSOR_POSITION_FUNC:
	PUSH	PSW	;save position byte
	MVI	A,1BH	;escape sequence
	CALL	WRITE_TO_DISPLAY_FUNC
	MVI	A,'H'	;initialize
	CALL	WRITE_TO_DISPLAY_FUNC
	POP	PSW	;restore position byte
	CALL	WRITE_TO_DISPLAY_FUNC
	RET


	;***PUTSTR_FUNC***
	;outputs string to display
	;called by -
	;calling parameters - hl point to string
	;returns - none
	;registers used - all
	;calls - WRITE_TO_DISPLAY_FUNC
PUTSTR_FUNC:	MOV	A,M	;get char
	CPI	'$'	;end of message?
	RZ
	CALL	WRITE_TO_DISPLAY_FUNC
	INX	H	;next char
	JMP	PUTSTR_FUNC

	;***WRITE_TO_DISPLAY_FUNC***
	;outputs byte to display
	;called by -
	;calling parameters - byte in acc.
	;returns - none
	;registers used - a reg
	;calls - none
WRITE_TO_DISPLAY_FUNC:
	PUSH	PSW	;store data
WTD_LOOP:
	IN	DISPLAY	;wait for not busy
	ANI	1
	JNZ	WTD_LOOP
	POP	PSW	;restore data
	OUT	DISPLAY
	RET

	;***BINASC_FUNC***
	;converts binary number 0 - 999,999,999 to 9 digit ascii bcd string
	;called by - d256, lcount, tcdisp, tvdisp
	;calling parameters = binary number in temp0, temp1
	;returns = 9 digit bcd string in bintemp, a ram buffer area
	;calls - digit
	;registers used = all
BINASC_FUNC: LXI	H,BINTEMP	;buffer
	PUSH	H
	LXI	H,05F5H	;load 100,000,000 in digit buffer
	SHLD	D1	;ms word
	LXI	H,0E100H	;ls word
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,00098H	;load 10,000,000 in digit buffer
	SHLD	D1	;ms word
	LXI	H,09680H	;ls word
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,0000FH	;load 1,000,000 in digit buffer
	SHLD	D1	;ms word
	LXI	H,04240H	;ls word
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,00001H	;load 100,000 in digit buffer
	SHLD	D1	;ms word
	LXI	H,086A0H	;ls word
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,00000H	;load 10,000 in digit buffer
	SHLD	D1	;ms word
	LXI	H,10000	;ls word
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,1000	;load 1,000 in digit buffer
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,100	;load 100 in digit buffer
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,10	;load 10 in digit buffer
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit

	PUSH	H
	LXI	H,1	;load 1 in digit buffer
	SHLD	D0
	POP	H
	CALL	DIGIT_FUNC	;convert digit
	RET

	;***DIGIT_FUNC***
	;puts bcd digits into memory
	;called by - BINASC_FUNC
	;calling parameters - decimal digit position in bc
	;returns - none
	;registers used - all
	;calls - none
DIGIT_FUNC:	MVI	M,30H	;init ascii digit
DI0:	XCHG		;save bintemp pointer
	LHLD	D0	;get ls digit word
	XCHG

	LDA	TEMP0	;get ls counter byte
	SUB	E	;subtract digit
	STA	TEMP0

	LDA	TEMP0+1	;get next counter byte
	SBB	D	;subtract digit
	STA	TEMP0+1

	XCHG		;save bintemp pointer
	LHLD	D1	;get next digit word
	XCHG

	LDA	TEMP1	;get ls counter byte
	SBB	E	;subtract digit
	STA	TEMP1

	LDA	TEMP1+1	;get next counter byte
	SBB	D	;subtract digit
	STA	TEMP1+1

	JC	NEXTD	;done, next digit

	INR	M	;bump bcd (ascii) digit
	JMP	DI0
			;restore temp after overflow subtraction

NEXTD:	XCHG		;save bintemp pointer
	LHLD	D0	;get ls digit word
	XCHG

	LDA	TEMP0	;get ls counter byte
	ADD	E	;add digit
	STA	TEMP0

	LDA	TEMP0+1	;get next counter byte
	ADC	D	;add digit
	STA	TEMP0+1

	XCHG		;save bintemp pointer
	LHLD	D1	;get next digit word
	XCHG

	LDA	TEMP1	;get ls counter byte
	ADC	E	;add digit
	STA	TEMP1

	LDA	TEMP1+1	;get next counter byte
	ADC	D	;add digit
	STA	TEMP1+1

	INX	H
	RET

	END
