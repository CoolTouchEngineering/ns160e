;$PG
$PL=59
;$TL(NS160E TREAT_FUNC.ASM SOURCE FILE)
	INCLUDE "INCLUDE.H"
;$Header: C:/Dosland/ns160e/rcs/TREAT_FUNC.ASM 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------
; ROUTINE NAMES:

	GLOBAL	TREAT_FUNC
	GLOBAL	RECHARGE_FUNC

; PROJECT: NS160E
;--------------------------------------------------------- 


	DEFSEG	TREATSEG,CLASS=CODE
	SEG	TREATSEG

;$SB(TREATMENT SUBROUTINE)


; DATA INPUTS: 
	EXTERN	FIBSTAT
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE0

	EXTERN	SEC_PROC_SYNCH
	EXTERN	PROC_COUNT_CURR

; DATA OUTPUTS
	EXTERN	CAPVAL
	EXTERN	DACVAL
	EXTERN	TFCNT
	EXTERN	RCNT
	EXTERN	RTEMP
	EXTERN	REPRATE
	EXTERN	RR10
	EXTERN	CBYTE
	EXTERN	RUNNING_STATUS_COUNTER
	EXTERN	EVREQ
	EXTERN	EMVALA
	EXTERN	EMVALB
	EXTERN	AVVAL
	EXTERN	AVVALB
	EXTERN	EBUFFA
	EXTERN	ABPOINA
	EXTERN	BUFCNTA
	EXTERN	ECNTA
	EXTERN	EBUFFB
	EXTERN	ABPOINB
	EXTERN	BUFCNTB
	EXTERN	ECNTB
	EXTERN	EBUFFE
	EXTERN	ABPOINE
	EXTERN	BUFCNTE
	EXTERN	ECNTE
	EXTERN	EDCNT
	EXTERN	DISPLAY_TOGGLE
	EXTERN	FIVEMIN
	EXTERN	MINUS20
	EXTERN	MEASURED_ENERGY_OK
	EXTERN	TOTAL_TIME
	EXTERN	PROC_TIME
	EXTERN	LOCAL_PULSES
	EXTERN	LOCAL_PULSES1
	EXTERN	LOCAL_TIME

	EXTERN	SHUTTER_FLAG
	EXTERN	ALARM_CYCLE
	EXTERN	ALARM_TIME

	EXTERN	SITE_TIME

	EXTERN	LP_20
	EXTERN	LP_201
	EXTERN	LP_30
	EXTERN	LP_301
	EXTERN	LP_40
	EXTERN	LP_401
	EXTERN	LP_50
	EXTERN	LP_501

	EXTERN	SC_20
	EXTERN	SC_201
	EXTERN	SC_30
	EXTERN	SC_301
	EXTERN	SC_40
	EXTERN	SC_401
	EXTERN	SC_50
	EXTERN	SC_501

	EXTERN	STS_RRVAL
	EXTERN	PS_TEMP

	EXTERN	PROC_COUNT_DISPLAYED
	EXTERN	PROC_COUNT_CURR



; ROUTINES CALLED BY: 
; ROUTINES CALLED
	EXTERN	DACOUT_FUNC
	EXTERN	WARMUP_FIRE_FUNC
	EXTERN	HOT_FIRE_FUNC
	EXTERN	POLL_KEYBOARD_FUNC
	EXTERN	KEYBOARD_SCAN_LINE1_FUNC
	EXTERN	TREAT_TEMP_CHECK_FUNC
	EXTERN	TCHECK_FUNC
	EXTERN	EMON_EXCESS_CHECK_FUNC
	EXTERN	EMON_TOO_LOW_CHECK_FUNC
	EXTERN	OUTPUT_DISPLAY_FUNC
	EXTERN	WATTS_DISPLAY_FUNC
	EXTERN	DUMP_FUNC
	EXTERN	SCLOSE_FUNC
	EXTERN	FTREL_FUNC
	EXTERN	RESTORE_DISPLAY_FUNC
	EXTERN	DIVIDE_FUNC
	EXTERN	MULT_FUNC
	EXTERN	ERROR
	EXTERN	SYNCH_TO_WD_FUNC_FUNC
	EXTERN	DEBOUNCE_DELAY_5MS_FUNC
	EXTERN	AVG_FUNC
	EXTERN	AVGB_FUNC

	EXTERN	SITE_TIME_DISPLAY_FUNC

	EXTERN	COMPARE_16_FUNC
	EXTERN	FIND_ACCOUNT_FUNC
	EXTERN	UPDATE_ACCOUNT_PROCEDURES_FUNC
	EXTERN	PROC_ALIVE
	EXTERN	DELAY_100MS_FUNC
	EXTERN 	TREATMENT_GREATER_30_SECS


$EJECT

	;*************************
	;*     LASER OUTPUT	;*
	;*      SUBROUTINES     ;*
	;*************************

;$AP
	;***TREAT_FUNC***
	;treat - treatment subroutine
	;called by - main loop
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - fire, avg, avgb, vadj, FATT_FUNC, 
TREAT_FUNC:	IN	SSTATB	;check safety shutter 
	ANI	SHMASK
	CPI	2	;2 = shutter closed 
	MVI	A,1	;in case
	JNZ	ERROR	;if not closed, error
	
	LDA	FIBSTAT	;fiber?
	CPI	'N'	;'s''o' = installed 'n' = not installed
	JZ	TREAT_OVER
	
	LDA	REPRATE	;fire for 1/2 second to stabilize
	ORA	A	;clear carry if any
	RAR		;divide by 2
	STA	TFCNT	;init number of fires   
	
	CALL	SYNCH_TO_WD_FUNC_FUNC	;synch to watch dog (use safety counter as flag)

	LDA	RCNT	;load reprate countdown
	STA	RTEMP

	LHLD	DACVAL
	CALL	DACOUT_FUNC	;output rod warmup value to hi voltage p.s.
	LDA	CBYTE
	ANI	PSON	;enable hi voltage p.s.
	STA	CBYTE
	OUT	CPORT

WLOOP:	CALL	WARMUP_FIRE_FUNC	;"real" fire without pulse counter
	LDA	TFCNT
	DCR	A
	STA	TFCNT
	
	LHLD	RUNNING_STATUS_COUNTER	;bump safcnt
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER
	IN	SSTATB	;check safety shutter 
	ANI	SHMASK
	CPI	2	;2 = shutter closed 
	MVI	A,1	;in case
	JNZ	ERROR	;if not closed, error

	IN	SSTATA	;is foot switch active?
	ANI	FTMASK
	CPI	2
	JZ	CNTWRM1	;if so, continue warm-up

	LDA	EVREQ	;if not, return
	STA	EMVALA	;normalize e display
	STA	EMVALB
	STA	AVVAL
	STA	AVVALB
	JMP	TREAT_OVER	;and return

*******************************************************************************
;check safety interlock

CNTWRM1:IN	SSTATA	;check system status
	ANI	INTERLOCK	;check interlock
	JZ	CNTWRM2	;if o.k., continue
	LDA	EVREQ	;if not, return
	STA	EMVALA	;normalize e display
	STA	EMVALB
	STA	AVVAL
	STA	AVVALB
	JMP	TREAT_OVER	;and return
*******************************************************************************


CNTWRM2:LDA	TFCNT
	ORA	A
	JZ	TREAT_NOW	;if 10 pulses done, treat
	CALL	RECHARGE_FUNC	;start charging hvps
	JMP	WLOOP

TREAT_NOW:		;open shutter

	LDA	CBYTE	;set safety shutter drive signal
	ANI	SSO	;active (lo)
	STA	CBYTE
	OUT	CPORT

	MVI	A,'Y'	;set shutter open flag
	STA	SHUTTER_FLAG
	MVI	A,'N'	;set shutter cycle flag w.d. will change to on cycle 
	STA	ALARM_CYCLE
	MVI	A,1	;this will cause immediate BEEP_FUNC by w.d.
	STA	ALARM_TIME

	CALL	RECHARGE_FUNC	;start charging hvps

	MVI	C,7	
SW_LOOP:CALL	DEBOUNCE_DELAY_5MS_FUNC	;give shutter a chance (35ms)
	DCR	C
	JNZ	SW_LOOP

	LXI	H,EBUFFA	;init energy average buffer 'a'
	SHLD	ABPOINA
	MVI	C,50	;assume max count
BAL:	MVI	M,0
	INX	H
	DCR	C
	JNZ	BAL  
	XRA	A          	
	STA	BUFCNTA
	STA	ECNTA

	LXI	H,EBUFFB	;init energy average buffer 'b'
	SHLD	ABPOINB
	MVI	C,50	;assume max count
BBL:	MVI	M,0
	INX	H
	DCR	C
	JNZ	BBL  
	XRA	A          	
	STA	BUFCNTB
	STA	ECNTB

	LXI	H,EBUFFE	;init energy average buffer 'ext'
	SHLD	ABPOINE
	MVI	C,50	;assume max count
BEL:	MVI	M,0
	INX	H
	DCR	C
	JNZ	BEL  
	XRA	A          	
	STA	BUFCNTE
	STA	ECNTE

	LDA	REPRATE	;init display counter
	ORA	A	;clear any carry
	RAR		;half the rep rate (displays every half sec)
	STA	EDCNT

	LXI	H,0	;init pulses per this treat count
	SHLD	LOCAL_PULSES
	SHLD	LOCAL_PULSES1


TREAT_INNER_LOOP:
	CALL	HOT_FIRE_FUNC	;"real" fire, reset rep rate counter

	LHLD	LOCAL_PULSES	;bump local pulses count (used to calc PROC_TIME)
	LXI	D,1	;use DAD to set carry
	DAD	D	;can't use "TOTAL_PULSES" cause can be reset by user
	SHLD	LOCAL_PULSES

	JNC	NO_LP_BUMP	;if no rollover, continue

	LHLD	LOCAL_PULSES1	;increment upper word
	INX	H
	SHLD	LOCAL_PULSES1

NO_LP_BUMP:

	CALL	TREAT_TEMP_CHECK_FUNC ;quick check of coolant temperature

	IN	SSTATA	;is foot switch still active?
	ANI	FTMASK
	CPI	2
	JNZ	TREAT_OVER	;if not, return

*******************************************************************************
;check safety interlock
	IN	SSTATA	;check system status
	ANI	INTERLOCK	;check interlock
	JNZ	TREAT_OVER	;if not o.k., return
*******************************************************************************

	LDA	FIBSTAT	;fiber?
	CPI	'N'	;'s''o' = installed 'n' = not installed
	JZ	TREAT_OVER

	CALL	POLL_KEYBOARD_FUNC
	LDA	KEYBOARD_DEBOUNCE_STORAGE0
	CPI	0FFH	;no keys pressed?
	JNZ	TREAT_OVER	;if key pressed, return

	CALL	KEYBOARD_SCAN_LINE1_FUNC
	CPI	0FFH	;no keys pressed?
	JNZ	TREAT_OVER	;if key pressed, return
KB_DONE:
	LXI	H,FIVE_MIN_TIMEOUT	;reload five minute timer
	SHLD	FIVEMIN
	CALL	TREAT_TEMP_CHECK_FUNC

	CALL	RECHARGE_FUNC		;start charging pfn
	CALL	AVG_FUNC
	CALL	AVGB_FUNC

	LDA	EDCNT	;display this time?
	DCR	A
	STA	EDCNT
	JZ	T_UPDATE_DISPLAY	;if 0, update power display

	CPI	1		;if edcnt +1, update counter display
	JZ	UPDATE_SITE_DISPLAY	;need to stagger, so 50hz doesn't
				;drop pulses

	CPI	2		;on edcnt=2, calc site count
	JZ	CALC_SITE_TIME		;which will be displayed on edcnt=1

	CPI	3		;on edcnt=3, update exposure totals
	JZ	UPDATE_TIME		;which will be displayed on edcnt=1

	JMP	TREAT_INNER_LOOP


UPDATE_TIME:
	CALL	ADD_PULSES_FUNC
	CALL	ADD_PROC_SECONDS_FUNC
	LXI	H,0	;clear count
	SHLD	LOCAL_PULSES
	SHLD	LOCAL_PULSES1
	JMP	TREAT_INNER_LOOP

CALC_SITE_TIME:
	CALL	SITE_TO_SECONDS_FUNC	;total
	SHLD	SITE_TIME

;now see if we've done 30 secs and card account hasn't been debited
;if >30 secs and NOT debited, debit now

	LDA	SEC_PROC_SYNCH	;if > required seconds in procedure, and procedure
	CPI	'Y'	;debited, this flag = 'y'
	JZ	SP_SYNCHED

	LHLD	PROC_TIME	;get treat seconds during this procedure
	LXI	D,MIN_TREATMENT_PERIOD_30SEC	;value is 10X
	;LXI	D,300	;value is 10X
	CALL	COMPARE_16_FUNC	;see if > 30 secs
	CPI	'L'	;if de (30) less than hl, then decr procedure
	JNZ	SP_SYNCHED	;else, skip

	LDA	PROC_COUNT_CURR	;Decrement Procedure Count
	STA	PROC_COUNT_DISPLAYED	;display "alive" proc til expired or card removed
	ORA	A	;clip at 0 (in case)
	JZ	SP_SYNCHED
	
	DCR	A
	STA	PROC_COUNT_CURR
		
	MVI A,'Y'
	STA TREATMENT_GREATER_30_SECS

	CALL	FIND_ACCOUNT_FUNC	;look at current account
	CALL	UPDATE_ACCOUNT_PROCEDURES_FUNC	;adjust account

	MVI	A,'Y'
	STA	SEC_PROC_SYNCH

SP_SYNCHED:


	JMP	TREAT_INNER_LOOP

UPDATE_SITE_DISPLAY:
	CALL	SITE_TIME_DISPLAY_FUNC
	JMP	TREAT_INNER_LOOP

T_UPDATE_DISPLAY:	
	CALL	EMON_EXCESS_CHECK_FUNC	;test emons for excessive power
	CALL	EMON_TOO_LOW_CHECK_FUNC	;test for insufficient power

	LDA     MINUS20	;if energy reading within window
	MOV     C,A	;display evreq
	LDA	AVVAL	;read - low limit
	CMP	C
	JC	OUTWIND	;if too low, display as is

	LDA	EVREQ	;if reading within window, display requested
	STA	AVVAL
	MVI	A,'Y'	;set display requested flag
	STA	MEASURED_ENERGY_OK
	JMP	INW

OUTWIND:MVI	A,'N'	;reset display requested flag
	STA	MEASURED_ENERGY_OK

INW:	CALL	WATTS_DISPLAY_FUNC
	
DISPLAY_DONE:
	LDA	REPRATE	;reset display counter
	ORA	A	;clear any carry
	RAR		;half the rep rate (displays every half sec)
	STA	EDCNT
	JMP	TREAT_INNER_LOOP		;else, carry on
	
TREAT_OVER:
	LXI	H,0	;zero dac
	CALL	DACOUT_FUNC
	LDA	CBYTE
	ORI	PSOFF	;disable p.s.
	STA	CBYTE
	OUT	CPORT

TO_CONT:CALL	DUMP_FUNC	;zero h.v.p.s.
	CALL	SCLOSE_FUNC

	CALL	EMON_EXCESS_CHECK_FUNC	;test emons for excessive power
	CALL	EMON_TOO_LOW_CHECK_FUNC	;test for insufficient power
	LDA     MINUS20	;if energy reading within window
	MOV     C,A	;display evreq
	LDA	AVVAL	;read - low limit
	CMP	C
	JC	OUTWIN2	;if too low, display as is

	JMP	INW2

OUTWIN2:MVI	A,'N'	;reset display requested flag
	STA	MEASURED_ENERGY_OK

INW2:	CALL	ADD_PULSES_FUNC	;add seconds of treatment to total

	CALL	RESTORE_DISPLAY_FUNC 	;restore normal display

	CALL	FTREL_FUNC	;wait

	RET

RECHARGE_FUNC:
	LHLD	DACVAL	;full voltage reference to p.s.
	CALL	DACOUT_FUNC	;output voltage control level to hi voltage p.s.
	LDA	CBYTE
	ANI	PSON	;enable hi voltage p.s.
	STA	CBYTE
	OUT	CPORT
	RET


ADD_PULSES_FUNC:		;total secs = treatment clock secs + total secs


			;re-writing this code as of rev 1.27
			;to avoid incremental errors in spasmotic
			;ftsw activity, new scheme as follows:

			;to convert # of pulses into time, we need
			;to know reprate. So just dividing total pulses 
			;by current reprate is wrong, cause any number
			;of pulses could have been accumulated at some
			;other reprate. SO:

			;Each reprate has it's own pulse accumulator
			;which AT DISPLAY TIME, we divide by it's corresponding
			;rep rate, add together and THAT'S total time.
			;HOWEVER, we need real time update for PROC_TIME
			;so card gets updated correctly,
			;We do the calcs at display time
			;for total_time, so we don't build up errors. 

			;so in treat routine (here), we just accumulate 
			;pulses in reprate's accumulator
			;no arithmentic til display routine. (exept PROC_TIME)
			;we add proc_time here (real time)

			;WELL! now that the pulses and total_time are
			;in agreement, we need to make the site count 
			;agree also. SO we have duplicate accumulators
			;("SC_xx") for site count that are seperately resetable
			;what fun.

	LDA	REPRATE
	CPI	20
	JNZ	TAP_30

	LHLD	LOCAL_PULSES
	XCHG

	LHLD	LP_20
	DAD	D
	SHLD	LP_20

	JNC	NO_LP20_ROLL

	LHLD	LP_201	;bump upper word
	INX	H
	SHLD	LP_201

NO_LP20_ROLL:

	LHLD	SC_20
	DAD	D
	SHLD	SC_20

	JNC	NO_SC20_ROLL

	LHLD	SC_201	;bump upper word
	INX	H
	SHLD	SC_201

NO_SC20_ROLL:

	LHLD	LOCAL_PULSES1
	XCHG

	LHLD	LP_201
	DAD	D
	SHLD	LP_201

	LHLD	SC_201
	DAD	D
	SHLD	SC_201

	JMP	TAP_CONT

TAP_30:	LDA	REPRATE
	CPI	30
	JNZ	TAP_40

	LHLD	LOCAL_PULSES
	XCHG

	LHLD	LP_30
	DAD	D
	SHLD	LP_30

	JNC	NO_LP30_ROLL

	LHLD	LP_301	;bump upper word
	INX	H
	SHLD	LP_301

NO_LP30_ROLL:

	LHLD	SC_30
	DAD	D
	SHLD	SC_30

	JNC	NO_SC30_ROLL

	LHLD	SC_301	;bump upper word
	INX	H
	SHLD	SC_301

NO_SC30_ROLL:

	LHLD	LOCAL_PULSES1
	XCHG

	LHLD	LP_301
	DAD	D
	SHLD	LP_301

	LHLD	SC_301
	DAD	D
	SHLD	LP_301

	JMP	TAP_CONT

TAP_40:	LDA	REPRATE
	CPI	40
	JNZ	TAP_50

	LHLD	LOCAL_PULSES
	XCHG

	LHLD	LP_40
	DAD	D
	SHLD	LP_40

	JNC	NO_LP40_ROLL

	LHLD	LP_401	;bump upper word
	INX	H
	SHLD	LP_401

NO_LP40_ROLL:

	LHLD	SC_40
	DAD	D
	SHLD	SC_40

	JNC	NO_SC40_ROLL

	LHLD	SC_401	;bump upper word
	INX	H
	SHLD	SC_401

NO_SC40_ROLL:

	LHLD	LOCAL_PULSES1
	XCHG

	LHLD	LP_401
	DAD	D
	SHLD	LP_401

	LHLD	SC_401
	DAD	D
	SHLD	LP_401

	JMP	TAP_CONT



TAP_50:	
	LHLD	LOCAL_PULSES
	XCHG

	LHLD	LP_50
	DAD	D
	SHLD	LP_50

	JNC	NO_LP50_ROLL

	LHLD	LP_501	;bump upper word
	INX	H
	SHLD	LP_501

NO_LP50_ROLL:

	LHLD	SC_50
	DAD	D
	SHLD	SC_50

	JNC	NO_SC50_ROLL

	LHLD	SC_501	;bump upper word
	INX	H
	SHLD	SC_501

NO_SC50_ROLL:

	LHLD	LOCAL_PULSES1
	XCHG

	LHLD	LP_501
	DAD	D
	SHLD	LP_501

	LHLD	SC_501
	DAD	D
	SHLD	LP_501

	JMP	TAP_CONT

TAP_CONT:
	RET



ADD_PROC_SECONDS_FUNC:		;total secs = treatment clock secs + total secs

			;ok, idea here is to divide pulse count by
			;rep rate to get seconds. if we use (reprate/10)
			;we can get an extra digit of precision.
			;so first, let's divide reprate by 10
	LDA	REPRATE
	MOV	E,A
	MVI	D,0
	LXI	B,10
	CALL	DIVIDE_FUNC	;divides de by bc
			;result in de, remainder in bc
	MOV	A,E
	STA	RR10
	
	LXI	H,0	;we need local temp accumulator
	SHLD	LOCAL_TIME
			;ok, that's done, now next problem is 32 bits
			;of pulses with only a 16 bit divide routine.
			;upper word is SMALL. 
			;then process lower word as straight 16 bit divide 
			;and add em all up. whew.
	LHLD	LOCAL_PULSES1
	MOV	C,L	;this is a small number cause each bit = 22 minutes
			;of treatment time

	MOV	A,L	;if zero, no add here
	ORA	A
	JZ	LPA_LOW	

LP1_LOOP:

	LDA	REPRATE	;add 64k/rr for each bit
	CPI	20
	JZ	LP20
	CPI	30
	JZ	LP30
	CPI	40
	JZ	LP40
	CPI	50
	JZ	LP50

LP20:	LXI	D,32768	;64k/2 = 32768
	JMP	LPA_CONT

LP30:	LXI	D,21845	;64k/3 = 21845
	JMP	LPA_CONT
		
LP40:	LXI	D,16383	;64k/4 = 16383
	JMP	LPA_CONT

LP50:	LXI	D,13107	;64k/5 = 13107
	JMP	LPA_CONT

		
LPA_CONT:
	LHLD	LOCAL_TIME	;accumulate local secs
	DAD	D
	SHLD	LOCAL_TIME
	DCR	C	;all bits done?
	JNZ	LP1_LOOP
			
LPA_LOW:		;ok, now divide lower word by rr/10 and add to total time
	LHLD	LOCAL_PULSES
	XCHG		;to de for divide
	LDA	RR10
	MOV	C,A
	MVI	B,0
	CALL	DIVIDE_FUNC

	LDA	RR10	;round off remainder
	ORA	A
	RAR		;rr/10 / 2
	CMP	C
	JNC	NO_LP_RND	;remainder < 1/2 divisor 
	INX	D	;round up if > 1/2 divisor

NO_LP_RND:
	LHLD	LOCAL_TIME	;add local secs lower word to local secs
	DAD	D
	SHLD	LOCAL_TIME

	XCHG		;local time to de to add to total time and procedure time
	
	LHLD	PROC_TIME	;add local secs to total procedure secs
	DAD	D
	SHLD	PROC_TIME

	RET


;virtually identical to routine "pulses_to_seconds" in totals.asm
;need seperate accumulators cause each total is resettable

SITE_TO_SECONDS_FUNC:	
			;ok, idea here is to divide pulse count by
			;rep rate to get seconds. if we use (reprate/10)
			;we can get an extra digit of precision.

	;derive seconds for each reprate acc. and then add
	LXI	H,0
	SHLD	PS_TEMP	;init temp storage

	LHLD	SC_201	;upper word of local pulse count
	MVI	A,20	;i.d. reprate
	CALL	CONVERT_SITE_TO_SECS1_FUNC;lookup upper word value 
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP	;temporary storage
	DAD	D
	SHLD	PS_TEMP

	LXI	B,2	;rr/10 = 2
	LHLD	SC_20
	MVI	A,2
	STA	RR10
	CALL	CONVERT_SITE_TO_SECS_FUNC
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP
	DAD	D
	SHLD	PS_TEMP

	LHLD	SC_301	;upper word of local pulse count
	MVI	A,30	;i.d. reprate
	CALL	CONVERT_SITE_TO_SECS1_FUNC;lookup upper word value 
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP	;temporary storage
	DAD	D
	SHLD	PS_TEMP

	LXI	B,3	;rr/10 = 3
	LHLD	SC_30
	MVI	A,3
	STA	RR10
	CALL	CONVERT_SITE_TO_SECS_FUNC
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP
	DAD	D
	SHLD	PS_TEMP


	LHLD	SC_401	;upper word of local pulse count
	MVI	A,40	;i.d. reprate
	CALL	CONVERT_SITE_TO_SECS1_FUNC;lookup upper word value 
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP	;temporary storage
	DAD	D
	SHLD	PS_TEMP

	LXI	B,4	;rr/10 = 4
	LHLD	SC_40
	MVI	A,4
	STA	RR10
	CALL	CONVERT_SITE_TO_SECS_FUNC
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP
	DAD	D
	SHLD	PS_TEMP

	LHLD	SC_501	;upper word of local pulse count
	MVI	A,50	;i.d. reprate
	CALL	CONVERT_SITE_TO_SECS1_FUNC;lookup upper word value 
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP	;temporary storage
	DAD	D
	SHLD	PS_TEMP

	LXI	B,5	;rr/10 = 5
	LHLD	SC_50
	MVI	A,5
	STA	RR10
	CALL	CONVERT_SITE_TO_SECS_FUNC
	XCHG		;from hl to de to add to total
	LHLD	PS_TEMP
	DAD	D
	SHLD	PS_TEMP
	
	RET



CONVERT_SITE_TO_SECS1_FUNC:
	STA	STS_RRVAL	;save reprate value from caller
	MOV	C,L	;this is a small number cause each bit = 22 minutes
			;of treatment time
	MOV	A,L	;if zero, no add here
	ORA	A
	RZ			

	LXI	H,0	;prep accumulator
	
STS1_LOOP:
	LDA	STS_RRVAL	;add 64k/rr for each bit
	CPI	20
	JZ	STS20
	CPI	30
	JZ	STS30
	CPI	40
	JZ	STS40
	CPI	50
	JZ	STS50

STS20:	LXI	D,32768	;64k/2 = 32768
	JMP	STSA_CONT

STS30:	LXI	D,21845	;64k/3 = 21845
	JMP	STSA_CONT
		
STS40:	LXI	D,16383	;64k/4 = 16383
	JMP	STSA_CONT

STS50:	LXI	D,13107	;64k/5 = 13107
	JMP	STSA_CONT

		
STSA_CONT:
			;accumulate local secs (upper word)
	DAD	D
	DCR	C	;all bits done?
	JNZ	STS1_LOOP
	RET		;return total upper word pulses in hl	


CONVERT_SITE_TO_SECS_FUNC:			
	XCHG		;to de for divide
	CALL	DIVIDE_FUNC	;divide pulses by rr/10

	LDA	RR10	;round off remainder
	ORA	A
	RAR		;rr/10 / 2
	CMP	C
	JNC	NO_STS_RND	;remainder < 1/2 divisor 
	INX	D	;round up if > 1/2 divisor

NO_STS_RND:		;return seconds in hl 
	XCHG		
	RET




	END
