;$PG
$PL=59
;$TL(NS160E RAM.ASM SOURCE FILE)
;$AP

	INCLUDE "INCLUDE.H"
;$Header: C:/Dosland/ns160e/rcs/ram.asm 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------
; ROUTINE NAMES:


; PROJECT: NS160E
; The RAM chip resides at 0xE000 - 0xFFFF (DS1243Y 8K of NVRAM)
;
; Note:
;	INIT_RAM:		; set all variables to default values, init first 2ffh bytes of ram to 0
;---------------------------------------------------------


        DEFSEG  RAMSEG,CLASS=DATA,START=RAM
	SEG 	RAMSEG


; DATA INPUTS:

; DATA OUTPUTS

; ROUTINES CALLED BY:
; ROUTINES CALLED

$EJECT
	;*************************
	;*         RAM          ;*
	;*	                ;*
	;*************************

RAMVARS:
GLOBAL SAFCHK, FIBBER,FBOUNCE, AIM_FORGIVENESS_COUNTER
GLOBAL WD_DIVIDE,LAST_INTLCK,LAST_FTSW,LOCAL_TIME, STS_RRVAL
GLOBAL RR10,CTS_RRVAL,TEST_FIRE_BURST_COUNT
GLOBAL LV_DEBOUNCE, OPTFLG, LAST_DISPLAYED_TEMP
GLOBAL SCFLG, MSEC_COUNT, SEC_COUNT, MIN_COUNT
GLOBAL PBUFF_POINTER, PA_BUFF, PB_BUFF, PE_BUFF

GLOBAL TTA_FLAG, COMM_FORGIVE, CARD_CHECK_TIMER, RC_COMM_TIMEOUT
GLOBAL PMJ_LIMIT20
GLOBAL PMJ_LIMIT30
GLOBAL PMJ_LIMIT40
GLOBAL PMJ_LIMIT50
GLOBAL REPRATE_START, POWER_LIMIT, POWER_START, PROC_COUNT_DISPLAYED, PROC_COUNT_CURR
GLOBAL FCAL_CURR, FTSTR_CURR, SERSTR_CURR, INBUFF_LRC_OK, HANDSHAKE_OK
GLOBAL CHAR_READ_OK0, CHAR_WRITTEN_OK0, CHAR_READ_OK1, CHAR_WRITTEN_OK1, CHAR_READ_OK2, CHAR_WRITTEN_OK2
GLOBAL CARD_COMM_STAT, ACCOUNT_POINTER, FA_POINTER, SAD_POINTER, INT_CARD_STATUS, CARD_STATUS, CARD_PIN
GLOBAL CARD_ERROR_CODE, PROC_TIME, SEC_PROC_SYNCH, AIM_OK_FLAG, AIM_ATT_STAT, KEYBOARD_DEBOUNCE_STORAGE1, TEVAL1

GLOBAL LP_TOGGLE

GLOBAL TMSEC_COUNT
GLOBAL TSEC_COUNT
GLOBAL TMIN_COUNT
GLOBAL EDCNT
GLOBAL LOCAL_PULSES
GLOBAL LOCAL_PULSES1
GLOBAL LP_20
GLOBAL LP_201
GLOBAL LP_30
GLOBAL LP_301
GLOBAL LP_40
GLOBAL LP_401
GLOBAL LP_50
GLOBAL LP_501
GLOBAL PS_TEMP
GLOBAL ETOP
GLOBAL VTR_POINTER
GLOBAL MAP_INCR
GLOBAL UD_COUNTER
GLOBAL TASK_COUNTER
GLOBAL PC_VAL
GLOBAL DTEMP
GLOBAL TIME_SINCE_TFIRE
GLOBAL PDP_FLAG
GLOBAL CARD_OK
GLOBAL SC_20
GLOBAL SC_201
GLOBAL SC_30
GLOBAL SC_301
GLOBAL SC_40
GLOBAL SC_401
GLOBAL SC_50
GLOBAL SC_501
GLOBAL KEYBOARD_DEBOUNCE_STORAGE2
GLOBAL LC0
GLOBAL LC1
GLOBAL P2D0
GLOBAL P2D1
GLOBAL WD_FLAG
GLOBAL MAXBUFF
GLOBAL AE_COUNTER
GLOBAL ESTEP
GLOBAL CAPVAL
GLOBAL MAX_POWER
GLOBAL MJOULES
GLOBAL MINUS10
GLOBAL PLUS10
GLOBAL MINUS20
GLOBAL HIWIND
GLOBAL VTR20
GLOBAL VTR30
GLOBAL VTR40
GLOBAL VTR50
GLOBAL VTR60
GLOBAL SPEEDUP
GLOBAL ERRCODE
GLOBAL EVREQ
GLOBAL EMVALA
GLOBAL EMVALB
GLOBAL AVVALB
GLOBAL EMVALE
GLOBAL PLUS20
GLOBAL READY_AIM_FLAG
GLOBAL AVVAL
GLOBAL DEVREQ
GLOBAL S_HOURS
GLOBAL OTAFLG
GLOBAL SITE_TIME
GLOBAL DAVVAL
GLOBAL TAVVAL
GLOBAL LZERO
GLOBAL D1
GLOBAL D0
GLOBAL CARD_ACCOUNTS
GLOBAL CA_TOP
GLOBAL BINTEMP
GLOBAL YIK
GLOBAL INBUFF
GLOBAL CARDBUFF
GLOBAL DATASTR1
GLOBAL CHAR_COUNTER
GLOBAL SUBSTR_ID
GLOBAL CURRENT_SUBSTR
GLOBAL AB_STRING
GLOBAL AB_LENGTH
GLOBAL AB_CONVERSION
GLOBAL TOTAL_PROCS_CURR
GLOBAL CARD_TYPE
GLOBAL TEMP0
GLOBAL TEMP1
GLOBAL CTS_ERR_FLAG
GLOBAL DEBUG_FLAG
GLOBAL NP_ATT_FLAG
GLOBAL NR_ATT_FLAG
GLOBAL PB_ATT_FLAG
GLOBAL ZP_ATT_FLAG
GLOBAL NEW_CARD
GLOBAL PROC_ALIVE
GLOBAL AVVALE
GLOBAL TMODEFLG
GLOBAL TFCNT
GLOBAL DACVAL
GLOBAL EBUFFA
GLOBAL ABPOINA
GLOBAL BUFCNTA
GLOBAL ECNTA
GLOBAL EBUFFB
GLOBAL ABPOINB
GLOBAL BUFCNTB
GLOBAL ECNTB
GLOBAL EBUFFE
GLOBAL ABPOINE
GLOBAL BUFCNTE
GLOBAL ECNTE
GLOBAL CAL_COUNT
GLOBAL FIVEMIN
GLOBAL TREQUESTED_POWER
GLOBAL TREPRATE
GLOBAL TRCNT
GLOBAL TEBUFFSIZE
GLOBAL TEST_FIRE_OK
GLOBAL SHUTTER_FLAG
GLOBAL ALARM_CYCLE
GLOBAL ALARM_TIME
GLOBAL KEYBOARD_DEBOUNCE_STORAGE0
GLOBAL FIBSTAT
GLOBAL FIBER_CONNECTED_ONCE
GLOBAL STK
GLOBAL PD_FLAG
GLOBAL ONE_HOUR_LSB
GLOBAL ONE_HOUR_MSB
GLOBAL TRY_TRY_AGAIN
GLOBAL BATFLG
GLOBAL MODEFLG
GLOBAL CBYTE
GLOBAL CBYTE2
GLOBAL CHKCNT
GLOBAL MEASURED_ENERGY_OK
GLOBAL REPRATE
GLOBAL RTPOINT
GLOBAL RCNT
GLOBAL RTEMP
GLOBAL EBUFFSIZE
GLOBAL TOTAL_PULSES
GLOBAL TOTAL_PULSES1
GLOBAL TOTAL_TIME
GLOBAL OTSTAT
GLOBAL TEMPER
GLOBAL TMPSTAT
GLOBAL TBOUNCE
GLOBAL FORGIVE
GLOBAL SERFLG
GLOBAL RUNNING_STATUS_COUNTER
GLOBAL REQUESTED_POWER
GLOBAL MIN_POWER
GLOBAL TEVAL
GLOBAL FTSW_STATUS, LAST_FTSW, FTSW_PREVIOUS_STATUS
GLOBAL TREATMENT_GREATER_30_SECS
GLOBAL FOOT_SWITCH_TEST_REQ

GLOBAL SW_DEBUG
GLOBAL SW_DEBUG_TRACE_MSG
GLOBAL SW_DBG_DISPLAY_POS
GLOBAL SW_DBG_STR_PTR


;patch!!!!
;ITS_POINTER
;	DS	50


EBUFFA	DS	64	;output energy average buffer
ABPOINA DS	2	;buffer pointer
BUFCNTA	DS	1	;keeps track of when to roll over buffer
ECNTA	DS	1	;number of readings to average (clips at 1/2 rr)
EBUFFSIZE DS	1	;size of avg buffer for any given reprate
EBUFFB	DS	64	;output energy average buffer
ABPOINB DS	2	;buffer pointer
BUFCNTB	DS	1	;keeps track of when to roll over buffer
ECNTB	DS	1	;number of readings to average (clips at 1/2 rr)

EBUFFE	DS	64	;output energy average buffer
ABPOINE DS	2	;buffer pointer
BUFCNTE	DS	1	;keeps track of when to roll over buffer
ECNTE	DS	1	;number of readings to average (clips at 1/2 rr)

EMVALA	DS	1	;energy output reading from energy monitor 'a'
EMVALB	DS	1	;energy output reading from energy monitor 'b'
EMVALE	DS	1	;energy output reading from energy monitor 'ext'
EMEMAX	DS	1	;maximum reading from external monitor
EMAMAX	DS	1	;maximum reading from monitor 'a'

TEVAL	DS	2	;total energy accumulator (lo byte)
TEVAL1	DS	2	;total energy accumulator (hi byte)

AVVAL	DS	1	;averaged energy reading
AVVALB	DS	1	;averaged energy reading
AVVALE	DS	1	;averaged energy reading
TAVVAL	DS	1	;averaged energy reading (temp storage)
DAVVAL	DS	1	;averaged energy reading (after fcal correction)

EVREQ	DS	1	;energy value requested by operator
DEVREQ	DS	1	;energy value requested by operator (deleivered) (totals)

MJOULES DS	2	;actual mj per pulse


PA_BUFF DS	4	;4 digit emvala buffer
PB_BUFF DS	4	;4 digit emvalb buffer
PE_BUFF DS	4	;4 digit emvale buffer
PBUFF_POINTER	DS 1	;point to proper buffer

AE_COUNTER DS	1	;avg_emon loop counter


REQUESTED_POWER	DS	1	;power setting (*10)

MIN_POWER DS	1	;minimum power setting (*10)
MAX_POWER DS	1	;maximum power setting (*10)

MINUS10 DS	1	;lowest acceptable output energy
PLUS10	DS	1	;highest    "        "      "
MINUS20 DS	1	;output energy window low
PLUS20	DS	1	;	"	"     high
HIWIND	DS	1	;upside vadj window for selected step

CAPVAL	DS	1	;8 bit pfn cap voltage value (0-2000 volts)
DACVAL	DS	2	;12 bit version of capval (fine tuning)

VTR_POINTER DS	2	;points to volts lookup tables

MAP_INCR DS	1	;service mode voltage table mapping variables
ESTEP	DS	1
ETOP	DS	1



UD_COUNTER DS	1	;fast display update utility counter
TASK_COUNTER DS	1	;also
PC_VAL DS	1	;cursor position variable


EDCNT	DS	1	;update display when edcnt = 0
DISPLAY_TOGGLE DS	1	;alternate joules, watts displays in treat

LZERO	DS	1 	;display routines leading zero flag
MEASURED_ENERGY_OK DS	1	;display requested (vs. measured) power flag

BINTEMP DS	9	;binary to bcd conversion buffer
TEMP0	DS	2	;binary - bcd storage
TEMP1	DS	2	; "        "     "
D1	DS	2	;bin - bcd temp storage
D0	DS	2	; "      "   "     "
DTEMP	DS	4	;divide routine temp storage



CBYTE	DS	1	;control port status byte
CBYTE2	DS	1	;control port status byte port 2 (kb, lems, gain)

MODEFLG DS	1	;mode status (ready, stby, aiming)
OPTFLG	DS	1	;option select flag
FIBSTAT	DS	1	;fiber installed flag
FIBER_CONNECTED_ONCE	DS	1	; fiber connected once is set when FIBSTAT is momentarily true,
								; after power up. Used for fiber removal to decrement procedure count
TREATMENT_GREATER_30_SECS	DS	1	; the energy treatment has been active for more than 30 seconds: N, Y

SW_DEBUG DS	1	; Software Debug
SW_DEBUG_TRACE_MSG DS 1	; used with SW_DEBUG_DISPLAY_TRACE. Value 0 -9 is displayed a t line 0 last position
SW_DBG_DISPLAY_POS DS 1 ; setup prior to call of SW_DBG_DISPLAY_FUNC sub routine, display position: Example - 20 is line 2 first position
SW_DBG_STR_PTR DS 2	; setup prior to call of SW_DBG_DISPLAY_FUNC sub routine, pointer to string


FIBBER	DS	1	;temp debounce storage
FBOUNCE	DS	1	;fiber read debounce counter
LV_DEBOUNCE DS	1	;debounce 15 volt check to avoid spurious
			;e08 on power-down

SHUTTER_FLAG	DS	1	;shutter open status
ALARM_CYCLE  	DS	1	;shutter open alarm on/off
ALARM_TIME  	DS	1	;shutter open alarm time
LP_TOGGLE   	DS	1	;alternate 630 / 1260 hz tones during treatment
				;using pullback device

READY_AIM_FLAG  DS	1	;aiming beam in READY  (on/off)
AIM_OK_FLAG     DS	1	;aiming beam detect = request (or not)
AIM_FORGIVENESS_COUNTER DS	1	;wait for ok (100ms)

LAST_INTLCK DS	1	;interlock debounce
INTLCK_STATUS   DS	1	;interlock status
LAST_FTSW       DS	1	;ftsw debounce
FTSW_STATUS     DS	1	;ftsw status
FTSW_PREVIOUS_STATUS DS 1	;footswitch previous delta/change


TEMPER	DS	1	;temperature (F.)
OTSTAT	DS	1	;over temp status
OTAFLG	DS	1	;over temp alarm status
TMPSTAT	DS	1	;temperature update status (debounced)
TBOUNCE	DS	1	;temperature read debounce
LAST_DISPLAYED_TEMP DS	1

KEYBOARD_DEBOUNCE_STORAGE0	DS	1	;keyboard debounce storage
KEYBOARD_DEBOUNCE_STORAGE1	DS	1	;   "         "       "
KEYBOARD_DEBOUNCE_STORAGE2	DS	1	;   "         "       "

SPEEDUP	DS	1	;keyboard repeat timer

FIVEMIN DS	2	;five minute ready mode time-out
TIME_SINCE_TFIRE DS	2	;too long since last tfire time-out

TMSEC_COUNT 	DS	1	;counts time ftsw is depressed in treat
TSEC_COUNT      DS	1	;counts time ftsw is depressed in treat
TMIN_COUNT      DS	1	;counts time ftsw is depressed in treat

ONE_HOUR_LSB DS	2	;one hour inactive plug puller
ONE_HOUR_MSB DS	2



TFCNT	DS	1	;test fire counter
TEST_FIRE_BURST_COUNT DS	1 	;test fire cycle counter
TEST_FIRE_OK   	DS	1	;test fire successful flag

REPRATE	DS	1	;rep rate selected
RCNT	DS	1	;rep rate cntr value
RTEMP	DS	1	;rep rate cntr value temp storage
RR10	DS	2	;reprate * 10 for calculations

RTPOINT	DS	2	;pointer to rep rate value table

TOTAL_PULSES    DS	2	;pulse counter
TOTAL_PULSES1   DS	2	;pulse counter
TOTAL_TIME      DS	2	;seconds of ops counter
PROC_TIME       DS	2	;seconds of procedure counter (see card variables)
LOCAL_TIME      DS	2	;used for total time
LOCAL_PULSES    DS	2	;ditto
LOCAL_PULSES1   DS	2	;ditto

LP_20	DS	2
LP_201	DS	2
LP_30	DS	2
LP_301	DS	2
LP_40	DS	2
LP_401	DS	2
LP_50	DS	2
LP_501	DS	2

SC_20	DS	2
SC_201	DS	2
SC_30	DS	2
SC_301	DS	2
SC_40	DS	2
SC_401	DS	2
SC_50	DS	2
SC_501	DS	2

PS_TEMP	DS	2
CTS_RRVAL       DS	1
STS_RRVAL       DS	1


SITE_TIME       DS	2	;seconds (*10) of treatment
			;at given site

CAL_COUNT       DS	1	;5 secs worth of pulses for calibrate


TMODEFLG        DS	1	;store mode status (ready, stby, aiming)

TEVREQ	DS	1	;store current energy/pulse setting during cal
TREQUESTED_POWER DS	1	;power setting (*10)
TREPRATE        DS	1	;store current rep rate
TRCNT	DS	1	;store current reprate count value
TEBUFFSIZE      DS	1	;buffer size for cal


TCAPVAL	DS	1	;store current voltage settings
TDACVAL	DS	2


RUNNING_STATUS_COUNTER	DS	2	;safety counter
SAFCHK	DS	1	;watch dog routine uses to compare safcnt
BATFLG	DS	1	;interlock attention flag
FORGIVE	DS	1	;ft sw illegal "forgiveness" counter
AIM_ATT_STAT    DS	1	;warning already given flag so doesn't loop

CHKCNT	DS	1	;safety counter "forgiveness" counter
WD_FLAG	DS	1	;used to synch to w.d.

PD_FLAG	DS	1	;tells init whether relay close delay or not

PULSE	DS	1	;pulse length (short/long) status flag


*******************************************************************************
;smart card land
*******************************************************************************

CARD_OK 	DS	1	;overall card is ok flag enables ready mode

CARD_STATUS	DS	1	;0 = No Reader, 4 = No Card, 6 = Card there
INT_CARD_STATUS	DS	1	;'p' = card there, needs reading, 'y' = ok

PROC_ALIVE	DS	1	;flags if internal procedure count is >0
				;and in process - used if card has been debited
				;to zero, but last procedure still current and ok

CARD_COMM_STAT  DS	1	;card data read ok flag
CHAR_WRITTEN_OK0 DS	1	;character written to serial port0 ok flag
CHAR_READ_OK0    DS	1	;character written to serial port0 ok flag
CHAR_WRITTEN_OK1 DS	1	;character written to serial port1 ok flag
CHAR_READ_OK1    DS	1	;character written to serial port ok flag
CHAR_WRITTEN_OK2 DS	1	;character written to serial port2 ok flag
CHAR_READ_OK2    DS	1	;character written to serial port2 ok flag

HANDSHAKE_OK    DS	1	;serial port handshake signal ok

INBUFF_LRC_OK   DS	1	;card data string input ok

CHAR_COUNTER    DS	1	;to avoid input overruns

INBUFF          DS	48	;input buffer
DATASTR1        DS	40	;output buffer
STATBUFF        DS	40	;card interchange status buffer
CARDBUFF        DS	48	;card data buffer

CURRENT_SUBSTR  DS	2	;pointer to current substitution string
SUBSTR_ID       DS	1	;substitution string #

AB_STRING	DS	2	;ASCII to binary temp storage

AB_LENGTH       DS	1
AB_CONVERSION   DS	2

CTS_ERR_FLAG    DS	1	;flag that we tried resetting balky SBC


RC_COMM_TIMEOUT DS	1
CARD_CHECK_TIMER DS	2	;used by main loop to check card reader every
			;5 secs
COMM_FORGIVE	DS	1	;comm error "forgiveness" counter


YIK	DS	1	;debug use


NEW_CARD 	DS	1	;flags new card inserted, update settings


;generate checksum storage
CARD_PIN 	DS	2

;current card data storage
PROC_COUNT_CURR	DS	1	;procedures credited to current valid card

PROC_COUNT_DISPLAYED	DS	1	;procedures displayed to user (will "lag" by one
			;during current treatment (procedure)
TOTAL_PROCS_CURR DS	1	;total procedures ever on current valid card

SERSTR_CURR	DS	7  	;serial # of current valid card (string

FCAL_CURR	DS	2	;fiber cal factor of current valid card

FTSTR_CURR	DS	7 	;fiber type (string)


POWER_LIMIT	DS	1	;upper power limit for this fiber

POWER_START     DS	1  	;"safe start" initial power level for this fiber
REPRATE_START   DS	1       ;"safe start" initial reprate for this fiber

CARD_TYPE   	DS	1	;old type card, new type card


PMJ_LIMIT20	DS	1	;mj per pulse limits as per rev 1.67+

PMJ_LIMIT30     DS	1

PMJ_LIMIT40     DS	1

PMJ_LIMIT50     DS	1



SEC_PROC_SYNCH  DS	1	;if 'y', then current procedure count up to date

SAD_POINTER     DS	2	;pointer for account storage use
FA_POINTER      DS	2	;pointer for find account use
ACCOUNT_POINTER DS	2	;pointer for general account access

CARD_ERROR_CODE DS	1	;error code from first byte of card data string




DEBUG_FLAG      DS	1

NP_ATT_FLAG     DS	1
NR_ATT_FLAG     DS	1
PB_ATT_FLAG     DS	1
ZP_ATT_FLAG     DS	1

DSTESTSTR       DS	48	;test string



;patch
;D_OKR	DS	45	;patch card string
;FAKE_CARD_ID
	DS	1	;patch string pointer

TRY_TRY_AGAIN	DS	1	;give plenty of chances if E31


TTA_FLAG	DS	1	;extra chance after card fail before E31



;patch!!!
TEMPY	DS	1	;patch temp storage


MAXBUFF    	DS	180	;emon peak detect buffer

MB_TOP_ADDR:
MB_TOP	DS	1	;top of scratchpad



********************************************************************************
;stack space
*******************************************************************************

		;ORG	RAM+0500H	;yes, i'm superstitious   
		ORG	0500H	;yes, i'm superstitious
STK:	DS	1
*******************************************************************************
;stack space: stack grows down to "lower addresses" as push operations are performed
;	stack range starts at 0500H and can grow to lower address 0300H, there is "data" 
;	below 0300H, which must not be stepped on (written over) by the stack!
*******************************************************************************
;	ORG	RAM+510H	;non-volatile storage area
;TEST_DISPLAY
;	DS	80
;TD_POSITION
;	DS	2

		ORG	508H
; Non initialized to zero area: address starts at 0508H - 050FH
FOOT_SWITCH_TEST_REQ	DS	1	; on warm or cold power up the foot switch test is required, if Fiber is removed during ready mode with 
								; minimum energy procedure period, then restart system, and do not test foot switch: Y, N value



	;ORG	RAM+510H	;non-volatile storage area        
	ORG	510H	;non-volatile storage area
VTR20	DS	40	;storage for volts value look-up table
VTR30	DS	48	;storage for volts value look-up table
VTR40	DS	48	;storage for volts value look-up table
VTR50	DS	64	;storage for volts value look-up table
VTR60	DS	56	;storage for volts value look-up table

	;ORG	RAM+620H	;non-volatile storage area	
	ORG	620H	;non-volatile storage area
LC0	DS	2	;lamp life counter (lo word)
LC1	DS	2	; "     "     "    (hi word)
P2D0	DS	2	;pulses to date counter (lo word)
P2D1	DS	2	; "     "     "    (hi word)
SCFLG	DS	1	;lamp counter cleared flag
ERRCODE DS	1	;last error code storage
SERFLG	DS	1	;in service mode flag
LANGUAGE_FLAG   DS	1	;English, or German or whatever
MSEC_COUNT      DS	1	;system hours counter ls byte (5msec per unit)
SEC_COUNT       DS	1	;system hours counter seconds byte (1 sec per unit)
MIN_COUNT       DS	1	;system hours counter minutes byte (1 min per unit)
S_HOURS         DS	2	;system hours counter

PDP_FLAG        	DS	1	;flags that system timed-out after 1 hour

WD_DIVIDE       DS	1	;does w.d. tasks every 5th interrupt (for speed)
			;up here in non-vol so ram init doesn't corrupt
*******************************************************************************
;card account land
;here we store card i.d.'s and the procedures in their account
*******************************************************************************
	;ORG	RAM+640H	; 
	ORG	640H

CARD_ACCOUNTS   DS	01940H	;smart card i.d.'s and proc counts are stored here
			;i.d.'s are 6 bytes, procs = 1 byte chksum = 1 byte

CA_TOP:
	DS	0	;mark top of card accounts



	END
