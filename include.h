;$SB(include.h file)

;NS160D
;assumes 95 uf cap and 150:1 hvps ref ratio
;9/20/2004
;copyright 9/20/2004 new star lasers
;Roseville, ca.

;$Header: C:/Dosland/ns160e/rcs/include.h 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $

;multi rep rate 20, 30, 40, 50, 60 Hz
;min out - 1 watt @ 20Hz (50mj), 2 watts @ 60Hz (33mj)
;max out - 10 watts @ 50,60 Hz (200mj), 4 watts @ 20HZ (200mj)
;300 usec max pulse width
;pump energy limits - 40 joules lo, 107 joules hi
;burn 1/2 sec of pulses before opening shutter

;New Star part numbers for new star 160D software

;project number


;Part Numbers:

;7020-0040	Spec, Sftwr Req, NS160, Card

;7020-0041	Spec, Frmwr Dsgn, NS160, Card

;7440-0018	Assy, Cntrl, Prog, NS160, Card

;7060-0061	File, Srce Code, NS160, Card

;7060-0062	File, Hex, NS160, Card

;7060-0063	Master EPROM, NS160, Card

;7013-0048	Proc, Tst, Frmwr, NS160, Card



;       error codes
;       error #                 error
;-------------------------------------------
;       1                       shutter failure
;       2                       h.v.p.s. error (> 10% too high)
;       3                       watch dog failure
;       4                       safety counter update failure
;       5                       ram test error
;       6                       rom check sum error
;       7                       hene error
;       8                       +15, -15 volt error
;       9               	energy output > 120% of requested energy
;	10		energy output < 20 mj (something broken)
;	11		h.v.p.s. too low error (< vlo)

;       fatal error codes
;       error #                 condition
;-------------------------------------------
;       41                      cooling failure
;       42                      h.v.p.s. railed (o.v.)

;       attention codes
;       attention #             condition
;-------------------------------------------
;       1                       interlock open
;       2                       foot switch unplugged
;       3                       wait for aim
;       4                       fiber not installed
;	5		service switch closed

; sw debug  - typically commented
;MIN_TREATMENT_PERIOD_30SEC	EQU	50;			; (10X) 5 seconds, decrement procedure count
;MAX_TREATMENT_PERIOD		EQU		1800   	; DEBUG ONLY!    (10X) 180 secs = 3 mins
;GREEN_MODE_PERIOD_MSB	EQU          00000H     ; DEBUG ONLY load x second system inactivity/shut-down timer. Uses 5msec interrupt, so x seconds =
;GREEN_MODE_PERIOD_LSB		EQU          08CA0H     ; DEBUG ONLY 200 * 60 * 3 = 36000 = 0000 8CA0 Hex	"3 minutes"
;FIVE_MIN_TIMEOUT	EQU	12000					; 1 minute timeout

; Normal Release mode - typically uncommented
MIN_TREATMENT_PERIOD_30SEC	EQU	300;			; (10X) 30 seconds, decrement procedure count
MAX_TREATMENT_PERIOD		EQU		9000          ; (10X) 900 secs = 15 mins
GREEN_MODE_PERIOD_MSB	EQU          0000AH     ; load 1 hour system inactivity/shut-down timer. Uses 5msec interrupt, so 1 hour =
GREEN_MODE_PERIOD_LSB	EQU          0FC80H     ; 200 * 60 * 60 = 720,000 = 000A FC80 Hex
FIVE_MIN_TIMEOUT	EQU	60000					; five minute timeout

;memory
RAM	EQU	0E000H
;STK	EQU	RAM+0390H	;top of volatile
ROM	EQU	0000H	;start of memory

;i/o ports

;display
DISPLAY	EQU	38H	;vacuum fluor. display interface
VPOS	EQU	30	;volts display position (service mode)

;keyboard
CPORT2	EQU	29H	;keyboard scan lines (output)
KBI	EQU	00H	;keyboard return lines

;misc. keyboard symbols:

SCAN0	EQU	01H	;drive scan line 0 low
SCAN1	EQU	02H	;drive scan line 1 low
SCAN2	EQU	04H	;drive scan line 2 low
RLMASK	EQU	0E0H	;"or" 3 unused msb's with kb return byte

;scan line 0 return lines
STBREQ_BTN	EQU	0FEH	;stby request
AIMREQ_BTN	EQU	0FDH	;aiming request
RDYREQ_BTN	EQU	0FBH	;ready request

INCREQ_BTN	EQU	0F7H	;increase energy
DECREQ_BTN	EQU	0EFH	;decrease   "
ESMASK	EQU	18H	;looks at both energy sel request keys
STBY_AND_AIM_BTNS	EQU	0FCH	;both keys pressed
RDY_AND_AIM_BTNS		EQU	0F9H	;both keys pressed

;scan line 1 return lines
SERREQ_BTN	EQU	0FEH	;second scan row, first bit
S1MASK	EQU	1	;mask service sw bit
			;service mode request

PPSREQ_BTN		EQU	0FDH	;second scan row, second bit
				;rep rate request
CAL_REQ_BTN		EQU	0FBH	;calibration request

;scan line 2 return lines
TOTALS_REQ_BTN	EQU	0FEH	;display totals request
				;third scan row, 1st bit
RESET_REQ_BTN	EQU	0F7H	;reset displayed accumulator request
				;third scan row, 4th bit
;patch!!!
CHANGE_CARD_REQ	EQU	0FDH	;fake new card request
				;third scan row, 2nd bit



;timer (8254)
TIMER	EQU	08H	;base address
TCONT	EQU	0BH	;timer control port
TIMER0	EQU	08H	;timer 0
TIMER1	EQU	09H	;timer 1
TIMER2	EQU	0AH	;timer 2
TMODE0	EQU	30H	;timer 0 in mode 0
TMODE1	EQU	70H	;timer 1 in mode 0
TMODE2	EQU	0B0H	;timer 2 in mode 0
TBEEP2	EQU	0B6H	;timer 2 in mode 3 (for BEEP_FUNC purposes)
TRD0	EQU	0E2H	;read  status of timer 0
TRD1	EQU	0E4H	;  "      "        "   1
TRD2	EQU	0E8H	;  "      "        "   2

TCLOCK1	EQU	076H	;timer 1 in mode 3 (for serial clock purposes)
READBACK	EQU	0E4H


;adc (ad7828) a/d converter
PSFB	EQU	10H	;hvps feedback port
EMONA	EQU	11H	;energy monitor 'a'
EMONB	EQU	12H	;energy monitor 'b'
EMEXT	EQU	13H	;external energy monitor
THERM	EQU	14H	;coolant thermistor
FIFTEEN	EQU	15H	;+- 15 volt status

;system status ports (74hct541)
SSTATA	EQU	18H
SSTATB	EQU	30H

;input port a: (sstata)
FTMASK		EQU	3	;mask both bits for closure check
PULLBACK	EQU	08	;fiber pullback active (active low)
FLOW		EQU	020H	;coolant flow o.k. (active lo)
INTERLOCK 	EQU	040H	;interlock o.k. (active lo)


;input port b: (sstatb)
SHUTO	EQU	1	;safety shutter closed (active lo)
SHUTC	EQU	2	;safety shutter opened (active lo)
SHMASK	EQU	3	;look at both bits for closure test
AIM_DET	EQU	4	;aiming beam on detected (active lo)
FDET	EQU	8	;fiber detected (active lo)
EOKLO	EQU	10H	;energy monitor compare low
EOKHI	EQU	20H	;energy monitor compare high

;dac (ad7248) d/a converter
DACH	EQU	2DH	;dac high byte
DACL	EQU	2CH	;dac low byte

;system control port (74hct374)
CPORT		EQU	28H

PSON		EQU	0FDH	;enable h.v. power supply (active lo)
PSOFF		EQU	2	;disable  "
TRIG		EQU	0FBH	;trigger pulse on (active lo)
TOFF		EQU	4	;trigger pulse off
SSO		EQU	0F7H	;open safety shutter (active lo)
SSC		EQU	8	;close  "       "
FAN_HIGH	EQU	0EFH	;fan to high speed (active lo)
FAN_SLOW 	EQU	010H	;fan to slow speed signal
AIM_ON		EQU	0DFH	;aiming beam on (active lo)
AIM_OFF 	EQU	020H	;aiming beam off
SAFEMSK 	EQU	02EH	;forces "dangerous" signals inactive
SBC_OFF		EQU	040H
SBC_ON		EQU	0BFH

;system control port 2 (74hct377) also drives keyboard scan lines

INTOFF	EQU	10H	;reset emon integrator (analog switch closed)
INTON	EQU	0EFH	;enable emon integrator (analog switch opened)

LEMOFF	EQU	080H	;'laser emission' off mask
LEMON	EQU	07FH	;'laser emission' on  mask


;main power relay latch port
RELAY	EQU	2AH

;watch dog keep alive
WATCH_DOG_KEEP_ALIVE	EQU	2BH

;output port enable address
OCONT	EQU	2EH	;0 disables, 1 enables

;misc symbols
VMAX	EQU	255	;1500 volts (max volts)
VMAX16	EQU	4095	;1500 volts (hi res)(max volts)

VFAT	EQU	68	;kill power if exceeded at wrong time (400v)

VLO	EQU	68	;400 volts
VLO16	EQU	1092	;400 volts (hi res)

SCALE	EQU	2	;energy vs value i.e. 50 = 100mj/scale

AA	EQU	05BH	;german "A"
OO	EQU	05CH	;german "O"
UU	EQU	05DH	;german "U"


;8251a uart constants (3 uarts)
COMM_DATA_0	EQU	20H	;serial port (uart) 0 data
COMM_CON_0	EQU	21H	;serial port (uart) 0 command
COMM_DATA_1	EQU	22H	;serial port (uart) 1 data
COMM_CON_1	EQU	23H	;serial port (uart) 1 command
COMM_DATA_2	EQU	24H	;serial port (uart) 2 data
COMM_CON_2	EQU	25H	;serial port (uart) 2 command

COMM_MODE_0	EQU	0DDH	;set stop bit, parity, baud factor
;COMM_MODE_0	EQU	04EH	;04dh = 8,N,1, clk factor 16:1 	// Sw Debug Test 
COMM_MODE_1	EQU	04EH	;04dh = 8,N,1, clk factor 16:1
COMM_MODE_2	EQU	0DDH	;char length = 8 bits, clk factor 1:1

;command instruction bits:
;Uart inverts handshakes (RTS/CTS) but driver re-inverts at connector
;so following the logic is puzzling

;e.g. to control RTS:
;RTS "on": bit is set HI, uart inverts to LOW, driver inverts to HI
;RTS "off": bit is set LOW, uart inverts to HI, driver inverts to LOW

;e.g. to read CTS_DSR:
;CTS_DSR "on": bit is HI, uart inverts from LOW, driver inverts from HI
;CTS_DSR "off": bit is LOW, uart inverts from HI, driver inverts from LOW

CC_BASE		EQU	00	;start building command word from this
XMIT_ENABLE	EQU	01H	;enable uart xmit
XMIT_DISABLE	EQU	0FEH	;disable uart xmit
RECV_ENABLE	EQU	04H	;enable uart receive
RECV_DISABLE	EQU	0FBH	;disable uart receive
RTS_ON		EQU	20H	;data bit 5 set hi (inverted by 8251)
RTS_OFF		EQU	0DFH	;data bit 5 set lo (inverted by 8251)
CTS_DSR		EQU	80H	;'cts' signal from port copied to
				;8251 'dsr' pin so status can be read

;NULL		EQU	00H	;null char string terminator

;patch!!
NULL		EQU	'$'	;null char string terminator
;patch!!



