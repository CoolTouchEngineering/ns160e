;$PG
$PL=59
;$TL(NS160E MAIN.ASM SOURCE FILE)

	INCLUDE "INCLUDE.H"
;$Header: C:/Dosland/ns160e/rcs/main.asm 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------
; ROUTINE NAMES:

	GLOBAL	MAIN
	GLOBAL	GREEN_MODE_INIT_FUNC

;global symbols

	GLOBAL	RDYSTR
	GLOBAL	AIMSTR

	;GLOBAL	DISPLAY_DEBUG_FUNC

; PROJECT: NS160E
;
; Architecture:
;	main loop
;	Timers (82C54 - 3 timers). OUT0 -> interrupt 5.5 Watchdog server, OUT1 -> SCLK (82C51 Comms Drivers), OUT2 -> Buzzer
;	Watchdog: keep alive is performed in Timer 0 ISR, should be re-architectured and moved to super loop!
;	2x20 Display (8 Bit data bus with control/status bits)
;	Card reader (Comm 1 - 4800 Baud)
;	Keypad
;	Inputs:
;		ISPARE3
;		ISPARE5
;		TEMP CLOCK B
;		*EOK-LO
;		*Fiber Detect
;		*Aim Detect
;		SDETB
;		SDETA
;	Outputs:	
;		*HVPS-ENABLE 
;		*Shutter
;		*Aiming-Beam
;		Opspare2
;		*SBC RELAY
;		*FAN
;		*FIRE_FUNC Laser control
;		Ospare3
;	ADC (AD7828): 8 channels 
;		High Voltage Power Supply (HVPS) Feedback
;		Energy A
;		Energy B
;		Energy External
;		T1, T2
;		+- 15v
;		Temp Sensor
;		Handpiece ID	
;	DAC: HVPS Ref (AD7248)
;	Toggle switch for normal or service mode
;	2 EEPROMS for data integrity
;	Code space 0x0 - 0xA000 (27C512 U16, U18)
;	NVRAM 0xE000 - 0xFFFF (DS1243Y)
;	Interrupts disabled on startup, and enabled in Watchdog
;	Interrupts:
;		Reset Vector Jump to code space at address 0x100
;		Power Down
;		7.5 Watchdog Fail
;		6.5 Memory Fail (EEPROM - Hardware verification with every read, 2 EEPROM devices)
;		5.5 Timer0 5Ms (Watchdog Server)
;		Watchdog Failure
;	Comm 0: internal (not used) 
;	Comm 1: Card Reader Intel 8251 U22
;	Comm 2: (not used) Intel 8251 U22
;
; Notes:
;	1) Release build
;		update revision/version number. INIT.ASM - REV:	DB	'$Revision: 1.51 $'
;		verify INIT.ASM has JMP ROM_OK commented out, and JMP CHKROM is uncommented
;		Build project using "Softools"" application
;		Import NS160 hex file (Intel Hex) into "ICY Hexplorer" application, and select "File->export->ascii text"
;		Use NewStarLaser checksum.py python script to generate the "new checksum"
;		ROM Checksum needs to be modified for release build. Modify ROMEND.ASM with the "new checksum",
;			as required at ROMEND.ASM CHKSUM: DW	01A9AH  Order: low byte, high byte
;		Re-build project using "Softools"" application
;		Burn EEPROM parts
;		Version label: Vx.y[a - alpha, b - beta, t- test, c - cleanup, F - FAST debug, FT - Fast Timer][z]
;			where x - major, y - minor, and z is sub minor
;
;	2) Software development:
;		remove checksum test. In INIT.ASM: add JMP ROM_OK, and comment out JMP CHKROM
;		include .h contains sw debug EQU/literals: to allow the system to have shorter timeouts for some features, allowing faster testing
;			Make sure all lines are set to Release Mode for final release build
; 		The display can be updated with "breadcrumbs" to allow run-time information : add sw_debug.asm
;		For additional sw debug capability: Uncomment "INCLUDE SW_DEBUG.ASM" in MAIN.ASM, and initialize sw debug variables to zero in init.asm
;	3) See NS160.map for memory usage
;	4) See listing files ".lst" for additional information (may denote build error description) 
;
;;;;;;;;;;;;;;;; Revisions
;
; 1.52c1
;	JMP	ROM_OK	active	
;	JMP CHKROM disabled
;	Adding KeyBoard Manufacturing test: press/release "performcance" status: DIAGS_KEYBOARD_MONITOR_LOOP
;		tested: now Disabled - domes 
;	Cleanup code:
;	EXEC_ISR_5MS changed to correct value EXEC_ISR_1MS
;	updating notes section for Version specifics
;	Commenting out: Tight Infinite Test Loop added at beginning of main
;	changing KCHEK0_FUNC to POLL_KEYBOARD_FUNC, and SERCHEK_FUNC to SERVICE_SWITCH_MONITOR_FUNC
;	changing KCHEK1_FUNC to KEYBOARD_SCAN_LINE1_FUNC, and KCHEK2_FUNC to KEYBOARD_SCAN_LINE2_FUNC 
;	changing DEBOUNCE_DELAY_5MS_FUNC to DEBOUNCE_DELAY_5MS_FUNC 
;	changing KCHECK to KEYBOARD_MONITOR
;	Added DIAGS_E04_SAFETY_COUNT_FUNC 
;	Added DIAGS_E05_RAM_FAIL_MSG_FUNC
;
; 1.52t2
;	Test firmware to exercise error message E04
;	Tight Infinite Test Loop added at beginning of main, after 10 laps in super loop
;	JMP	ROM_OK	active	
;	JMP CHKROM disabled
;	
;
; 1.52t1
;	Test firmware to exercise error message E05
;	"Force RAM Error" message. INIT.ASM added JMP	RERROR
;	JMP CHKROM disabled
;
; 1.52
;	Release Build
;	renamed WDKA to WATCH_DOG_KEEP_ALIVE
;	added CHKROM
;	cleaned up code
;
; 1.52a8
;	removed fast timeouts
;
; 1.52a8FT
;	3 min Green Mode1 minute fiber
;	adding suffix for scan buttons "_BTN" on control surface
;	disabled CHKROM
;	Green Mode time changed from 1 out to 5 mins for software testing! GREEN_MODE_PERIOD_MSB, _LSB
;	adding Foot switch detection in fiber test, to re-initialize Green Mode for 1 hour timeout
;	adding Fiber Test (Cal Test) entrance, with no foot switch pressed requires 5 minute timer
;	adding Fiber Test timeout when waiting for Ready Button to be pressed!
;	changing 5 minute timer to 1 mins for software testing of FIBER TEST inactivity in menus
;	fixing issues with strings having _FUNC, due to global find/replace in 1.52a6
;
; 1.52a7
;	cleaning up code
;	adding CHKROM
;
; 1.52a6
;	all call functions/subroutines have an added suffix "_FUNC"
;
; 1.52a5
;	removed debug breadcrumbs,and comm debug
;	all normal timeouts are activated
;
; 1.52a3FTS
;	adding serial debug out of comm0
;
; 1.52a4FT
;	adding SW_DEBUG_TRACE_MSG, SW_DEBUG_DISPLAY_TRACE
;	adding code when treatment is occurring and fiber removal occurs after 30 seconds of treatment to exit treatment (re-initialize), such that
;		mutiple treatments result in decrementing treatment count correctly. Reset procedure timer PROC_TIME, and disable PROC_ALIVE
;	3 minute Fast Timeout for treatment
;
; 1.52a3FT
;	removing code in main related to fiber removal and decrement procedure count
;	changing 4 hour to 1 hour for comments and labels
;	3 minute Fast Timeout for treatment
;
; 1.52a2FT
;	3 minute Fast Timeout for treatment
;
;  V1.52a2
;
;
;  V1.52a2F - Fast
;	no checksum performed
;	added delays around "decrement Treatment vount and Restore Display"  in main
;	removing debug state machine info
;	removing test code: proc count displayed and alive reset after fiber disconnected
;
;  V1.52a1F - Fast
;	no checksum performed
;	adding back debug state machine info
;	adding proc count displayed and alive reset after fiber disconnected
;	using equates for sw debug fast period modes, to allow efficient testing of system
;
; V1.52 - Normal
;	adding checksum   JMP CHKROM
;
; V1.51a7 - Normal
;
; V1.51a7F - fast!
;  	remove sw debug state machine display on 2x20
;	removing FIBERxyz_DBG: labels
;	adding checksum changes to calculate based on one contiguous image 0x0000 - 0x9FFF
;	current build is inhibiting over CHKROM
;
; V1.51a6F - fast!
;	adding back RESTORE_DISPLAY_FUNC call after DEC_PROCEDURE_COUNT
;	adding debug statements
;
; V1.51a5 - Normal!
;	build for Fast Activity counter 1 hour instead of 90 seconds
;   build for Fast Treatment 15 minutes instead of 3 minutes
;	Cleaning up code
;	inactivity counter re-initialization: updated comments, and re-ordered MSB and LSB
;
; V1.51a5f - fast!
;	build for Fast Activity counter 90 seconds instead of 1 hour
;        build for Fast Treatment 3 minutes instead of 15 minutes
;	Cleaning up code
;
; V1.51a4
;	modifying power down order for PULL_DE_PLUG
;
; V1.51a3
;	reordering PULL_DE_PLUG to allow display information to be seen, and adding more bloops
;	adding DBG_PRINTF
;	removing RESTORE_DISPLAY_FUNC, did not update display as expected
;
; V1.51a2
;	fixing RESTORE_DISPLAY_FUNC
;
; V1.51a2
;	SPS_CTEV changed from 30 mins to 3 mins
;	remove sw debug messages
;	remove delta flag: FTSW_DELTA
;	changed from 3 to 5 seconds  display time for 1 HOUR INACTIVITY SHUT DOWN
;	added RESTORE_DISPLAY_FUNC prior to procedure decrement count being triggered for fiber removal
;
; V1.51
;	Fiber removed, decrement procedure count
;	Power down system if non-activity with foot switch occurs for a duration of one hour
;	Checksum (summation) is over entire contiguous code image 0x0 thru ROMEND -1  (0xA000 -1)
;	Adding copyright statements in headers
;
;---------------------------------------------------------

	DEFSEG	MAINSEG,CLASS=CODE
	SEG	MAINSEG

;$SB(MAIN SUBROUTINE)


; DATA INPUTS:
	EXTERN	FIBSTAT
	EXTERN 	FIBER_CONNECTED_ONCE
	EXTERN	AIM_OK_FLAG
	EXTERN	TMPSTAT
	EXTERN	REPRATE
	EXTERN	ESTEP
	EXTERN	MODEFLG
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE0
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE1
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE2
	EXTERN	EVREQ
	EXTERN	TEST_FIRE_OK
	EXTERN	FIVEMIN
	EXTERN	TIME_SINCE_TFIRE
	EXTERN	READY_AIM_FLAG
	EXTERN	SEC_PROC_SYNCH
	EXTERN	PROC_TIME
	EXTERN	CARD_STATUS
	EXTERN	INT_CARD_STATUS
	EXTERN	CARD_ERROR_CODE

	EXTERN	SERSTR_CURR
	EXTERN	FTSTR_CURR

	EXTERN	CARD_CHECK_TIMER

	EXTERN	NP_ATT_FLAG
	EXTERN	NR_ATT_FLAG
	EXTERN	PB_ATT_FLAG
	EXTERN	ZP_ATT_FLAG
	EXTERN	ONE_HOUR_LSB
	EXTERN	ONE_HOUR_MSB
	EXTERN	PDP_FLAG
	EXTERN	FTSW_STATUS
	EXTERN	FTSW_PREVIOUS_STATUS
	EXTERN  FOOT_SWITCH_TEST_REQ

; DATA OUTPUTS
	EXTERN	RUNNING_STATUS_COUNTER
	EXTERN	FORGIVE
	EXTERN	CBYTE2
	EXTERN	OTAFLG
	EXTERN	CBYTE
	EXTERN	AIM_ATT_STAT


	EXTERN	POWER_LIMIT
	EXTERN	POWER_START
	EXTERN	REPRATE_START


	EXTERN	PROC_COUNT_CURR
	EXTERN	PROC_COUNT_DISPLAYED
 	EXTERN	CARD_OK
	EXTERN	RC_COMM_TIMEOUT
	EXTERN	PROC_ALIVE

	EXTERN	FCAL_CURR
	EXTERN	CARD_PIN
	EXTERN	SAD_POINTER
	EXTERN	FA_POINTER
	EXTERN	ACCOUNT_POINTER
	EXTERN	COMM_FORGIVE
	EXTERN	CLEAN_ACCOUNTS_FUNC
	EXTERN	INBUFF_LRC_OK

	EXTERN	RCNT
	EXTERN	RTEMP
	EXTERN	AVVAL


	EXTERN	RTAB
	EXTERN	RTPOINT

	EXTERN	MEASURED_ENERGY_OK


	EXTERN	NEW_CARD

	EXTERN	PMJ_LIMIT20
	EXTERN	PMJ_LIMIT30
	EXTERN	PMJ_LIMIT40
	EXTERN	PMJ_LIMIT50

	EXTERN	SITE_TIME
	EXTERN	SC_20
	EXTERN	SC_201
	EXTERN	SC_30
	EXTERN	SC_301
	EXTERN	SC_40
	EXTERN	SC_401
	EXTERN	SC_50
	EXTERN	SC_501
	EXTERN	YIK	;for debug!!


; ROUTINES CALLED BY:
; ROUTINES CALLED
	EXTERN	ERROR
	EXTERN	FATAL
	EXTERN	TREAT_FUNC
	EXTERN	LVPS_TEST_FUNC
	EXTERN	HVPS_READ_FUNC
	EXTERN	TCHECK_FUNC
	EXTERN	DELAY_500MS_FUNC
	EXTERN	DELAY_100MS_FUNC
	EXTERN	DELAY_1MS_FUNC
	EXTERN	FTATT_FUNC
	EXTERN	KREL_FUNC
	EXTERN	BEEP_FUNC
	EXTERN	RESTORE_DISPLAY_FUNC
	EXTERN	MODE_DISPLAY_FUNC
	EXTERN	DEBOUNCE_DELAY_5MS_FUNC
	EXTERN	POLL_KEYBOARD_FUNC
	EXTERN	KEYBOARD_SCAN_LINE1_FUNC
	EXTERN	KEYBOARD_SCAN_LINE2_FUNC
	EXTERN	SET_STANDBY_FUNC
	EXTERN	FATT_FUNC
	EXTERN	OUTPUT_DISPLAY_FUNC
	EXTERN	PUTSTR_FUNC
	EXTERN	ESELECT_FUNC
	EXTERN	BLOOP_FUNC
	EXTERN	SET_REPRATE
	EXTERN	CALIBRATE_FUNC
	EXTERN	DISPLAY_TOTALS_FUNC
	EXTERN	TESTFIRE_FUNC
	EXTERN	REVISION_DISPLAY_FUNC
	EXTERN	AIM_ATT_FUNC
	EXTERN	RESET_DISPLAY_FUNC
	EXTERN	SET_CURSOR_POSITION_FUNC
	EXTERN	BATT_FUNC
	EXTERN	COMPARE_16_FUNC

	EXTERN	UBER_CARD_ROUTINE_FUNC

	EXTERN	FIND_ACCOUNT_FUNC
	EXTERN	UPDATE_ACCOUNT_PROCEDURES_FUNC
	EXTERN	DECR_PROCEDURE_COUNT_FUNC

	EXTERN	READ_CAL_ATT_FUNC
	EXTERN	CARD_ERROR_HANDLER_FUNC
	EXTERN	CARD_READER_ATT_FUNC
	EXTERN	CARD_INVALID_ATT_FUNC
	EXTERN	CARD_EMPTY_ATT
	EXTERN	CARD_NOT_PRESENT_ATT_FUNC

	EXTERN	CARD_REMOVED_ATT
	EXTERN	CARD_INSERTED_ATT
	EXTERN	WIPE_CURRENT_CARD_DATA_FUNC

	EXTERN	SET_LIMITS_FUNC
	EXTERN	ESETUP_FUNC

	EXTERN	SITE_TIME_DISPLAY_FUNC
	EXTERN TREATMENT_GREATER_30_SECS 
	EXTERN INIT_FIBER_REMOVAL_IN_TREATMENT	
	;EXTERN DIAGS_E04_SAFETY_COUNT_FUNC
	EXTERN DIAGS_KEYBOARD_MONITOR_LOOP

;debug
	EXTERN	TEMP0
	EXTERN	TEMP1
	EXTERN	BINASC_FUNC
	EXTERN	BINTEMP
	EXTERN	WRITE_TO_DISPLAY_FUNC
	EXTERN  CHAR_OUT_0_FUNC



$EJECT


;INCLUDE SW_DEBUG.ASM

;$AP
    	;*************************
	;*       MAIN LOOP      ;*
	;*                      ;*
	;*************************
	
MAIN:	; the "superloop" code performs multiple features, and flows from one code section (feature) to the next. Some sections jmp to MAIN:)

    	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;  GREEN MODE: power down if no footswitch activity for specified period
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
	;************************
	; DIAGNOSTIC TEST CODE!!!
	;************************
	;CALL DIAGS_E04_SAFETY_COUNT_FUNC
	;JMP DIAGS_KEYBOARD_MONITOR_LOOP

	LDA	FTSW_PREVIOUS_STATUS
	MOV	B,A
	LDA	FTSW_STATUS
	CMP	B
	JZ 	HAS_ONE_HOUR_OF_INACTIVITY ; is there a change in the footswitch? no, check 1 hour activity
	STA	FTSW_PREVIOUS_STATUS		; yes, update previous to new, and set change flag
	
	; Re-initialize inactivity timer to the maximum time for inactivity
    ; The system shall shut down in Green Mode after a specified period (1 hour)  of inactivity from the foot-switch
	CALL GREEN_MODE_INIT_FUNC

HAS_ONE_HOUR_OF_INACTIVITY:
	LHLD	ONE_HOUR_MSB	;check upper word first
	MOV	A,L
	ORA	H
	JNZ CHECK_AIM_STATUS

	LHLD	ONE_HOUR_LSB	;now lower word
	MOV	A,L
	ORA	H
	JNZ CHECK_AIM_STATUS

	JMP	PULL_DE_PLUG	; Power Down  System


CHECK_AIM_STATUS:
	LDA	AIM_OK_FLAG	;check aim status
	CPI	'Y'
	JZ	MA_OK
	CALL	AIM_ATT_FUNC

MA_OK:	LHLD	RUNNING_STATUS_COUNTER	;update safety counter
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER

	LHLD	TIME_SINCE_TFIRE	;is 'time since last tfire' timer=0?
	MOV	A,L		;if 0, then reset 'tfire ok ' flag
	ORA	H
	JNZ	CHECK_15VOLT_SUPPLIES

	MVI	A,'N'	;reset flag so system will force testfire
	STA	TEST_FIRE_OK

CHECK_15VOLT_SUPPLIES:
	CALL	LVPS_TEST_FUNC	;check +- 15 volt supplies

 	CALL	HVPS_READ_FUNC	;is power supply on?
	CPI	VFAT	; >400 volts?
	MVI	A,41	;error message
	JNC	FATAL	;if out of bounds, kill power

	CALL	TCHECK_FUNC	;check coolant status

	IN	SSTATA	;is foot switch in illegal position?
	ANI	FTMASK
	CPI	1	;nope
	JZ	FTNA1
	CPI	2	;nope
	JZ	FTNA1

	LDA	FORGIVE	;run out of patience?
	DCR	A
	STA	FORGIVE
	JNZ	CHECK_SI	;not yet, proceed
;foot switch in illegal position for 20 main loops (about 100 msec) so att.
	CALL	FTATT_FUNC	;foot switch attention display
	CALL	KREL_FUNC
	CALL	BEEP_FUNC	;signal o.k.

	CALL	RESTORE_DISPLAY_FUNC 	;restore normal display
	JMP	KS	;force standby

FTNA1:	MVI	A,255	;reset ft sw error forgiveness counter
	STA	FORGIVE

*******************************************************************************
;check safety interlock
CHECK_SI:
	IN	SSTATA	;check system status
	ANI	INTERLOCK	;check interlock
	JZ	MI_OK	;if o.k., continue
	CALL	BATT_FUNC	;interlock att. routine
	JMP	KS	;force standby
*******************************************************************************

MI_OK:	NOP

;check if safety shutter closed
SSCHECK:IN	SSTATB	;check safety shutter
	ANI	SHMASK
	CPI	2	;2 = shutter closed
	MVI	A,1	;in case
	JNZ	ERROR	;if not closed, error


*******************************************************************************
;check card status

MCARD_CHECK:

;check card status every .5 seconds
;if no response after 20 * .5 seconds, E45

	LHLD	CARD_CHECK_TIMER
	MOV	A,H
	ORA	L
	JNZ	KEYBOARD_MONITOR

;Every .5 seconds:
;Check INT_CARD_STATUS "internal", which is my local status flag not from card
;reader
;If = "R", not present, ask for card insertion
;If = "V", not valid
;if = "P", pending, i.e. no problem haven't tried reading yet, so not 'y'
;	   this will cause a "loading cal data message" after a good read
;If = "Z", then zero procs on card
;if = "Y", no display, everythings "groovy", but check status for change

	MVI	A,CC_BASE
	ORI	XMIT_ENABLE		;assert rts
	ORI	RECV_ENABLE
	ORI	RTS_ON
	OUT	COMM_CON_1

DO_UBER:LXI	H,100	;reload timer
	SHLD	CARD_CHECK_TIMER


	CALL	UBER_CARD_ROUTINE_FUNC



UBER_DONE:
;patch for debug in Roseville

;	PUSH	PSW	;save flag

;	MVI	A,20	;mode display position
;	CALL	SET_CURSOR_POSITION_FUNC
;	LXI	H,STATSTR
;	CALL	PUTSTR_FUNC
;	POP	PSW
;	CALL	WRITE_TO_DISPLAY_FUNC
;	JMP	UPATCH_DONE

;STATSTR:	DB	'STAT=$'

;UPATCH_DONE:
;end patch


	CPI	'Y'
	JZ	MT_AOK

	CPI	'C'	;card is bigger, so we'll need to ask for
	JZ	MCARD_BIGGER	;decrement

	CPI	'E'	;error?
	JZ	MCARD_ERROR

	CPI	'S'
	JZ	MCARD_STATUS_PROB

	CPI	'H'
	JZ	PIN_BAD

	CPI	'Z'
	JZ	MZERO_PROCS

	CPI	'W'	;new card, force update next uber
	JZ	MCCC_NC

	CPI	'T'	;no comm, but hasn't timed out yet
	JMP	KEYBOARD_MONITOR


HUH:	JMP	HUH

MCARD_BIGGER:		;decrement
	CALL	DELAY_100MS_FUNC	;wait for SBC to settle after "D$" response
	CALL	DELAY_100MS_FUNC
	CALL	DELAY_100MS_FUNC
	CALL	DELAY_100MS_FUNC
	CALL	DELAY_100MS_FUNC

;patch for debug in Roseville
;	MVI	A,20	;mode display position
;	CALL	SET_CURSOR_POSITION_FUNC
;	LXI	H,DECSTR
;	CALL	PUTSTR_FUNC
;	CALL	WRITE_TO_DISPLAY_FUNC
;	JMP	DECPATCH_DONE

;DECSTR:	DB	'DECR$'

;DECPATCH_DONE:
;end patch

DECR_PROCEDURE_CNT:

	CALL	DECR_PROCEDURE_COUNT_FUNC

	CPI	'E'	;error?
	JZ	DECR_ERROR

	CALL	DELAY_500MS_FUNC	;slow down for reader access

	CALL	UBER_CARD_ROUTINE_FUNC	;update card procedure count

	CALL	DELAY_500MS_FUNC

;	CALL	RESTORE_DISPLAY_FUNC

	LDA	PROC_ALIVE	;if procedure is active, no "load" message
	CPI	'Y'
	JZ	DEC_NO_LOAD

;	CALL	READ_CAL_ATT_FUNC		;reading calibration data
	LDA	PROC_COUNT_CURR
	STA	PROC_COUNT_DISPLAYED
	CALL	RESTORE_DISPLAY_FUNC

DEC_NO_LOAD:
	JMP	KEYBOARD_MONITOR


DECR_ERROR:
	CALL	RESET_DISPLAY_FUNC
	LXI	H,DECR_ERROR_STR
	CALL	PUTSTR_FUNC
	CALL	BLOOP_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	RESTORE_DISPLAY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN

DECR_ERROR_STR:	DB	'DECREMENT ERROR$'



MCARD_ERROR:
	LDA	INT_CARD_STATUS	;debug
	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	INT_CARD_STATUS
	STA	CARD_OK

	CALL	CARD_ERROR_HANDLER_FUNC	;"real error!"

;00 -- All OK,
;20 -- Can't power up the smart card
;21 -- Smart card unresponsive
;22 -- Can't ID Card
;23 -- Correct type but not ours
;24 -- Can't determine card status
;25 -- (new, see below) CSC1 Ratification count is at max
;26 -- Couldn't decrement procedure count on card
;30 -- Comm error with RDR
;31 -- Error during RDR init.
;32 -- Invalid RDR firmware version
;40 -- Comm. error with 8085
;41 -- Don't understand command from 8085
;42 -- (event) Card has been removed
;43 -- (event) Card has been inserted
;44 -- 8085 write to SBC error
;45 -- 8085 read from SBC error
;46 -- No CTS error (no response on handshake line)
;47 -- Card data string corrupted (LRC fail)

	LDA	CARD_ERROR_CODE	;if comm error, try to read again
	CPI	40	;if < 40, not comm error
	JC	ME_NOTCOMM
	CPI	48	;if > 47, not comm error
	JNC	ME_NOTCOMM

MCOMM_ERROR:
	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK
	STA	INT_CARD_STATUS
	JMP	KEYBOARD_MONITOR

ME_NOTCOMM:
	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK
	STA	INT_CARD_STATUS
	JMP	KEYBOARD_MONITOR

MCARD_STATUS_PROB:
	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK
	LDA	CARD_STATUS
	CPI	0	;0 = card not present
	JZ	MC_NOT_PRESENT
	CPI	4	;4 = card reader powered down
	JZ	MC_PD
	JMP	KEYBOARD_MONITOR	;huh? ignore

MC_NOT_PRESENT:
	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK
	MVI	A,'R'
	STA	INT_CARD_STATUS
	MVI	A,'N'	;this will cause debit with next 60 sec. use
	STA	SEC_PROC_SYNCH
	LXI	H,0	;reset procedure timer
	SHLD	PROC_TIME
	MVI	A,'N'	;flag that procedure is over, not current
	STA	PROC_ALIVE
	
	LDA	FIBSTAT	;determine if fiber installed
	CPI	'N'	;if not att. and standby
	JNZ MC_NOT_PRESET_CONT
	
	LDA TREATMENT_GREATER_30_SECS	
	CPI 'N'
	JZ	MC_NOT_PRESET_CONT	; yes, less than 30 seconds
	
	CALL	FATT_FUNC	;if not installed, att and stby
	LDA 'N'
	STA FOOT_SWITCH_TEST_REQ
	CALL DELAY_100MS_FUNC
	JMP 	INIT_FIBER_REMOVAL_IN_TREATMENT

MC_NOT_PRESET_CONT:
	LDA	NP_ATT_FLAG	;already warned?
	CPI	'Y'	;if so, no att
	JZ	MPD_NOT


			;this also indicates card has just been removed
			;so set power limit = 15w, xmission = 100%


	MVI	A,'Y'	;init display requested value flag to 'yes'
	STA	MEASURED_ENERGY_OK


	MVI	A,'N'	;this will cause debit with next 60 sec. use
	STA	SEC_PROC_SYNCH
	LXI	H,0	;reset procedure timer
	SHLD	PROC_TIME
	MVI	A,'N'	;flag that procedure is over, not current
	STA	PROC_ALIVE

	MVI	A,0
	STA	PROC_COUNT_CURR	;procedure count
	STA	PROC_COUNT_DISPLAYED	;procedure count
	STA	CARD_ERROR_CODE

	LXI	H,0
	SHLD	CARD_PIN
	SHLD	PROC_TIME
	SHLD	SAD_POINTER
	SHLD	FA_POINTER
	SHLD	ACCOUNT_POINTER


	MVI	A,150
	STA	POWER_LIMIT
	MVI	A,0
	STA	POWER_START
	MVI	A,40
	STA	REPRATE_START

			;default values for power limits

	MVI	A,80	;20hz limit = 400mj
	STA	PMJ_LIMIT20
	MVI	A,120	;30hz limit = 400mj
	STA	PMJ_LIMIT30
	MVI	A,150	;40hz limit = 375mj
	STA	PMJ_LIMIT40
	MVI	A,150	;50hz limit = 300mj
	STA	PMJ_LIMIT50

	CALL	WIPE_CURRENT_CARD_DATA_FUNC	;zero "current" variables

	LXI	H,SERSTR_CURR	;zero card serial #
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,'$'


	LXI	H,FTSTR_CURR	;zero fiber type string
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,'$'


	LXI	H,100	;init to 100% fiber transmission
	SHLD	FCAL_CURR

	MVI	A,2
	STA	COMM_FORGIVE

	CALL	CLEAN_ACCOUNTS_FUNC

	MVI	A,'Y'	;init comm error flags to ok
	STA	INBUFF_LRC_OK

	CALL	SET_LIMITS_FUNC	;set min, max power limits

	CALL	ESETUP_FUNC	;get evreq, dacval

	MVI	A,'N'
	STA	TEST_FIRE_OK	;requires test fire before READY

	MVI	A,'R'
	STA	CARD_OK
	STA	INT_CARD_STATUS

	CALL	CARD_NOT_PRESENT_ATT_FUNC

	CALL	RESTORE_DISPLAY_FUNC
	MVI	A,'Y'
	STA	NP_ATT_FLAG

	MVI	A,'N'
	STA	NR_ATT_FLAG
	STA	PB_ATT_FLAG
	STA	ZP_ATT_FLAG
	JMP	KEYBOARD_MONITOR

MC_PD:	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK

	LDA	NR_ATT_FLAG	;already warned?
	CPI	'Y'	;if so, no att
	JZ	MPD_NOT

	CALL	CARD_READER_ATT_FUNC
	MVI	A,'R'
	STA	INT_CARD_STATUS
	CALL	RESTORE_DISPLAY_FUNC
	MVI	A,'Y'
	STA	NR_ATT_FLAG

	MVI	A,'N'
	STA	NP_ATT_FLAG
	STA	PB_ATT_FLAG
	STA	ZP_ATT_FLAG

MPD_NOT:MVI	A,'R'
	STA	INT_CARD_STATUS
	JMP	KEYBOARD_MONITOR

PIN_BAD:MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK

	LDA	PB_ATT_FLAG	;already warned?
	CPI	'Y'	;if so, no att
	JZ	MPB_NOT

	CALL	CARD_INVALID_ATT_FUNC
	MVI	A,'V'
	STA	INT_CARD_STATUS
	CALL	RESTORE_DISPLAY_FUNC
	MVI	A,'Y'
	STA	PB_ATT_FLAG

	MVI	A,'N'
	STA	NP_ATT_FLAG
	STA	NR_ATT_FLAG
	STA	ZP_ATT_FLAG

MPB_NOT:MVI	A,'V'
	STA	INT_CARD_STATUS
	JMP	KEYBOARD_MONITOR

MZERO_PROCS:		;card can have zero procs, but account still
	LDA	PROC_ALIVE	;has 1 procedure (probably in process)
	CPI	'Y'	;don't want to deal with zero procs til
	JZ	MT_AOK	;procedure completed and account is zero

	MVI	A,'N'	;flag card status doesn't allow 'Ready'
	STA	CARD_OK

	LDA	ZP_ATT_FLAG	;already warned?
	CPI	'Y'	;if so, no att
	JZ	MZ_NOT

	CALL	CARD_EMPTY_ATT
	MVI	A,'Z'
	STA	INT_CARD_STATUS
	CALL	RESTORE_DISPLAY_FUNC
	MVI	A,'Y'
	STA	ZP_ATT_FLAG

	MVI	A,'N'
	STA	NP_ATT_FLAG
	STA	NR_ATT_FLAG
	STA	PB_ATT_FLAG

MZ_NOT:	MVI	A,'Z'
	STA	INT_CARD_STATUS
	JMP	KEYBOARD_MONITOR

MCCC_NC:

;ok, new card detected.
;initialize card variables
			;we've now got a new fcal (xmission) value
			;for this card, new power limit, new starts
			;for power, reprate so setup reprate,
			;set new min, max limits
	LDA	REPRATE
	CPI	20	;get proper timer value for new reprate ala card
	JNZ	MCCC_30
	MVI	A,49
	STA	RTEMP	;load timer variables
	STA	RCNT
	LXI	H,RTAB	;load rep table pointer for this reprate
	SHLD	RTPOINT
	JMP	RR_SETUP_DONE

MCCC_30:CPI	30
	JNZ	MCCC_40
	MVI	A,32
	STA	RTEMP	;load timer variables
	STA	RCNT
	LXI	H,RTAB+1	;load rep table pointer for this reprate
	SHLD	RTPOINT
	JMP	RR_SETUP_DONE

MCCC_40:CPI	40
	JNZ	MCCC_50
	MVI	A,24
	STA	RTEMP	;load timer variables
	STA	RCNT
	LXI	H,RTAB+2	;load rep table pointer for this reprate
	SHLD	RTPOINT
	JMP	RR_SETUP_DONE

MCCC_50:CPI	50
	JNZ	MCCC_60
	MVI	A,19
	STA	RTEMP	;load timer variables
	STA	RCNT
	LXI	H,RTAB+3	;load rep table pointer for this reprate
	SHLD	RTPOINT
	JMP	RR_SETUP_DONE

MCCC_60:MVI	A,16
	STA	RTEMP	;load timer variables
	STA	RCNT
	LXI	H,RTAB+4	;load rep table pointer for this reprate
	SHLD	RTPOINT

RR_SETUP_DONE:
	CALL	SET_LIMITS_FUNC	;set min, max power limits

	CALL	ESETUP_FUNC	;get evreq, dacval

	MVI	A,'N'
	STA	TEST_FIRE_OK	;requires test fire before READY

	MVI	A,'N'	;force at least one read before use
	STA	CARD_OK
	STA	NEW_CARD

	MVI	A,'N'	;this will cause debit with next 60 sec. use
	STA	SEC_PROC_SYNCH
	LXI	H,0	;reset procedure timer
	SHLD	PROC_TIME
	MVI	A,'N'	;flag that procedure is over, not current
	STA	PROC_ALIVE
	STA TREATMENT_GREATER_30_SECS			

;	CALL	READ_CAL_ATT_FUNC		;reading calibration data

	JMP	KS	;and force standby

MT_AOK:	MVI	A,'N'
	STA	NP_ATT_FLAG
	STA	NR_ATT_FLAG
	STA	PB_ATT_FLAG
	STA	ZP_ATT_FLAG

	LDA	INT_CARD_STATUS
	CPI	'Y'		;if data not yet read, display
	JZ	MT_ND		;if not pending, no display

	CALL	BEEP_FUNC

	LDA	INT_CARD_STATUS
	CPI	'D'		;if decrementing, no cal_att
	JZ	MNO_CAL_ATT

	CALL	READ_CAL_ATT_FUNC		;reading calibration data

MNO_CAL_ATT:
	MVI	A,'Y'	;set internal card status to read ok
	STA	INT_CARD_STATUS	;need to do here for restore display to be ok
	STA	CARD_OK
	CALL	RESTORE_DISPLAY_FUNC		;display

MT_ND:	MVI	A,'Y'	;set internal card status to read ok
	STA	INT_CARD_STATUS
	STA	CARD_OK

	MVI	A,20
	STA	RC_COMM_TIMEOUT	;reload timer


*******************************************************************************

;check key board
KEYBOARD_MONITOR:	CALL	DEBOUNCE_DELAY_5MS_FUNC
	CALL	POLL_KEYBOARD_FUNC	;check for kb input
	CPI	0FFH	;valid input?
	JZ	KC1	;if not, check scan line 1

	JMP	KNORM	;see what is requested

KC1:	CALL	KEYBOARD_SCAN_LINE1_FUNC	;check for kb input
	CPI	0FFH	;valid input?
	JZ	KC2	;if not, check scan line 2
	JMP	KNORM1

KC2:	CALL	KEYBOARD_SCAN_LINE2_FUNC	;check for kb input
	CPI	0FFH	;valid input?
	JZ	RDYCHK	;if not, check if in ready or aiming mode
	JMP	KNORM2

RDYCHK:	LDA	MODEFLG
	CPI	'S'
	JZ	MAIN	;if not in 'ready' or 'aiming' mode, then loop
	JMP	FTCHECK	;else, check ftsw


	;jump to appropriate routine
KNORM:	LDA	KEYBOARD_DEBOUNCE_STORAGE0	;get kb data
	CPI	STBY_AND_AIM_BTNS	;double key depression?
	JZ	SHOW_REV	;if yes, display revision level
	CPI	STBREQ_BTN	;standby request?
	JZ	KS
	CPI	RDYREQ_BTN	;rdy?
	JZ	KR
	CPI	AIMREQ_BTN	;aiming mode?
	JZ	KA
	CPI	INCREQ_BTN	;increment energy?
	JZ	KEN	;if yes, increment energy
	CPI	DECREQ_BTN	;decrement energy?
	JZ	KEN	;if not, decrement energy
	JMP	MAIN

KNORM1:	LDA	KEYBOARD_DEBOUNCE_STORAGE1
	CPI	PPSREQ_BTN	;rep rate request
	JZ	KREP	;yes, jump to reprate routine
	CPI	CAL_REQ_BTN	;calibration ?
	JZ	KCAL	;yes, jump to cal routine
	JMP	MAIN

KNORM2:	LDA	KEYBOARD_DEBOUNCE_STORAGE2
	CPI	TOTALS_REQ_BTN	;totals display ?
	JZ	KTD

	LDA	MODEFLG	;reset site counter in READY only
	CPI	'R'
	JNZ	MAIN

	LDA	KEYBOARD_DEBOUNCE_STORAGE2
	CPI	RESET_REQ_BTN	;reset site counter?
	JZ	KRSC

;patch
;	CPI	CHANGE_CARD_REQ	;new card?
;	JZ	CHANGE_CARD
;patch

	JMP	MAIN

;set standby mode
KS:	LDA	MODEFLG	;if already in stby, loop
	CPI	'S'
	JZ	MAIN

	CALL	BEEP_FUNC	;audio feedback that key closure detected
	CALL	SET_STANDBY_FUNC
	CALL	KREL_FUNC	;wait for key release
	JMP	MAIN

;set ready mode requested
KR:	LDA	CARD_OK	;is current card ok to use?
	CPI	'Y'
	JZ	KCARD_OK

	CALL	CARD_INVALID_ATT_FUNC	;if not, tell operator
	CALL	RESTORE_DISPLAY_FUNC
	JMP	MAIN


KCARD_OK:
	LXI	H,FIVE_MIN_TIMEOUT	;set five minute timer
	SHLD	FIVEMIN

	IN	SSTATA	;is foot switch active?
	ANI	FTMASK
	CPI	2
	JNZ	NEXT_RDY_CHECK	;if ftsw ok, continue

	CALL	RESET_DISPLAY_FUNC

	LXI	H,FOOTSW_STUCK_STR
	CALL	PUTSTR_FUNC
	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,FOOTSW_STUCK_STR2
	CALL	PUTSTR_FUNC
	CALL	BLOOP_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	RESTORE_DISPLAY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN	;don't go to ready if active

NEXT_RDY_CHECK:
	LDA	EVREQ	;if evreq = 0, don't go ready
	ORA	A
	JNZ	NEXT_RDY_CHECK2

	CALL	RESET_DISPLAY_FUNC
	LXI	H,SEL_POWERSTR
	CALL	PUTSTR_FUNC
	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,SEL_POWERSTR2
	CALL	PUTSTR_FUNC
	CALL	BLOOP_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	RESTORE_DISPLAY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN	;don't go to ready if no energy selected



NEXT_RDY_CHECK2:
	LDA	TEST_FIRE_OK	;if successful testfire within last 3 min.,
			;then ok to go to ready
	CPI	'Y'
	JZ	TEST_FIRE_IS_OK
			;else, do test fire

	CALL	BEEP_FUNC	;acknowledge key entry

	CALL	RESET_DISPLAY_FUNC
	LXI	H,FRESH_TEST_FIRE_STR
	CALL	PUTSTR_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC

	CALL	KREL_FUNC	;first wait for release of 'ready' button
			;so tfire doesn't abort
	CALL	RESTORE_DISPLAY_FUNC
	CALL	TESTFIRE_FUNC

	LDA	TEST_FIRE_OK	;if successful testfire
			;then ok to go to ready
	CPI	'Y'
	JZ	TEST_FIRE_IS_OK

	JMP	MAIN	;else stay in standby


TEST_FIRE_IS_OK:
	LXI	H,FIVE_MIN_TIMEOUT	;set five minute timer
	SHLD	FIVEMIN

	LDA	FIBSTAT	;determine if fiber installed
	CPI	'N'	;if not att. and standby
	JNZ	KRFOK

	CALL	FATT_FUNC	;if not installed, att and stby
	CALL	SET_STANDBY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN

KRFOK:	CALL	BEEP_FUNC	;audio feedback that key closure detected
	CALL	OUTPUT_DISPLAY_FUNC	;restore display

	MVI	A,'R'           ;set ready mode
	STA	MODEFLG

FCOK:	CALL	RESET_DISPLAY_FUNC

	LDA	CBYTE2
	ANI	LEMON	;turn on laser emission leds
	OUT	CPORT2
	STA	CBYTE2

	MVI	A,2
	STA	KEYBOARD_DEBOUNCE_STORAGE0	;store loop count
MRS_LOOP:
	LXI	H,RSIGN	;write ready signon string
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;point to second line
	LXI	H,RSIGN2	;write rest of signon
	CALL	PUTSTR_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	BEEP_FUNC

	CALL	RESET_DISPLAY_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	BEEP_FUNC
	LDA	KEYBOARD_DEBOUNCE_STORAGE0
	DCR	A
	STA	KEYBOARD_DEBOUNCE_STORAGE0
	JNZ	MRS_LOOP

	CALL	RESTORE_DISPLAY_FUNC

	MVI	A,'N'	;init overtemp alarm flag
	STA	OTAFLG

	CALL	KREL_FUNC	;wait for key release

	LDA	CBYTE
	ANI	AIM_ON	;turn on AIM
	STA	CBYTE
	OUT	CPORT

	LDA	CBYTE2
	ANI	LEMON	;turn on laser emission leds
	OUT	CPORT2
	STA	CBYTE2

	CALL	RESTORE_DISPLAY_FUNC
	CALL	DELAY_500MS_FUNC

	MVI	A,'N'	;force new aim warning if necessary
	STA	AIM_ATT_STAT

	LDA	AIM_OK_FLAG	;check aim status
	CPI	'Y'
	JZ	ROK
	CALL	AIM_ATT_FUNC

ROK:	JMP	MAIN

;set aiming mode request

KA:	LDA	FIBSTAT	;determine if fiber installed
	CPI	'N'	;if not att. and standby
	JNZ	KAFOK

	CALL	FATT_FUNC	;if not installed, att and stby
	CALL	SET_STANDBY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN

KAFOK:	CALL	BEEP_FUNC	;audio feedback that key closure detected

	LDA	CBYTE
	ANI	AIM_ON	;turn on AIM
	STA	CBYTE
	OUT	CPORT
	MVI	A,'A'         	;set aiming mode
	STA	MODEFLG
	CALL	MODE_DISPLAY_FUNC
	CALL	KREL_FUNC
	MVI	A,'N'	;force new aim warning if necessary
	STA	AIM_ATT_STAT

	MVI	A,'N'	;init overtemp alarm flag
	STA	OTAFLG

	LDA	CBYTE2
	ANI	LEMON	;turn on laser emission leds
	OUT	CPORT2
	STA	CBYTE2

	CALL	RESTORE_DISPLAY_FUNC	;restore display
	CALL	KREL_FUNC
	JMP	MAIN

;display revision level
SHOW_REV:
	CALL	BEEP_FUNC
	CALL	REVISION_DISPLAY_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	KREL_FUNC
	CALL	RESTORE_DISPLAY_FUNC
	JMP	MAIN


;select energy requested
KEN:	CALL	ESELECT_FUNC	;select new energy setting
	LDA	TEST_FIRE_OK	;if testfire not successful in eselect, force standby
	CPI	'Y'
	JNZ	KENFAIL
	CALL	KREL_FUNC	;wait for key release
	JMP	MAIN

KENFAIL:CALL	SET_STANDBY_FUNC
	CALL	KREL_FUNC	;wait for key release
	JMP	MAIN

KREP:	CALL	SET_REPRATE	;reprate set routine
	CALL	KREL_FUNC
	LDA	EVREQ	;if power = 0, then force standby
	ORA	A
	JNZ	MAIN
	JMP	KS	;if 0, force standby

KCAL:	MVI	A,'N'	;force new warning if necessary
	STA	AIM_ATT_STAT

	CALL	CALIBRATE_FUNC	;do calibrate routine
	JMP	MAIN

KTD:	CALL	BEEP_FUNC
	CALL	DISPLAY_TOTALS_FUNC
	CALL	RESTORE_DISPLAY_FUNC
	JMP	MAIN

KRSC:	CALL	BEEP_FUNC	;reset site_time and all rr accumulators
	LXI	H,0
	SHLD	SITE_TIME
	SHLD	SC_20
	SHLD	SC_201
	SHLD	SC_30
	SHLD	SC_301
	SHLD	SC_40
	SHLD	SC_401
	SHLD	SC_50
	SHLD	SC_501


	CALL	SITE_TIME_DISPLAY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN

;patch

;	EXTERN	FAKE_CARD_ID
;	EXTERN	D_OKR


;CHANGE_CARD:
;	LDA	FAKE_CARD_ID	;this simulates a number of different cards
;	CPI	1
;	JZ	FC_1
;	CPI	2
;	JZ	FC_2
;	CPI	3
;	JZ	FC_3
;	CPI	4
;	JZ	FC_4

;FC_1:	LXI	H,FCARD_1	;card i.d. string
;	INR	A	;point to next card string
;	STA	FAKE_CARD_ID
;	JMP	LOAD_FC
;FC_2:	LXI	H,FCARD_2	;card i.d. string
;	INR	A	;point to next card string
;	STA	FAKE_CARD_ID
;	JMP	LOAD_FC
;FC_3:	LXI	H,FCARD_3	;card i.d. string
;	INR	A	;point to next card string
;	STA	FAKE_CARD_ID
;	JMP	LOAD_FC
;FC_4:	LXI	H,FCARD_4	;card i.d. string
;	MVI	A,1	;roll-over point to next card string
;	STA	FAKE_CARD_ID
;	JMP	LOAD_FC


;LOAD_FC:		;load fake i.d. into ram
;	LXI	D,D_OKR	;buffer in RAM
;	MVI	C,44	;number of chars to move

;LFC_LOOP:
;	MOV	A,M
;	STAX	D
;	INX	D
;	INX	H
;	DCR	C
;	JNZ	LFC_LOOP
;	CALL	KREL_FUNC
;	JMP	MAIN


;FCARD_1:DB	'&0065B12345NS16051000904900000000000354782r$'
;FCARD_2:DB	'&0064B12345NS16041000904900000000000359782q$'
;FCARD_3:DB	'&0063C12345NS16031000904900000000000359782q$'
;FCARD_4:DB	'&0062D12345NS16041000904900000000000359782q$'



;check periodically: 5 min timer, fiber disconnect, foot switch if in ready mode
FTCHECK:
	LDA	FIBSTAT	;determine if fiber installed
	CPI	'N'	;if not att. and standby
	JNZ	FTFOK
	
	LDA TREATMENT_GREATER_30_SECS		; has 30 seconds of energy treatment been performed?
	CPI 'N'
	JZ	FTCHECK_CONT	; yes, less than 30 seconds
	
	CALL	FATT_FUNC	;if not installed, att and stby
	LDA 'N'
	STA FOOT_SWITCH_TEST_REQ
	CALL DELAY_100MS_FUNC
	JMP 	INIT_FIBER_REMOVAL_IN_TREATMENT

FTCHECK_CONT:
	CALL	FATT_FUNC	;if not installed, att and stby
	CALL	SET_STANDBY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN

FTFOK:
	LDA	MODEFLG	;if in aiming (i.e. not ready), end of loop here
	CPI	'R'
	JNZ	MAIN

	LDA	CARD_OK	;if card status has changed to not 'y'
	CPI	'Y'	;then return to standby
	JZ	MR_CARD_OK

	CALL	SET_STANDBY_FUNC
	CALL	KREL_FUNC
	JMP	MAIN


MR_CARD_OK:
	LHLD	FIVEMIN	;check if any activity for last minute
	MOV	A,H
	ORA	A
	JZ	KS	;time - out forces standby

         ; The system shall shut down in Green Mode after a specified period (1 hour)  of inactivity from the foot-switch
	LXI	H,GREEN_MODE_PERIOD_MSB
	SHLD	ONE_HOUR_MSB
	LXI	H,GREEN_MODE_PERIOD_LSB
	SHLD	ONE_HOUR_LSB	;it's two 16-bit words, so load as two hex quantities

	IN	SSTATA	;is foot switch active?
	ANI	FTMASK
	CPI	2
	JNZ	MAIN

;patch to simulate treat

;fake procedure time

;FAKE:	LXI	D,350	;35 secs

;	LHLD	PROC_TIME	;add local secs to total procedure secs
;	DAD	D
;	SHLD	PROC_TIME

;	LXI	D,0	;add minutes

;	LHLD	PROC_TIME
;	DAD	D
;	SHLD	PROC_TIME


;STOPPIT:		;debug stop point

;	LXI	H,FCARD_4	;card i.d. string
;	MVI	A,1	;roll-over point to next card string
;	STA	FAKE_CARD_ID
			;fake card swap during treat
			;load fake i.d. into ram
;	LXI	D,D_OKR	;buffer in RAM
;	MVI	C,44	;number of chars to move

;FTC_LOOP:
;	MOV	A,M
;	STAX	D
;	INX	D
;	INX	H
;	DCR	C
;	JNZ	FTC_LOOP

;FTC_DONE:
;	NOP

;patch to simulate treat


	CALL	TREAT_FUNC	;if so, treatment

;	LDA	SEC_PROC_SYNCH	;if > required seconds in procedure, and procedure
;	CPI	'Y'	;debited, this flag = 'y'
;	JZ	SP_SYNCHED

;	LHLD	PROC_TIME	;get treat seconds during this procedure
;	LXI	D,300	;value is 10X
;	CALL	COMPARE_16_FUNC	;see if > 30 secs
;	CPI	'L'	;if de (30) less than hl, then decr procedure
;	JNZ	SP_SYNCHED	;else, skip

;	LDA	PROC_COUNT_CURR	;Decrement Procedure Count
;	STA	PROC_COUNT_DISPLAYED	;display "alive" proc til expired or card removed
;	ORA	A	;clip at 0 (in case)
;	JZ	SP_SYNCHED
;	DCR	A
;	STA	PROC_COUNT_CURR

;	CALL	FIND_ACCOUNT_FUNC	;look at current account
;	CALL	UPDATE_ACCOUNT_PROCEDURES_FUNC	;adjust account

;	MVI	A,'Y'
;	STA	SEC_PROC_SYNCH

;SP_SYNCHED:

	LHLD	PROC_TIME	;have we expired 60 mins (lipo) or 15 mins (CTEV)?
			; if yes, close out


	LDA	FTSTR_CURR+2	;mode info is in this string
	CPI	'L'	;"__LIPO" or "__CTEV"
	JZ	SPS_LIPO

SPS_CTEV:
	LXI	D,MAX_TREATMENT_PERIOD	; configure the max treatment period, used to disable treatment

	JMP	SPS_CONT

SPS_LIPO:
	LXI	D,36000	;(10X) 3600 secs = 60 mins

SPS_CONT:
	CALL	COMPARE_16_FUNC	;
	CPI	'L'	;if de (36000 LIPO, or 9000 CTEV) less than hl, then procedure closed
	JNZ	MPROC_ALIVE	;else, skip

	MVI	A,'N'	;this will cause debit with next 30 sec. use
	STA	SEC_PROC_SYNCH
	LXI	H,0	;reset procedure timer
	SHLD	PROC_TIME
	MVI	A,'N'	;flag that procedure is over, not current
	STA	PROC_ALIVE
	STA TREATMENT_GREATER_30_SECS			

	LDA	PROC_COUNT_CURR
	STA	PROC_COUNT_DISPLAYED	;display current as now expired

	CALL	DELAY_100MS_FUNC	;discourage shutter chatter
	JMP	MAIN	;continue main loop

MPROC_ALIVE:
	MVI	A,'Y'	;flag that procedure is still current
	STA	PROC_ALIVE	;this is useful if card = 0 (see mcard_check)

	CALL	DELAY_100MS_FUNC	;discourage shutter chatter
	JMP	MAIN	;continue main loop

SEL_POWERSTR:		DB	'  SET POWER BEFORE  $'
SEL_POWERSTR2:		DB	'SELECTING READY MODE$'
FRESH_TEST_FIRE_STR:	DB	'TEST FIRE NECESSARY $'
FOOTSW_STUCK_STR:	DB	' FOOT SWITCH ACTIVE $'
FOOTSW_STUCK_STR2:	DB	'RELEASE FOOT SWITCH $'
RDYBLNK:		DB	'READY  $'
RDYSTR:			DB	'READY  $'
RSIGN:			DB	'READY MODE SELECTED $'
RSIGN2:			DB	'   LASER ENABLED    $'
AIMSTR:			DB	'AIMING $'

	;*******************************
	;* 			GREEN MODE		;*
	;* ONE HOUR TIMEOUT ROUTINE ;*
	;*                            ;*
	;*******************************
	
	; Re-initialize inactivity timer to the maximum time for inactivity
    ; The system shall shut down in Green Mode after a specified period (1 hour)  of inactivity from the foot-switch, or Fiber Test
GREEN_MODE_INIT_FUNC:
	DI	;Disable Interrupts
	PUSH H 
	LXI	H,GREEN_MODE_PERIOD_MSB
	SHLD	ONE_HOUR_MSB
	LXI	H,GREEN_MODE_PERIOD_LSB
	SHLD	ONE_HOUR_LSB	;it's two 16-bit words, so load as two hex quantities
	POP H
	EI	; Enable Interrupts
	RET

;powers down machine when system inactive for one hours

PULL_DE_PLUG:
	MVI	A,SAFEMSK	;all safe
	OUT	CPORT

	MVI	A,0FFH
	OUT	CPORT2	; "laser emission" leds off

	;MVI	A,0	;power down (unlatch power relay)
	;OUT	RELAY

	XRA	A	;disable main control port
	OUT	OCONT


;Powering down warning

	CALL	RESET_DISPLAY_FUNC

	LXI	H,PDP_STR
	CALL	PUTSTR_FUNC
	MVI	A,20	;2nd line
	CALL	SET_CURSOR_POSITION_FUNC
	LXI	H,PDP_STR2
	CALL	PUTSTR_FUNC
	CALL	BLOOP_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC

	CALL	BLOOP_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC

	CALL	BLOOP_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC

	MVI	A,'Y'	;non-vol flag that I did this
	STA	PDP_FLAG

	MVI	A,0	;power down (unlatch power relay)
	OUT	RELAY

PDP:	JMP	PDP	;wait for pd interrupt


PDP_STR:	DB	' 1 HOURS UNATTENDED $'
PDP_STR2:	DB	'   SHUTTING DOWN    $'





	END

