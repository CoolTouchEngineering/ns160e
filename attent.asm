;$PG
$PL=59
;$TL(NEW STAR 160D ATTENT.ASM SOURCE FILE)

	INCLUDE "INCLUDE.H"

;$Header: C:/Dosland/ns160e/rcs/attent.asm 1.50 2012/11/15 14:30:33Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------
; ROUTINE NAMES:

	GLOBAL	FATT_FUNC
	GLOBAL	FTATT_FUNC
	GLOBAL	BATT_FUNC
	GLOBAL	AIM_ATT_FUNC

; PROJECT: NS160E
;--------------------------------------------------------- 

	DEFSEG	ATTENT_SEG,CLASS=CODE
	SEG	ATTENT_SEG


; DATA INPUTS: 
	EXTERN	SERFLG
	EXTERN	FIBSTAT
	EXTERN	TMPSTAT
	EXTERN	FORGIVE
	EXTERN	READY_AIM_FLAG
	EXTERN	AIM_OK_FLAG
	EXTERN	AIM_FORGIVENESS_COUNTER

; DATA OUTPUTS
	EXTERN	RUNNING_STATUS_COUNTER
	EXTERN	CBYTE
	EXTERN	CBYTE2
	EXTERN	AIM_ATT_STAT
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE0
 
; ROUTINES CALLED BY: 

; ROUTINES CALLED
	EXTERN	ERROR
	EXTERN	DEBOUNCE_DELAY_5MS_FUNC
	EXTERN	INIT
	EXTERN	DACOUT_FUNC
	EXTERN	BLOOP_FUNC
	EXTERN	PUTSTR_FUNC
	EXTERN	DELAY_500MS_FUNC
	EXTERN	DELAY_200MS_FUNC
	EXTERN	DUMP_FUNC
	EXTERN	TREAD_FUNC
	EXTERN	THDISPL
	EXTERN	RESTORE_DISPLAY_FUNC
	EXTERN	SET_CURSOR_POSITION_FUNC
	EXTERN	RESET_DISPLAY_FUNC
	EXTERN	SET_STANDBY_FUNC

$EJECT
;$AP

	;*************************
	;*  ATTENTION ROUTINES  ;*
	;*                      ;*
	;*************************
;program jumps to these when attention condition detected

	;***FATT_FUNC***
	;FATT_FUNC - displays 'fiber not installed'
	;called by - main,treat
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - PUTSTR_FUNC
FATT_FUNC:	LDA	CBYTE
	ORI	AIM_OFF	;turn off aiming
	STA	CBYTE
	OUT	CPORT

	LDA	CBYTE2
	ORI	LEMOFF	;turn off laser emission leds
	OUT	CPORT2	
	STA	CBYTE2

	CALL	BLOOP_FUNC
	CALL	DUMP_FUNC

	CALL	RESET_DISPLAY_FUNC

	MVI	A,4
	STA	KEYBOARD_DEBOUNCE_STORAGE0	;store loop count
FIB_LOOP:
	LXI	H,FATSTR	;write 'fiber not installed '
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;point to 2nd line
			;position cursor
	
	LXI	H,FATSTR2	
	CALL	PUTSTR_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	RESET_DISPLAY_FUNC
	CALL	DELAY_200MS_FUNC
	LDA	KEYBOARD_DEBOUNCE_STORAGE0
	DCR	A
	STA	KEYBOARD_DEBOUNCE_STORAGE0
	JNZ	FIB_LOOP
	RET

FATSTR:		DB	'FIBER NOT INSTALLED $'
FATSTR2:	DB	'                    $'

	;***FTATT_FUNC***
	;ftatt - displays 'foot switch not installed'
	;called by - main
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - PUTSTR_FUNC

FTATT_FUNC:	CALL	BLOOP_FUNC
	CALL	RESET_DISPLAY_FUNC
	CALL	DUMP_FUNC

	MVI	A,4
	STA	KEYBOARD_DEBOUNCE_STORAGE0	;store loop count
FTT_LOOP:
	LXI	H,FTSTR1	;write 'foot switch    '
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;point to 2nd line
	LXI	H,FTSTR2	;write 'not installed  '
	CALL	PUTSTR_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	RESET_DISPLAY_FUNC
	CALL	DELAY_200MS_FUNC
	LDA	KEYBOARD_DEBOUNCE_STORAGE0
	DCR	A
	STA	KEYBOARD_DEBOUNCE_STORAGE0
	JNZ	FTT_LOOP

	LXI	H,FTSTR1	;write 'foot switch    '
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;point to 2nd line
	LXI	H,FTSTR2	;write 'not installed  '
	CALL	PUTSTR_FUNC

FTRW:	CALL	DELAY_500MS_FUNC
	IN	SSTATA	;is foot switch restored to legal pos?
	ANI	FTMASK
	CPI	1	;yep
	JNZ	FTRW	;wait for foot sw. 
	MVI	A,255	;restore "forgiveness" counter
	STA	FORGIVE
	RET

FTSTR1:		DB	'     FOOTSWITCH     $'
FTSTR2:		DB	'   NOT INSTALLED    $'

;safety interlock attention routine

	;****BATT_FUNC****
BATT_FUNC:	CALL	SET_STANDBY_FUNC
	CALL	DUMP_FUNC
	CALL	BLOOP_FUNC

	LDA	CBYTE	;set safe signals
	ORI	SAFEMSK	;02eh, aim,shut,trigger,hvps disabled
	STA	CBYTE
	OUT	CPORT

	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home

BATT_LOOP:
	LHLD	RUNNING_STATUS_COUNTER	;bump safety counter
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER	

	LXI	H,BRHSTR	;write 'interlock open'
	CALL	PUTSTR_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC

	IN	SSTATA	;recovered?
	ANI	INTERLOCK	;check interlock
	JZ	BATT_DONE	;if o.k., return

	CALL	RESET_DISPLAY_FUNC
	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC
	JMP	BATT_LOOP

BATT_DONE:
	CALL	RESTORE_DISPLAY_FUNC
	RET


BRHSTR:		DB	'   INTERLOCK OPEN   $'


	;**AIM_ATT_FUNC***
	;displays 'aiming beam malfunction'
	;called by - main,attent,ftest,modeset,ready
	;calling parameters - none
	;returns - none
	;registers used - all
	;calls - PUTSTR_FUNC
AIM_ATT_FUNC:LDA	AIM_ATT_STAT	;warning already given?
	CPI	'Y'
	RZ		;if so, return

	CALL	BLOOP_FUNC
	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home
	
	LXI	H,AIM_STR1	;write 'aiming beam'
	CALL	PUTSTR_FUNC
	MVI	A,20
	CALL	SET_CURSOR_POSITION_FUNC	;point to 2nd line
	LXI	H,AIM_STR2	;write 'malfunction'
	CALL	PUTSTR_FUNC

	MVI	C,4	;wait 2 seconds
AIM_ATT_LOOP:
	CALL	DELAY_500MS_FUNC
	DCR	C
	JNZ	AIM_ATT_LOOP

	MVI	A,'Y'
	STA	AIM_ATT_STAT	;flag that warning already given

	CALL	RESTORE_DISPLAY_FUNC
	RET

AIM_STR1:	DB	'    AIMING BEAM     $'
AIM_STR2:	DB	'    MALFUNCTION     $'



	END
