;$PG
$PL=59
;$TL(NS160E INIT.ASM SOURCE FILE)
	INCLUDE "INCLUDE.H"
;$Header: C:/Dosland/ns160e/rcs/init.asm 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $
;---------------------------------------------------------

	DEFSEG	INITSEG,CLASS=CODE,START=0H
	SEG	INITSEG

; ROUTINE NAMES:

	GLOBAL	INIT
	GLOBAL  INIT_FIBER_REMOVAL_IN_TREATMENT

;GLOBAL SYMBOLS:
	GLOBAL	REV


; PROJECT: NS160E
;---------------------------------------------------------


; DATA INPUTS:
	EXTERN	FIBSTAT
	EXTERN 	FIBER_CONNECTED_ONCE
	EXTERN	STK
	EXTERN	PD_FLAG
	EXTERN	ONE_HOUR_LSB
	EXTERN	ONE_HOUR_MSB
	EXTERN  TRY_TRY_AGAIN
	EXTERN  FTSW_STATUS
	EXTERN  LAST_FTSW
	EXTERN	FTSW_PREVIOUS_STATUS
	EXTERN	FOOT_SWITCH_TEST_REQ

; DATA OUTPUTS
	EXTERN	ROMEND
	EXTERN	CHKSUM
	EXTERN	BATFLG
	EXTERN	MODEFLG
	EXTERN	CBYTE
	EXTERN	CBYTE2
	EXTERN	CHKCNT
	EXTERN	MEASURED_ENERGY_OK
	EXTERN	REPRATE
	EXTERN	RTAB
	EXTERN	RTPOINT
	EXTERN	RCNT
	EXTERN	RTEMP
	EXTERN	EBUFFSIZE
	EXTERN	ETOP
	EXTERN	TOTAL_PULSES
	EXTERN	TOTAL_PULSES1
	EXTERN	TOTAL_TIME
	EXTERN	OTSTAT
	EXTERN	TEMPER
	EXTERN	TMPSTAT
	EXTERN	TBOUNCE
	EXTERN	FORGIVE
	EXTERN	SERFLG
	EXTERN	RUNNING_STATUS_COUNTER
	EXTERN	ESTEP
	EXTERN	REQUESTED_POWER
	EXTERN	MIN_POWER
	EXTERN	EMVALA
	EXTERN	EMVALB
	EXTERN	EMVALE
	EXTERN	AVVAL
	EXTERN	AVVALB
	EXTERN	AVVALE
	EXTERN	TEVAL
	EXTERN	TEVAL1
	EXTERN	KEYBOARD_DEBOUNCE_STORAGE1
	EXTERN	SERVICE
	EXTERN	AIM_ATT_STAT
	EXTERN	AIM_OK_FLAG
	EXTERN	READY_AIM_FLAG

	EXTERN	SEC_PROC_SYNCH
	EXTERN	PROC_TIME
	EXTERN	CARD_ERROR_CODE
	EXTERN	CARD_PIN
	EXTERN	CARD_STATUS
	EXTERN	INT_CARD_STATUS

	EXTERN	SAD_POINTER
	EXTERN	FA_POINTER
	EXTERN	ACCOUNT_POINTER

	EXTERN	CARD_COMM_STAT
	EXTERN	CHAR_WRITTEN_OK0
	EXTERN	CHAR_READ_OK0
	EXTERN	CHAR_WRITTEN_OK1
	EXTERN	CHAR_READ_OK1
	EXTERN	CHAR_WRITTEN_OK2
	EXTERN	CHAR_READ_OK2
	EXTERN	HANDSHAKE_OK
	EXTERN	INBUFF_LRC_OK

	EXTERN	SERSTR_CURR
	EXTERN	FTSTR_CURR
	EXTERN	FCAL_CURR
    	EXTERN	PROC_COUNT_CURR
    	EXTERN	PROC_COUNT_DISPLAYED

	EXTERN	POWER_START
	EXTERN	POWER_LIMIT
	EXTERN	REPRATE_START

	EXTERN	PMJ_LIMIT20
	EXTERN	PMJ_LIMIT30
	EXTERN	PMJ_LIMIT40
	EXTERN	PMJ_LIMIT50

	EXTERN	RC_COMM_TIMEOUT

	EXTERN	CARD_CHECK_TIMER
	EXTERN	COMM_FORGIVE

	EXTERN	TRY_TRY_AGAIN
	EXTERN	TTA_FLAG

; ROUTINES CALLED BY:
; ROUTINES CALLED
	EXTERN	MAIN
	EXTERN	POWER_DOWN
	EXTERN	EXEC_ISR_1MS
	EXTERN	WDFAIL
	EXTERN	ERROR
	EXTERN	DELAY_500MS_FUNC
	EXTERN	DEBOUNCE_DELAY_5MS_FUNC
	EXTERN	WDST_FUNC
	EXTERN	PUTSTR_FUNC
	EXTERN	BEEP_FUNC
	EXTERN	LVPS_TEST_FUNC
	EXTERN	SERVICE_SWITCH_MONITOR_FUNC
	EXTERN	RESTORE_DISPLAY_FUNC
	EXTERN	INITIALIZE_DISPLAY_FUNC
	EXTERN	RESET_DISPLAY_FUNC
    	EXTERN	SET_CURSOR_POSITION_FUNC
    	EXTERN	WRITE_TO_DISPLAY_FUNC
	EXTERN	START_CLOCK_4800_16X_FUNC
	EXTERN	INIT_COMM_0_FUNC
	EXTERN	INIT_COMM_1_FUNC
	EXTERN	INIT_COMM_2_FUNC

	EXTERN	CLEAN_ACCOUNTS_FUNC

	EXTERN	SET_LIMITS_FUNC
	EXTERN	ESETUP_FUNC

	EXTERN	DELAY_100MS_FUNC
	EXTERN	SW_DEBUG
	EXTERN	SW_DEBUG_TRACE_MSG
	EXTERN 	TREATMENT_GREATER_30_SECS
	;EXTERN DIAGS_E05_RAM_FAIL_MSG	


;$AP

	ORG	0H
	JMP	0100H

	ORG	24H	;trap (power fail interr.)
	JMP	POWER_DOWN

	ORG	2CH	;5.5  (timer 0 (5ms.) interr.)
	JMP	EXEC_ISR_1MS

	ORG	38H	;7.0  (unprogrammed bytes in rom == 0ffh)
	JMP	POWER_DOWN	;0ffh = restart 7 which jumps to 38h

	ORG	3CH	;7.5  (watch dog fail interr.)
	JMP	WDFAIL

	ORG	40H	;copyright message
COPY:	DB	'COPYRIGHT 2004, NEW STAR LASERS INC.'

	;ORG	70H
	;DB	'$Header: C:/Dosland/ns160e/rcs/init.asm 1.50 2012/11/15 14:30:34Z Mike_Brewer Exp Mike_Brewer $'

	ORG	0D0H
REV:	DB	'$Revision: 1.52$c1'

	ORG	0100H

;initialize system

INIT:	DI		;disable interrupts
	MVI	A,'Y'		
	STA FOOT_SWITCH_TEST_REQ	; on warm or cold power up (reset) the foot switch test is required, if Fiber is removed during ready mode with 
								; minimum energy procedure period, then restart system, and do not test foot switch: Y, N value
INIT_FIBER_REMOVAL_IN_TREATMENT:
	DI		;disable interrupts

	LDA	PD_FLAG	;if this flag = 'y', relay reset as "normal" power-down
	CPI	'Y'
	JNZ	KEEP_RELAY_ON

	MVI	A,0	;reset power relay latch
	OUT	RELAY

KEEP_RELAY_ON:
	MVI	A,'N'	;reset this flag, so warm reset doesnt reset relay drive
	STA	PD_FLAG	;if this flag = 'y', relay reset as "normal" power-down

	XRA	A	;force outputs to disabled (safe) state
	OUT	OCONT
	LXI	SP,STK
	MVI	A,TMODE0	;init timers(8254)
	OUT	TCONT
	MVI	A,TMODE1
	OUT	TCONT
	MVI	A,TMODE2
	OUT	TCONT

;init control ports
	MVI	A,SAFEMSK	;all safe
	ORI	SBC_OFF	;also, SBC off for beginning of power-up
	ORI	FAN_SLOW
	OUT	CPORT
	MVI	A,0FFH
	OUT	CPORT2	; "laser emission" leds off

	CALL	DELAY_100MS_FUNC	;delay 200 msec before latching relay
	CALL	DELAY_100MS_FUNC

	MVI	A,1	;set power relay latch
	OUT	RELAY

;init display
DINIT:	CALL	DELAY_500MS_FUNC	;wait for 500 msec display reset

	CALL	INITIALIZE_DISPLAY_FUNC

	MVI	A,16H
	CALL	WRITE_TO_DISPLAY_FUNC

	MVI	A,1BH		;set flickerless
	CALL	WRITE_TO_DISPLAY_FUNC	;escape sequence
	MVI	A,'S'
	CALL	WRITE_TO_DISPLAY_FUNC

	CALL	START_CLOCK_4800_16X_FUNC
	CALL	INIT_COMM_1_FUNC		;comm 1 used for smart card

; init comm 0 for software debug
	;CALL INIT_COMM_0_FUNC
	
;copy non-volatile ram contents to check ram so contents don't trip error
	LXI	H,RAM	;start at bottom
RAM_COPY_LOOP:
	MOV	A,M	;read, then write back
	MOV	M,A	;this copies from non-vol to both ram
	INX	H
	MOV	A,L	;top?
	ORA	H	;if zero, then rolled over top
	JNZ	RAM_COPY_LOOP


;ram test: writes data patterns to ram and checks retention

	LXI	D,TDATA	;point to test data
PUTDAT: LXI	H,RAM	;test first 256 bytes of ram
	LDAX	D
P0:	MOV	M,A	;put test byte in ram
	INR	L
	JNZ	P0
TSTDAT: LXI	H,RAM	;8000h
	LDAX	D	;get test byte
TD:	CMP	M
	JNZ	RERROR
	INR	L
	JNZ	TD

	INX	D
	LDAX	D
	CPI	'$'
	JNZ	PUTDAT

	;CALL DIAGS_E05_RAM_FAIL_MSG_FUNC		; Normally commented, otherwise in test mode "Force RAM Error" message E05 

	JMP	ROM_OK		; Normally commented, otherwise in sw debug mode "Ignore CHecksum calculation". More expediant sw process.
	;JMP	CHKROM		; normally uncommented

RERROR: MVI	A,5	;set ram error flag
	JMP	ERROR


;data for ram test
TDATA:	DB	0,0FFH,55H,0AAH,66H,99H,'$'

;check sum
; original code did not perform a checksum on address 0x40 - 0x100, which is no longer supported
; to allow any PROM reader to easily calculate a checksum on one contiguous memory area
; the code below performs a checksum from 0x0 to ROMEND
;
; Register usage:
;	A - "data" read from memory
;	B - "dataPointer" to code space address
; 	D - "calculated checksum"
;	E -
;	H - "firmware Stored Checksum"
CHKROM: LXI	H,0	;clear hl reg. H shall be the "?"
	LXI	D,0	;clear the 16 bit reg D. Which is D and E (low)
	LXI	B,ROM	;point to beginning of rom (0), initialize "dataPointer"
CSLOOP: LDAX	B	;get data from memory at the address of the "dataPointer", and save to Reg A
	MOV	E,A	;move to the D and E (16 bit) reg.
	DAD	D	;add D and E to the H register
	INX	B	;point to next address
	;MOV	A,B	;skip locations 40h thru ffh (rev. level mess)
	;ORA	A	;hi byte compare
	;JNZ	PASTREV	;if hi byte >0, then past rev message
	;MOV	A,C	;compare low end of copyright+rev
	;CPI	40H
	;JNZ	PASTREV	;not up to rev message yet
	;LXI	B,0100H	;skip 40h - 100h  window

;PASTREV:
	; check if we are at the end of the "designated" code space
	MOV	A,B	;re-load hi byte of pointer
	CPI	HIGH ROMEND	;at end of program?
	JNZ	CSLOOP	;not there yet

	MOV	A,C	;compare with low byte
	CPI	LOW ROMEND
	JNZ	CSLOOP

	; yes, we are at the end of the "designated" code space
	LDA	CHKSUM	;compare total in hl with chksum
	CMP	L
	JNZ	CSERR	; no match: rom error!
	LDA	CHKSUM+1
	CMP	H
	JNZ	CSERR	; no match: rom error!

ROM_OK:	JMP	WD_START

CSERR:	PUSH	H	;make it easy for emulator to show
	POP	H
	MVI	A,6
	JMP	ERROR


WD_START:
	MVI	A,0EH	;enable 5.5 interrupt Timer0 (not wdfail for
	SIM

	CALL	WDST_FUNC	;start  w.d., enable interrupts!

	MVI	A,1	;enable output port
	OUT	OCONT

	MVI	A,0AH	;enable 5.5 int. (5ms timer)
	SIM		;plus enable 7.5 int. (w.d. fail)


;init ram storage

INIT_RAM:		;set all variables to default values
	LXI	H,RAM	;init first 2ffh bytes of ram to 0
ILOOP:	MVI	M,0
	INX	H
	MOV	A,H
	CPI	0E3H
	JC	ILOOP

	; Initialize flags and variables
	MVI	A,'N'
	STA	BATFLG
	STA FIBER_CONNECTED_ONCE	; re-initialize to default

	MVI	A,'S'
	STA	MODEFLG

	MVI	A,SAFEMSK	;init cbyte (control port status)
	ANI	SBC_ON	;turn on SBC (card reader)
	ORI	FAN_SLOW
	STA	CBYTE
	OUT	CPORT

	MVI	A,7	;init cbyte2 and control port 2 (kb scan lines)
	ORI	LEMOFF	;plus laser emission leds off
	ORI	INTOFF	;plus emon integrators reset
	STA	CBYTE2
	OUT	CPORT2

	MVI	A,0FFH	;init safety check counter
	STA	CHKCNT
			;set standby mode
	MVI	A,'S'
	STA	MODEFLG	;set stby mode flag


	MVI	A,'Y'	;init display requested value flag to 'yes'
	STA	MEASURED_ENERGY_OK


	MVI	A,20	;init rep rate to 20 hz
	STA	REPRATE
	LXI	H,RTAB	;init pointer to position 0 in reprate table
	SHLD	RTPOINT	;

	MVI	A,49	;init timer value for 20 hz
	STA	RCNT
	STA	RTEMP

    	LXI	H,100	;fiber transmission default to 100% for set_limits
	SHLD	FCAL_CURR

	MVI	A,80	;8W
	STA	PMJ_LIMIT20

	MVI	A,120	;12W
	STA	PMJ_LIMIT30

	MVI	A,150	;15W
	STA	PMJ_LIMIT40

	MVI	A,150	;15W
	STA	PMJ_LIMIT50


	CALL	SET_LIMITS_FUNC	;set min, max power limits


	LDA	REPRATE	;buff size = 1/2 rep rate
	ORA	A	;clear carry
	RAR		;div by 2
	STA	EBUFFSIZE	;this is energy reading averaging buffer size

	LXI	H,0	;init pulse counter
	SHLD	TOTAL_PULSES
	SHLD	TOTAL_PULSES1
	SHLD	TOTAL_TIME	;init seconds counter

         ; The system shall shut down in Green Mode after a specified period (1 hour)  of inactivity from the foot-switch
	LXI	H,GREEN_MODE_PERIOD_MSB
	SHLD	ONE_HOUR_MSB
	LXI	H,GREEN_MODE_PERIOD_LSB
	SHLD	ONE_HOUR_LSB	;it's two 16-bit words, so load as two hex quantities

	MVI	A,'N'	;init overtemp flag
	STA	OTSTAT
	STA	FTSW_PREVIOUS_STATUS

	MVI	A,70	;init coolant temperature values to
	STA	TEMPER	;nominal values
	STA	TMPSTAT
	MVI	A,5	;init temperature check de-bounce
	STA	TBOUNCE

	MVI	A,255	;init footswitch debounce counter
	STA	FORGIVE

	MVI	A,'Y'	;init aim status to "ok"
	STA	AIM_OK_FLAG
	MVI	A,'N'	;force new aim warning if necessary
	STA	AIM_ATT_STAT
	
	;MVI	A,0		; initialize sw Debug
	;STA	SW_DEBUG        
	;STA	SW_DEBUG_TRACE_MSG        
	
	MVI A,'N'
	STA TREATMENT_GREATER_30_SECS

****************************************************************************
;initialize card variables

	MVI	A,'P'	;init this for 1st read purposes (see main)
	STA	INT_CARD_STATUS
	STA	CARD_STATUS

	MVI	A,'N'
	STA	SEC_PROC_SYNCH	;reset proc ready to debit flag

	LXI	H,0
	SHLD	PROC_TIME

	LXI	H,SERSTR_CURR	;dummy card serial #
	MVI	M,0	;NOT ASCII "0" (30H)
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,'$'


	LXI	H,FTSTR_CURR	;zero fiber type string
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,0
	INX	H
	MVI	M,'$'


	MVI	A,0
	STA	PROC_COUNT_CURR	;procedure count
	STA	PROC_COUNT_DISPLAYED
	STA	SEC_PROC_SYNCH
	STA	CARD_ERROR_CODE

	STA	POWER_START	;init safe start power, reprate
	MVI	A,20
	STA	REPRATE_START

	LXI	H,0
	SHLD	CARD_PIN
	SHLD	PROC_TIME

	SHLD	SAD_POINTER
	SHLD	FA_POINTER
	SHLD	ACCOUNT_POINTER

	LXI	H,100	;init to 100% nominal fiber transmission
	SHLD	FCAL_CURR

	MVI	A,150	;init power limit to 15 W
	STA	POWER_LIMIT


	MVI	A,6
	STA	TRY_TRY_AGAIN	;give plenty of chances
	MVI	A,'N'
	STA	TTA_FLAG

	MVI	A,2
	STA	COMM_FORGIVE

	CALL	CLEAN_ACCOUNTS_FUNC

	MVI	A,'Y'	;init comm error flags to ok
	STA	CARD_COMM_STAT
	STA	CHAR_WRITTEN_OK1
	STA	CHAR_READ_OK1
	STA	HANDSHAKE_OK
	STA	INBUFF_LRC_OK

;patch!!!!


;	EXTERN	D_OKR
;	EXTERN	FAKE_CARD_ID


;	MVI	A,1
;	STA	FAKE_CARD_ID

;	LXI	H,NO_CARD_STR	;point to sim data in ROM
;	LXI	D,D_OKR	;buffer in RAM
;	MVI	C,44	;number of chars to move

;IPP_LOOP:
;	MOV	A,M
;	STAX	D
;	INX	D
;	INX	H
;	DCR	C
;	JNZ	IPP_LOOP
;	JMP	PATCH_DONE


;NO_CARD_STR:	DB	'&0005A12345NS16011000904900000000000359782q$' ;fake no card

;PATCH_DONE:
;	NOP

;patch!!!!
			;if service flg set, skip lengthy delays
	LDA	SERFLG	;has flag been set since power on?
	CPI	'Y'
	JZ	SERVICE?	;if so, skip to service switch check


	MVI	A,0	;sign on message
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,SELSTR
	CALL	PUTSTR_FUNC

	MVI	A,20	;second line
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,SELSTR2
	CALL	PUTSTR_FUNC

	CALL	BEEP_FUNC

;test +15 and -15 volt supplies
LVCHK:	IN	FIFTEEN	;dummy read adc chan 6 to flush trash from
			;power up
	IN	FIFTEEN
	IN	FIFTEEN
	IN	FIFTEEN
	IN	FIFTEEN
	IN	FIFTEEN
	IN	FIFTEEN
	IN	FIFTEEN
	CALL	LVPS_TEST_FUNC	;read for real

			;init all energy settings
	LDA	MIN_POWER	;min_power initialized above (set_limits)
	STA	REQUESTED_POWER

	CALL	ESETUP_FUNC

	LXI	H,0
	SHLD	TEVAL	;zero total energy value
	SHLD	TEVAL1


SERVICE?:
	CALL	SERVICE_SWITCH_MONITOR_FUNC	;check for service switch
	LDA	KEYBOARD_DEBOUNCE_STORAGE1
	CPI	SERREQ_BTN
	JNZ	NOSER
	LDA	SERFLG	;has flag been set since power on?
	CPI	'Y'
	JZ	SERVICE	;if so, do service routine
	MVI	A,'Y'	;if not, set it now
	STA	SERFLG

    	CALL	RESET_DISPLAY_FUNC
	LXI	H,SERATT	;display service mode attention message
	CALL	PUTSTR_FUNC
    	MVI	A,20	;second line
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,SERATT2
	CALL	PUTSTR_FUNC
SERW:	LHLD	RUNNING_STATUS_COUNTER
	INX	H
	SHLD	RUNNING_STATUS_COUNTER	;wait for reset
	JMP	SERW

SELSTR:		DB	'    PLEASE WAIT     $'
SELSTR2:	DB	'     SELF TEST      $'
SERATT:		DB	' SERVICE SWITCH ON  $'
SERATT2:	DB	'  RESET CONTROLLER  $'
FTSWTSTR:	DB	'  PRESS FOOTSWITCH  $'
FTSWTSTR2:	DB	'  FOR SHUTTER TEST  $'
REL_FTSWTSTR:	DB	' RELEASE FOOTSWITCH $'
STPSTR:		DB	'SHUTTER TEST PASSED $'

NOSER:	MVI	A,'N'
	STA	SERFLG	;double sure service mode not set

;test ftsw and safety shutter
FTSW_SHUTTER_TEST:
	LDA FOOT_SWITCH_TEST_REQ
	CPI 'Y'
	JNZ BYPASS_FOOT_SWITCH_TEST
	
	CALL	RESET_DISPLAY_FUNC	;ftsw message
	LXI	H,FTSWTSTR
	CALL	PUTSTR_FUNC

	MVI	A,20	;ftsw message (second line)
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,FTSWTSTR2
	CALL	PUTSTR_FUNC
	CALL	BEEP_FUNC

FTLOOP1:
	LHLD	RUNNING_STATUS_COUNTER
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER

	IN	SSTATB
	ANI	SHMASK
	CPI	2	;2 = shutter closed
			;1 = shutter open
			;0 = shutter in between positions
			;3 = illegal (detector fault)
	JZ	FTLOOP2

	MVI	A,1	;indicate shutter not closed failure
	JMP	ERROR

FTLOOP2:
	IN	SSTATA	;wait for foot switch
	ANI	FTMASK
	CPI	2	;ftsw pressed?
	JNZ	FTLOOP1

			;if ftsw pressed,
			;test shutter open indicator
SHUTEST:LDA	CBYTE	;set safety shutter drive signal
	ANI	SSO	;active (lo)
	STA	CBYTE
	OUT	CPORT
	MVI	C,30	;wait 150 ms. for shutter to open

SHTLOOP:CALL	DEBOUNCE_DELAY_5MS_FUNC	;5 ms. delay
	IN	SSTATB	;test if safety shutter opened
	ANI	SHMASK
	CPI	1	;2 = shutter closed
			;1 = shutter open
			;0 = shutter in between positions
			;3 = illegal (detector fault)
	JZ	SOOK

	LHLD	RUNNING_STATUS_COUNTER	;bump safcnt
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER
	DCR	C
	JNZ	SHTLOOP

	MVI	A,13	;indicate shutter not open failure
	JMP	ERROR

SOOK:	CALL	BEEP_FUNC
	MVI	A,0	;release ftsw message
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,REL_FTSWTSTR
	CALL	PUTSTR_FUNC

	MVI	A,20	;ftsw message (second line)
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,FTSWTSTR2
	CALL	PUTSTR_FUNC

FTSW_SHUTTER_TEST2:
	LHLD	RUNNING_STATUS_COUNTER
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER

	IN	SSTATB
	ANI	SHMASK
	CPI	1	;2 = shutter closed
			;1 = shutter open
			;0 = shutter in between positions
			;3 = illegal (detector fault)
	JZ	FTLOOP22

	MVI	A,13	;indicate shutter not open failure
	JMP	ERROR

FTLOOP22:
	IN	SSTATA	;wait for foot switch
	ANI	FTMASK
	CPI	2	;ftsw released?
	JZ	FTSW_SHUTTER_TEST2	;if not, loop

;close shutter, test for closure
	LDA	CBYTE	;set safety shutter drive signal
	ORI	SSC	;not active (hi)
	STA	CBYTE
	OUT	CPORT

	MVI	C,12	;wait 60 ms. for shutter
SHTL1:	CALL	DEBOUNCE_DELAY_5MS_FUNC	;5 ms. delay
	LHLD	RUNNING_STATUS_COUNTER	;bump safcnt
	DCX	H
	SHLD	RUNNING_STATUS_COUNTER
	DCR	C
	JNZ	SHTL1

;test if safety shutter closed
	IN	SSTATB
	ANI	SHMASK
	CPI	2	;2 = shutter closed
			;1 = shutter open
			;0 = shutter in between positions
			;3 = illegal (detector fault)
	JZ	SSHUTTER_OK
	MVI	A,1	;indicate shutter not closed failure
	JMP	ERROR

SSHUTTER_OK:
	CALL	RESET_DISPLAY_FUNC	;clear screen, cursor home
	LXI	H,STPSTR	;display shutter test passed message
	CALL	PUTSTR_FUNC
	CALL	BEEP_FUNC
	CALL	DELAY_500MS_FUNC	;adds dignity to procedure
	CALL	DELAY_500MS_FUNC	;adds dignity to procedure

;test done, continue
BYPASS_FOOT_SWITCH_TEST:
	MVI	A,'Y'
	STA FOOT_SWITCH_TEST_REQ
	
	CALL	RESET_DISPLAY_FUNC
	LXI	H,ICARDSTR
	CALL	PUTSTR_FUNC

	MVI	A,20	;second line
	CALL	SET_CURSOR_POSITION_FUNC	;position cursor
	LXI	H,ICARDSTR2
	CALL	PUTSTR_FUNC

	CALL	DELAY_500MS_FUNC
	CALL	DELAY_500MS_FUNC

	MVI	A,'S'
	STA	MODEFLG	;set stby mode flag
			;display 'standby' mode
	MVI	A,2
	STA	RC_COMM_TIMEOUT	;reload timer


	CALL	BEEP_FUNC	;alert operator that unit is ready for use

	LXI	H,100	;init card check timer for .5 secs
	SHLD	CARD_CHECK_TIMER

	MVI	A,'P'	;init card update pending status
	STA	INT_CARD_STATUS

	JMP	MAIN	;enter main loop

ICARDSTR:	DB	' PERFORMING SYSTEM  $'
ICARDSTR2:	DB	'    STATUS CHECK    $'

	END


